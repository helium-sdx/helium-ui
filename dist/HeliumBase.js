"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.changeFreezeState = exports.isHeliumHazed = exports.heliumHaze = void 0;
/** Add rookie._helium = {} if it doesn't exist already */
function heliumHaze(rookie) {
    rookie._helium = rookie._helium || {};
    return rookie;
}
exports.heliumHaze = heliumHaze;
/** Check if thing has storage area for Helium logic */
function isHeliumHazed(checkMe) {
    return checkMe._helium;
}
exports.isHeliumHazed = isHeliumHazed;
function changeFreezeState(hazed, shouldBeFrozen) {
    const helium = hazed._helium;
    if (helium.frozen === shouldBeFrozen) {
        return;
    }
    helium.frozen = shouldBeFrozen;
    const fns = shouldBeFrozen ? helium.freeze : helium.unfreeze;
    if (fns) {
        fns.forEach((fn) => fn());
    }
}
exports.changeFreezeState = changeFreezeState;
//# sourceMappingURL=HeliumBase.js.map