import { RkMapFn } from './SourceBase';
import { SourceMap as _CoreSourceMap } from "helium-sdx";
/**
 * @importance 7
 */
export declare class SourceMap<KEY, VALUE> extends _CoreSourceMap<KEY, VALUE> {
    renderMap(renderFn: RkMapFn<VALUE, KEY>): ChildNode[];
    renderMapKeyed(renderFn: RkMapFn<VALUE, KEY>): ChildNode[];
    private _mapReplacers;
}
