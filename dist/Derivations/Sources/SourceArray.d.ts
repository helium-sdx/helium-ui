import { Innards } from '../../El-Tool/El-Tool';
import { SourceArray as _CoreSourceArray } from "helium-sdx";
/**
 * @importance 7
 */
export declare class SourceArray<TYPE> extends _CoreSourceArray<TYPE> {
    renderMap(renderFn: (value: TYPE) => Innards): ChildNode[];
    renderMapKeyed(renderFn: (value: TYPE, index: number) => Innards): ChildNode[];
    private _mapReplacers;
}
