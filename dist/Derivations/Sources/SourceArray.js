"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SourceArray = void 0;
const RerenderReplacer_1 = require("../Derivers/RerenderReplacer");
const helium_sdx_1 = require("helium-sdx");
/**
 * @importance 7
 */
class SourceArray extends helium_sdx_1.SourceArray {
    renderMap(renderFn) {
        return this._mapReplacers(false, renderFn);
    }
    renderMapKeyed(renderFn) {
        return this._mapReplacers(true, renderFn);
    }
    _mapReplacers(withIndex, renderFn) {
        const obsToRerenderMap = new Map();
        const listReplacer = new RerenderReplacer_1.RerenderReplacer(() => {
            this.orderMatters();
            const out = [];
            const length = this.length;
            for (let i = 0; i < length; i++) {
                const observable = withIndex ? this._getIndexObserver(i) : this.getSource(i);
                if (obsToRerenderMap.has(observable) === false) {
                    obsToRerenderMap.set(observable, new RerenderReplacer_1.RerenderReplacer(() => {
                        const value = observable.get();
                        if (withIndex) {
                            return renderFn(this.noDeletedConstant(value.get()), i);
                        }
                        else {
                            return renderFn(this.noDeletedConstant(value));
                        }
                    }));
                }
                const nodes = obsToRerenderMap.get(observable).getRender();
                nodes.forEach((node) => out.push(node));
            }
            return out;
        });
        return listReplacer.getRender();
    }
}
exports.SourceArray = SourceArray;
//# sourceMappingURL=SourceArray.js.map