import { Innards } from '../../El-Tool/El-Tool';
/** A unique id for a source, used by DerivationManager for dependency mapping. */
export declare type SourceId = string;
export declare type Setter<TYPE> = (value: TYPE) => void;
export declare type RMapFn<TYPE> = (value: TYPE) => Innards;
export declare type RkMapFn<TYPE, KEY> = (value: TYPE, key: KEY) => Innards;
export declare type AnyRMapFn<TYPE, KEY> = RMapFn<TYPE> | RkMapFn<TYPE, KEY>;
