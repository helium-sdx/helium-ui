"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SourceMap = void 0;
const RerenderReplacer_1 = require("../Derivers/RerenderReplacer");
const helium_sdx_1 = require("helium-sdx");
/**
 * @importance 7
 */
class SourceMap extends helium_sdx_1.SourceMap {
    renderMap(renderFn) {
        return this._mapReplacers(false, renderFn);
    }
    renderMapKeyed(renderFn) {
        return this._mapReplacers(true, renderFn);
    }
    _mapReplacers(withKey, renderFn) {
        const entryReplacers = new Map();
        const orderer = new RerenderReplacer_1.RerenderReplacer(() => {
            const out = [];
            const keys = this.keys();
            for (const key of keys) {
                if (entryReplacers.has(key) === false) {
                    const source = this.getSource(key);
                    entryReplacers.set(key, new RerenderReplacer_1.RerenderReplacer(() => renderFn(source.get(), withKey ? key : undefined)));
                }
                const nodeList = entryReplacers.get(key).getRender();
                for (const node of nodeList) {
                    out.push(node);
                }
            }
            return out;
        });
        return orderer.getRender();
    }
}
exports.SourceMap = SourceMap;
//# sourceMappingURL=SourceMap.js.map