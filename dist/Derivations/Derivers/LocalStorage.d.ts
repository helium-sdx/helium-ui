import { Sourcified } from "../../Sourcify/SourceProxyShallowTypes";
export declare type Storable<TYPE extends object = {}> = Sourcified<TYPE>;
export declare function syncWithLocalStorage<TYPE extends object>(id: string, storeMe: Storable<TYPE>): void;
