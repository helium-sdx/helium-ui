"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.anchorDeriverToDomNode = void 0;
const HeliumBase_1 = require("../../HeliumBase");
const helium_sdx_1 = require("helium-sdx");
function anchorDeriverToDomNode(node, ddx, type) {
    let scope;
    setupNodeAnchorMutationObserver();
    return {
        run: () => {
            const helium = HeliumBase_1.heliumHaze(node)._helium;
            const anchor = new helium_sdx_1.AnchorNode();
            anchor.details = [`(Right Click > Reveal in Elements Panel).  Issue occurred in render preceding: `, node];
            const deriveFn = () => {
                const onPage = node.getRootNode() === document;
                if (!onPage) {
                    HeliumBase_1.changeFreezeState(node, true);
                }
                ddx();
            };
            scope = new helium_sdx_1.DeriverScope({
                fn: deriveFn,
                batching: "singleFrame",
                anchor,
            });
            (helium.unfreeze = helium.unfreeze || []).push(() => {
                anchor.setStatus(helium_sdx_1.AnchorStatus.NORMAL);
            });
            (helium.freeze = helium.freeze || []).push(() => {
                anchor.setStatus(helium_sdx_1.AnchorStatus.FROZEN);
            });
            scope.runDdx();
        },
        report: (req) => ({
            ...(req.scope) && { scope },
        })
    };
}
exports.anchorDeriverToDomNode = anchorDeriverToDomNode;
let mutationsBuffer = [];
let bufferTimeout;
let mutationObserver;
function setupNodeAnchorMutationObserver() {
    if (typeof MutationObserver === "undefined") {
        return;
    }
    if (mutationObserver) {
        return;
    }
    mutationObserver = new MutationObserver((mutationsList) => {
        mutationsList.forEach((mutation) => {
            if (mutation.type !== 'childList') {
                return;
            }
            mutationsBuffer.push(mutation);
        });
        if (bufferTimeout) {
            clearTimeout(bufferTimeout);
        }
        bufferTimeout = setTimeout(() => {
            bufferTimeout = undefined;
            const oldBuffer = mutationsBuffer;
            mutationsBuffer = [];
            const haveNode = new Map();
            for (const mutation of oldBuffer) {
                mutation.removedNodes.forEach((node) => haveNode.set(node, false));
                mutation.addedNodes.forEach((node) => haveNode.set(node, true));
            }
            const addedNodes = [];
            const removedNodes = [];
            Array.from(haveNode.entries()).forEach(([node, shouldHave]) => {
                if (HeliumBase_1.isHeliumHazed(node) && node._helium.moveMutation) {
                    return node._helium.moveMutation = false;
                }
                shouldHave ? addedNodes.push(node) : removedNodes.push(node);
            });
            helium_sdx_1.aLog("treeMutations", "Adding:", addedNodes);
            helium_sdx_1.aLog("treeMutations", "Removing:", removedNodes);
            addedNodes.forEach((node) => {
                HeliumBase_1.heliumHaze(node);
                permeate(node, (child) => {
                    if (HeliumBase_1.isHeliumHazed(child)) {
                        HeliumBase_1.changeFreezeState(child, false);
                    }
                });
            });
            removedNodes.forEach((node) => {
                HeliumBase_1.heliumHaze(node);
                permeate(node, (child) => {
                    if (HeliumBase_1.isHeliumHazed(child)) {
                        HeliumBase_1.changeFreezeState(child, true);
                    }
                });
            });
        }, 10);
    });
    var config = { childList: true, subtree: true };
    if (typeof window !== "undefined") {
        const tryAddObserver = () => mutationObserver.observe(document.body, config);
        if (document && document.body) {
            tryAddObserver();
        }
        else {
            document.addEventListener("DOMContentLoaded", () => tryAddObserver());
        }
    }
    function permeate(root, cb) {
        cb(root);
        root.childNodes.forEach((child) => permeate(child, cb));
    }
}
//# sourceMappingURL=DomAnchoredDeriver.js.map