import { DeriverScope, DeriveFn } from "helium-sdx";
export interface IDomAnchorReport {
    scope?: DeriverScope;
}
export declare type ReportRequest<OUTLINE> = {
    [key in keyof OUTLINE]?: true;
};
export declare type Reporter<OUTLINE> = (request: ReportRequest<OUTLINE>) => Required<Pick<OUTLINE, keyof typeof request>>;
export declare function anchorDeriverToDomNode(node: ChildNode, ddx: DeriveFn, type?: string): {
    run: () => void;
    report: Reporter<IDomAnchorReport>;
};
