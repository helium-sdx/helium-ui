"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.syncWithLocalStorage = void 0;
const helium_sdx_1 = require("helium-sdx");
function syncWithLocalStorage(id, storeMe) {
    const init = localStorage.getItem(id);
    if (init) {
        try {
            const startingData = JSON.parse(init);
            storeMe.upsert(startingData);
        }
        catch (err) {
            console.warn("Could not parse locally stored data", err);
        }
    }
    helium_sdx_1.derive({
        anchor: "NO_ANCHOR",
        fn: () => {
            const toStore = storeMe.toObject();
            try {
                localStorage.setItem(id, JSON.stringify(toStore));
            }
            catch (err) {
                console.warn("Could not store", err);
                console.log(toStore);
            }
        }
    });
}
exports.syncWithLocalStorage = syncWithLocalStorage;
//# sourceMappingURL=LocalStorage.js.map