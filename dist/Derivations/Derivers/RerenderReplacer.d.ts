import { Innards, BaseNode } from '../../El-Tool/El-Tool';
import { IDomAnchorReport, Reporter } from './DomAnchoredDeriver';
import { SourceBase } from 'helium-sdx';
/** A form of deriver management.  Beyond hosting the deriver function, this class
 * also handles freezing and unfreezing derivation when its placeholder is added
 * or removed from the page.  Further, it makes sure to only add, remove, or swap
 * the elements in each rerender if they require it.
 * @param.rerenderFn - A function which returns whatever the most up to date elements
 * for it should be */
export declare class RerenderReplacer {
    protected rerenderFn: () => Innards;
    private static nextId;
    private currentRender;
    private id;
    private placeHolder;
    protected getAnchorReport: Reporter<IDomAnchorReport>;
    constructor(rerenderFn: () => Innards);
    protected preConstructHook(): void;
    /** Returns the current render. This render is live and will update automatically. */
    getRender(): BaseNode[];
    /** Typically called by DerivationManager, if not frozen, this function
     * will update its current render both on the page and locally, then return it. */
    derive(): BaseNode[];
    /** Removes all elements managed by this replacer from the page */
    removeAll(): void;
}
export interface ICachedRender {
    sourceVals: Map<SourceBase, any>;
    render: Innards;
}
export declare class RenderRecycler extends RerenderReplacer {
    protected preConstructHook(): void;
}
/** Shorthand for creating a new RenderRecycler class.  Use this when you want to
 * cache previous renders.
 * @importance 17
 * @warning This functions stops garbage collection of old views by caching them.  Only use this
 * function for sections of your UI which have very few permutations each with large amounts of content.
 * @eg const view = source("bert");\
 * const viewEl = div("View", recycle(\
 *   // each of these will only be rendered once\
 *   switch (view.get()) {\
 *     case "bert": return renderLargeMarkdown("berts-autobiography.md");\
 *     case "ernie": return renderLargeMarkdown("ernies-autobiography.md");\
 *   }\
 * ));
 */
export declare function recycle(renderFn: () => Innards): ChildNode[];
