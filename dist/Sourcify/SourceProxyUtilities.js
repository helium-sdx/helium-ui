"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.renderMapKeyed = exports.renderMap = exports.isProxied = exports.Unsourcify = exports.Sourcify = void 0;
const El_Tool_1 = require("../El-Tool/El-Tool");
const helium_sdx_1 = require("helium-sdx");
// import { SourceProxyArray } from "./SourceProxyArray";
// import { SourceProxyObject } from "./SourceProxyObject";
// SourcificationManager.createSourceProxyArray = (item) => {
//   console.log("Creating better arry");
//   return new SourceProxyArray(item);
// };
// SourcificationManager.createSourceProxyObject = (item) => new SourceProxyObject(item);
let SourcificationManagerModded = false;
/** Takes an ordinary object and makes it act like a recursive source.
 * Changing any property will update any deriver which accessed
 * that property or a child of it.
 * @importance 22 */
// export function Sourcify<TYPE extends ProxiedAny<any>>(target: TYPE): Sourcified<TYPE[keyof TYPE]>;
function Sourcify(target) {
    if (!SourcificationManagerModded) {
        const { SourceProxyArray } = require("./SourceProxyArray");
        const { SourceProxyObject } = require("./SourceProxyObject");
        helium_sdx_1.SourcificationManager.createSourceProxyArray = (item) => new SourceProxyArray(item);
        helium_sdx_1.SourcificationManager.createSourceProxyObject = (item) => new SourceProxyObject(item);
        SourcificationManagerModded = true;
    }
    return helium_sdx_1.Sourcify(target);
}
exports.Sourcify = Sourcify;
function Unsourcify(target) {
    return helium_sdx_1.Unsourcify(target);
}
exports.Unsourcify = Unsourcify;
function isProxied(checkMe) {
    return helium_sdx_1.isProxied(checkMe);
}
exports.isProxied = isProxied;
/**
 * Efficiently renders and manages every child of an object or array.
 * This function renders without key dependency, so items will be moved if their index changes but they will not be re-rendered.
 * @importance 18
 * @param.obj Anything which has children that can be iterated over.  Can be an array, an object, or a proxy.
 * @param.renderFn This function will get called for every child.  It only takes one argument which is the child
 * @eg const list = Sourcify(["a", "b", "c", "d"]);\
 * const listEl = div("List", rmap(list, (value) => \
 *   div("ListValue", value),\
 * ));\
 *
 * const obj = Sourcify({ a: 1, b: 2, c: 3});\
 * const objEl = div("Obj", rmap(obj, (value) => \
 *   div("ObjValue", value),\
 * ));\
 */
function renderMap(obj, renderFn) {
    return _anyRMap(false, obj, renderFn);
}
exports.renderMap = renderMap;
/**
 * Efficiently renders and manages every child of an object or array.  This
 * function is key/index dependent, so changes to an items index will cause it to be rendered from scratch.  Only use
 * this function if you need to display the index somehow.  Otherwise, renderMap() can handle re-rendering by only reordering
 * previous renders.
 * @importance 18
 * @param.obj Anything which has children that can be iterated over.  Can be an array, an object, or a proxy.
 * @param.renderFn This function will get called for every child.  It will recieve both the child and the key or index of the child.
 * @eg const list = Sourcify(["a", "b", "c", "d"]);\
 * const listEl = div("List", rkmap(list, (value, index) => \
 *   div("ListValue", `${value} is ${ index%2 ? "odd" : "even" }`),\
 * ));\
 *
 * const obj = Sourcify({ a: 1, b: 2, c: 3});\
 * const objEl = div("Obj", rmap(obj, (value, key) => \
 *   div("ObjValue", `${key} is equal to ${value}`),\
 * ));\
 */
function renderMapKeyed(obj, renderFn) {
    return _anyRMap(true, obj, renderFn);
}
exports.renderMapKeyed = renderMapKeyed;
function _anyRMap(withKey, obj, renderFn) {
    if (isProxied(obj)) {
        if (withKey) {
            return obj.rkmap(renderFn);
        }
        return obj.rmap(renderFn);
    }
    else if (obj && typeof obj === "object") {
        const out = [];
        let renders;
        if (Array.isArray(obj)) {
            renders = obj.map((value, index) => renderFn(value, index));
        }
        else {
            renders = Object.entries(obj).map(([key, value]) => renderFn(value, key));
        }
        renders.forEach((render) => {
            const nodesList = El_Tool_1.convertInnardsToElements(render);
            if (!nodesList || !nodesList.length) {
                return;
            }
            nodesList.forEach((node) => out.push(node));
        });
        return out;
    }
}
//# sourceMappingURL=SourceProxyUtilities.js.map