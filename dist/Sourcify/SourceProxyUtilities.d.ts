import { RMapFn, RkMapFn } from '../Derivations/Sources/SourceBase';
import { Sourcified, ProxiedAny } from '../Sourcify/SourceProxyShallowTypes';
/** Takes an ordinary object and makes it act like a recursive source.
 * Changing any property will update any deriver which accessed
 * that property or a child of it.
 * @importance 22 */
export declare function Sourcify<TYPE extends object>(target: TYPE): Sourcified<TYPE>;
export declare function Unsourcify<TYPE>(target: TYPE | Sourcified<TYPE>): TYPE;
export declare function isProxied<TYPE>(checkMe: TYPE | Sourcified<TYPE>):
  // @ts-ignore
  checkMe is ProxiedAny<TYPE>;
export declare function renderMap<TYPE extends Array<any>>(arr: TYPE | Sourcified<TYPE>, renderFn: RMapFn<TYPE[number]>): ChildNode[];
export declare function renderMap<TYPE extends object>(obj: TYPE | Sourcified<TYPE>, renderFn: RMapFn<TYPE[keyof TYPE]>): ChildNode[];
export declare function renderMapKeyed<TYPE extends Array<any>>(arr: TYPE | Sourcified<TYPE>, renderFn: RkMapFn<number, TYPE[number]>): ChildNode[];
export declare function renderMapKeyed<TYPE extends object>(obj: TYPE | Sourcified<TYPE>, renderFn: RkMapFn<keyof TYPE, TYPE[keyof TYPE]>): ChildNode[];
