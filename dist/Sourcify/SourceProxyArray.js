"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ASSERT_SHALLOW_TYPE_MATCH_ARRAY = exports.SourceProxyArray = void 0;
const SourceArray_1 = require("../Derivations/Sources/SourceArray");
const helium_sdx_1 = require("helium-sdx");
class SourceProxyArray extends helium_sdx_1.SourceProxyArray {
    constructor(target) {
        super(target, new SourceArray_1.SourceArray());
    }
    getSrcArray() {
        return this.srcArray;
    }
    // TODO - test
    renderMap(renderFn) {
        this.refreshAll();
        return this.getSrcArray().renderMap(renderFn);
    }
    // TODO - test
    renderMapKeyed(renderFn) {
        this.refreshAll();
        return this.getSrcArray().renderMapKeyed(renderFn);
    }
}
exports.SourceProxyArray = SourceProxyArray;
exports.ASSERT_SHALLOW_TYPE_MATCH_ARRAY = {};
//# sourceMappingURL=SourceProxyArray.js.map