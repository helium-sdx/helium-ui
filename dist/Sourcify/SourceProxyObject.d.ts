import { SourceMap } from "../Derivations/Sources/SourceMap";
import { SourceProxyObject as _CoreSourceProxyObject } from "helium-sdx";
import { Innards } from "../El-Tool/El-Tool";
export declare class SourceProxyObject<TARGET extends object> extends _CoreSourceProxyObject<TARGET> {
    constructor(target: TARGET);
    protected getSrcMap(): SourceMap<keyof TARGET, TARGET[keyof TARGET]>;
    renderMap(renderFn: (value: TARGET[keyof TARGET]) => Innards): ChildNode[];
    renderMapKeyed<KEY extends keyof TARGET>(renderFn: (value: TARGET[KEY], key: KEY) => Innards): ChildNode[];
}
interface IAssertTestType {
    a: boolean;
    b: number[];
    c: {
        d: string;
    };
}
export declare const ASSERT_SHALLOW_TYPE_MATCH: keyof (SourceProxyObject<IAssertTestType> & {
    getProxyManager: any;
    getSourceMap: any;
    toObjectNoDeps: any;
    pretend: any;
    a: any;
    b: any;
    c: any;
    toString: any;
    toLocaleString: any;
    lastIndexOf: any;
    reduceRight: any;
    copyWithin: any;
    values: any;
    flatMap: any;
    forEach: any;
    entries: any;
    clear: any;
    delete: any;
    has: any;
    isEmpty: any;
    overwrite: any;
    peekHas: any;
    upsert: any;
});
export {};
