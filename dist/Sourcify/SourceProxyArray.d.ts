import { Innards } from "../El-Tool/El-Tool";
import { SourceArray } from "../Derivations/Sources/SourceArray";
import { SourceProxyArray as _CoreSourceProxyArray, Sourcified } from "helium-sdx";
export declare class SourceProxyArray<TARGET extends any[]> extends _CoreSourceProxyArray<TARGET> {
    constructor(target: TARGET);
    protected getSrcArray(): SourceArray<Sourcified<TARGET[number]>>;
    renderMap(renderFn: (value: TARGET[number]) => Innards): ChildNode[];
    renderMapKeyed(renderFn: (value: TARGET[number]) => Innards): ChildNode[];
}
export declare const ASSERT_SHALLOW_TYPE_MATCH_ARRAY: keyof (SourceProxyArray<string[]> & {
    getProxyManager: any;
    toObjectNoDeps: any;
    pretend: any;
    [key: number]: any;
    toString: any;
    toLocaleString: any;
    lastIndexOf: any;
    reduceRight: any;
    copyWithin: any;
    values: any;
    flatMap: any;
});
