"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ASSERT_SHALLOW_TYPE_MATCH = exports.SourceProxyObject = void 0;
const SourceMap_1 = require("../Derivations/Sources/SourceMap");
const helium_sdx_1 = require("helium-sdx");
class SourceProxyObject extends helium_sdx_1.SourceProxyObject {
    constructor(target) {
        super(target, new SourceMap_1.SourceMap());
    }
    getSrcMap() {
        return this.srcMap;
    }
    // TODO - test
    renderMap(renderFn) {
        this.refreshAll();
        return this.getSrcMap().renderMap(renderFn);
    }
    // TODO - test
    renderMapKeyed(renderFn) {
        this.refreshAll();
        return this.getSrcMap().renderMapKeyed(renderFn);
    }
}
exports.SourceProxyObject = SourceProxyObject;
;
exports.ASSERT_SHALLOW_TYPE_MATCH = {};
//# sourceMappingURL=SourceProxyObject.js.map