import { Innards } from "../El-Tool/El-Tool";
import { ISourcifiedArrayProto as __CoreISourcifiedArrayProto, ISourcifiedObjectProto as __CoreISourcifiedObjectProto, SourcificationManager } from "helium-sdx";
export declare type Sourcified<TARGET> = (TARGET extends ChildNode ? TARGET : TARGET extends Function ? TARGET : TARGET extends {
    _noHeliumProxy: any;
} ? TARGET : TARGET extends Array<any> ? SourcifiedArray<TARGET> : TARGET extends object ? SourcifiedObject<TARGET> : TARGET);
export declare type ProxiedAny<TYPE extends object> = TYPE & {
    getProxyManager(): SourcificationManager<TYPE>;
    toObjectNoDeps(): TYPE | undefined;
};
declare type IT<TYPE extends Array<any>> = Sourcified<TYPE[number]>;
export interface ISourcifiedArrayProto<TYPE extends Array<any>> extends Omit<__CoreISourcifiedArrayProto<TYPE, IT<TYPE>, SourcifiedArray<TYPE>>, "map"> {
    [Symbol.iterator](): Generator<IT<TYPE>, void, unknown>;
    [Symbol.unscopables](): any;
    map<OUT>(cb: (it: IT<TYPE>, index: number, array: IT<TYPE>[]) => OUT): SourcifiedArray<Array<OUT>>;
    renderMap(renderFn: (value: IT<TYPE>) => Innards): ChildNode[];
    renderMapKeyed(renderFn: (value: IT<TYPE>, key: number) => Innards): ChildNode[];
}
export declare type SourcifiedArray<TYPE extends Array<any>> = {
    [key in (keyof TYPE & number)]: IT<TYPE>;
} & ISourcifiedArrayProto<TYPE>;
export interface ISourcifiedObjectProto<TYPE extends object> extends Omit<__CoreISourcifiedObjectProto<TYPE>, "get" | "overwrite"> {
    [Symbol.iterator](): Generator<TYPE[keyof TYPE], void, unknown>;
    get<IT extends KEY<TYPE>>(key: IT): Sourcified<TYPE[IT]>;
    overwrite(me: TYPE): SourcifiedObject<TYPE>;
    renderMap<PROP extends KEY<TYPE>>(renderFn: (value: TYPE[PROP]) => Innards): ChildNode[];
    renderMapKeyed<PROP extends KEY<TYPE>>(renderFn: (value: TYPE[PROP], key: PROP) => Innards): ChildNode[];
}
declare type KEY<TYPE> = keyof TYPE;
export declare type SourcifiedObject<TYPE extends object> = {
    [key in keyof TYPE]: Sourcified<TYPE[key]>;
} & ISourcifiedObjectProto<TYPE>;
export {};
