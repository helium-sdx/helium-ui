/**
 * @importance 12
 * @param init
 * @param cb
 */
export declare function blockEvent(eventName: string): (ref: HTMLElement) => void;
