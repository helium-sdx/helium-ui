"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.onIntersect = exports.addIntersectionObserver = void 0;
const Factories_1 = require("../El-Tool/Factories");
const El_Tool_1 = require("../El-Tool/El-Tool");
let styleEl;
function assertStyling() {
    if (styleEl) {
        return;
    }
    styleEl = El_Tool_1.el("style");
    styleEl.innerHTML = `
    .hintRelative {
      position: relative;
    }

    .CoverCheckerPositioner {
      position: absolute;
      top: 0px;
      left: 0px;
      right: 0px;
      bottom: 0px;
      pointer-events: none;
      visibility: hidden;
    }
    
    .CoverChecker {
      position: sticky;
      height: 100vh;
      width: 100vw;
      top: 0px; 
    }
  `;
    document.head.prepend(styleEl);
}
function addIntersectionObserver(nodes, init, cb) {
    let thresholds;
    const isObject = (init) => {
        return (init && typeof init === "object") && !Array.isArray(init);
    };
    if (isObject(init)) {
        thresholds = init.threshold;
    }
    if (!Array.isArray(thresholds)) {
        thresholds = [thresholds];
    }
    let startLength = thresholds.length;
    const normalThresholds = thresholds.map((it) => {
        switch (it) {
            case "contained":
            case "maxFill": return 1;
            case "any": return 0;
        }
        return it;
    }).filter((it) => typeof it === "number");
    const needsCoverMod = startLength > normalThresholds.length;
    if (!Array.isArray(nodes)) {
        nodes = [nodes];
    }
    if (needsCoverMod) {
        assertStyling();
        const checkUs = nodes.map((node) => {
            let checkMe;
            const addMe = Factories_1.div("CoverCheckerPositioner", checkMe = Factories_1.div("CoverChecker"));
            node.classList.add("hintRelative");
            node.prepend(addMe);
            return checkMe;
        });
        const watcher = new IntersectionObserver(cb, {
            threshold: 1
        });
        checkUs.forEach((node) => watcher.observe(node));
    }
    if (normalThresholds.length === 0) {
        return;
    }
    const options = isObject(init) ? init : {};
    options.threshold = normalThresholds;
    const watcher = new IntersectionObserver(cb, options);
    setTimeout(() => {
        nodes.forEach((node) => watcher.observe(node));
    }, 0);
}
exports.addIntersectionObserver = addIntersectionObserver;
/**
 * @importance 14
 * @param init
 * @param cb
 */
function onIntersect(init, cb) {
    return (ref) => addIntersectionObserver(ref, init, (entries, observer) => {
        cb(ref, entries[0], observer);
    });
}
exports.onIntersect = onIntersect;
//# sourceMappingURL=OnIntersect.js.map