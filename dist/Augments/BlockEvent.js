"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.blockEvent = void 0;
/**
 * @importance 12
 * @param init
 * @param cb
 */
function blockEvent(eventName) {
    return (ref) => ref.addEventListener(eventName, (ev) => { ev.preventDefault(); return false; });
}
exports.blockEvent = blockEvent;
//# sourceMappingURL=BlockEvent.js.map