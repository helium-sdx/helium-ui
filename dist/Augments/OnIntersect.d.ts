import { AugmentCb } from '../El-Tool/El-Tool';
export declare type ThresholdType = number | "any" | "covering" | "contained" | "maxFill";
export declare type InitArg = ThresholdType | (ThresholdType[]) | AugmentedIntersectionObserverInit;
declare type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
export interface AugmentedIntersectionObserverInit extends Omit<IntersectionObserverInit, 'threshold'> {
    threshold?: ThresholdType | ThresholdType[];
}
export declare function addIntersectionObserver(nodes: Element | Element[], init: InitArg, cb: IntersectionObserverCallback): void;
export declare type CbArg = AugmentCb<[IntersectionObserverEntry, IntersectionObserver]>;
/**
 * @importance 14
 * @param init
 * @param cb
 */
export declare function onIntersect(init: InitArg, cb: CbArg): (ref: HTMLElement) => void;
export {};
