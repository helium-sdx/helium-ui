"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.replaceUrl = exports.followUrl = exports.observeUrl = exports.onUrlChange = exports.UrlState = exports._UrlState = exports.NavigationEvent = exports.externalLink = exports.a = exports.anchor = void 0;
const El_Tool_1 = require("./El-Tool");
const Derivations_1 = require("../Derivations");
const helium_sdx_1 = require("helium-sdx");
const Sourcify_1 = require("../Sourcify");
function anchor(...[classOrProp, propOrChild, child]) {
    const { props, innards } = El_Tool_1.standardizeInput(classOrProp, propOrChild, child);
    props.attr = props.attr || {};
    const { href } = props;
    if (!href) {
        props.attr.href = "#";
    }
    else if (typeof href === "string") {
        props.attr.href = href;
    }
    if (props.target) {
        props.attr.target = props.target;
    }
    const ref = El_Tool_1.el("a", props, innards);
    if (typeof href === "function") {
        Derivations_1.anchorDeriverToDomNode(ref, () => {
            ref.href = href();
        });
    }
    if (props.allowTextSelect) {
        El_Tool_1.setStyle(ref, { "-webkit-user-drag": "none" });
        let selectedText = "";
        El_Tool_1.on("mousedown", ref, (ev) => selectedText = window.getSelection().toString());
        El_Tool_1.on("click", ref, (ev) => {
            if (selectedText !== window.getSelection().toString()) {
                ev.preventDefault();
            }
        });
    }
    El_Tool_1.on("click", ref, (ev) => {
        if (ref.href) {
            if (props.disableHref) {
                return ev.preventDefault();
            }
            if (ev.metaKey || ref.target === "_blank") {
                window.open(ref.href, '_blank');
                return ev.preventDefault();
            }
            else if (exports.UrlState.update({ urlPart: ref.href, mode: "push", deferLocationChange: true }) !== "DEFERRED") {
                return ev.preventDefault();
            }
        }
    });
    return ref;
}
exports.anchor = anchor;
function a(...args) {
    console.warn(`helium function "a" has been deprecated.  Please use "anchor" instead.`);
    return anchor(...args);
}
exports.a = a;
function externalLink(...[classOrProp, propOrChild, child]) {
    const { props, innards } = El_Tool_1.standardizeInput(classOrProp, propOrChild, child);
    props.target = "_blank";
    return a(props, innards);
}
exports.externalLink = externalLink;
class NavigationEvent extends Event {
    constructor(name, props, newUrl) {
        super(name, props);
        this.pageRefreshCancelled = false;
        this.hashFocusCancelled = false;
        // Safari doesn't like class extension
        this.preventPageRefresh = () => {
            this.pageRefreshCancelled = true;
        };
        this.preventHashFocus = () => {
            this.hashFocusCancelled = true;
        };
        this.newUrl = NavigationEvent.createFullUrl(newUrl);
    }
    static createFullUrl(urlPart) {
        try {
            const out = new URL(urlPart);
            if (!out.host) {
                throw "";
            }
            return out;
        }
        catch (err) {
            return new URL(urlPart, location.origin);
        }
    }
}
exports.NavigationEvent = NavigationEvent;
class _UrlState {
    constructor() {
        this.state = Sourcify_1.Sourcify({
            url: new URL(location.href),
            urlParams: {},
            hash: "",
        });
        this.initListeners();
    }
    getUrl() { return this.state.url; }
    get href() { return this.getUrl().href; }
    get host() { return this.getUrl().host; }
    get hostname() { return this.getUrl().hostname; }
    get pathname() { return this.getUrl().pathname; }
    get hash() { return this.state.hash; }
    get urlParams() { return this.state.urlParams; }
    // Called by clicking link, first gets the event returned by dispatching a url update
    // If the event is captured and handled (pageRefreshCancelled) the url is pushed to history
    followUrl(newUrl) {
        return this.update({ urlPart: newUrl, mode: "push" });
    }
    update(argsOrUrl) {
        const args = typeof argsOrUrl === "object" ? argsOrUrl : {
            urlPart: argsOrUrl
        };
        args.mode = args.mode || "push";
        const followResponse = this.newNavEvent("followurl", args.urlPart, true);
        if (followResponse && args.preventHashFocus) {
            followResponse.preventHashFocus();
        }
        let evResponse;
        if (followResponse) {
            const changeEv = this.newNavEvent("locationchange", args.urlPart);
            if (followResponse.hashFocusCancelled) {
                changeEv.preventHashFocus();
            }
            evResponse = window.dispatchEvent(changeEv) && changeEv;
        }
        if (!evResponse) {
            return "BLOCKED";
        }
        if (evResponse.pageRefreshCancelled) {
            if (evResponse.newUrl.href !== location.href) {
                // can not cancel refresh with host change
                if (location.host !== evResponse.newUrl.host) {
                    window.location.href = evResponse.newUrl.href;
                    return;
                }
                else {
                    if (args.mode === "push") {
                        history.pushState(null, "", args.urlPart);
                    }
                    else if (args.mode === "replace") {
                        history.replaceState(null, "", args.urlPart);
                    }
                    this.state.set("url", new URL(args.urlPart));
                }
            }
            return "CAPTURED";
        }
        if (args.deferLocationChange) {
            return "DEFERRED";
        }
        else {
            window.location.href = NavigationEvent.createFullUrl(args.urlPart).href;
        }
    }
    replaceUrl(newUrl) {
        return this.update({ urlPart: newUrl, mode: "replace" });
    }
    onUrlChange(cb) {
        window.addEventListener("locationchange", cb);
    }
    observeUrl(cb) {
        this.onUrlChange(cb);
        const ev = this.newNavEvent(name, location.href);
        cb(ev);
    }
    // public _updateUrl(args: {
    //   urlPart: string, 
    //   deferLocationChange?: boolean,
    //   mode: "push" | "replace"
    // }): "CAPTURED" | "BLOCKED" | "DEFERRED" {
    //   const evResponse = dispatchUrlUpdate("followurl", args.urlPart);
    //   if (!evResponse) { return "BLOCKED"; }
    //   if (evResponse.pageRefreshCancelled) {  
    //     if (evResponse.newUrl.href !== location.href) { 
    //       // can not cancel refresh with host change
    //       if (location.host !== evResponse.newUrl.host) {
    //         window.location.href = evResponse.newUrl.href as any;
    //         return;
    //       } else {
    //         if (args.mode === "push") {
    //           history.pushState(null, "", args.urlPart);
    //         } else if (args.mode === "replace") {
    //           history.replaceState(null, "", args.urlPart);
    //         }
    //         this.state.set("url", new URL(args.urlPart));
    //       }
    //     }
    //     return "CAPTURED";
    //   }
    //   if (args.deferLocationChange) {
    //     return "DEFERRED";
    //   } else {
    //     window.location.href = NavigationEvent.createFullUrl(args.urlPart).href as any;
    //   }
    // }
    newNavEvent(name, newUrl, dispatch = false) {
        const ev = new NavigationEvent(name, { cancelable: true }, newUrl);
        if (dispatch) {
            window.dispatchEvent(ev);
        }
        return ev;
    }
    initListeners() {
        window.addEventListener('popstate', () => {
            this.newNavEvent("locationchange", location.href, true);
        });
        // Some strange circular reference exists somewhere
        setTimeout(() => {
            El_Tool_1.onDomReady(() => {
                setTimeout(() => {
                    const hash = location.hash;
                    const target = El_Tool_1.byId(hash.slice(1));
                    if (!!target) {
                        target.scrollIntoView({ behavior: "auto" });
                    }
                }, 200);
            });
        }, 0);
        let lastHash;
        this.onUrlChange((ev) => {
            if (ev.hashFocusCancelled !== true
                && ev.newUrl.hash !== lastHash) {
                lastHash = ev.newUrl.hash;
                requestAnimationFrame(() => requestAnimationFrame(() => {
                    const target = El_Tool_1.byId(lastHash.slice(1));
                    if (!!target) {
                        target.scrollIntoView({ behavior: "auto" });
                    }
                }));
            }
        });
        helium_sdx_1.derive({
            anchor: "NO_ANCHOR",
            fn: () => {
                const url = this.getUrl();
                const urlParams = new URLSearchParams(url.search);
                urlParams.forEach((value, key) => {
                    this.state.urlParams[key] = value;
                });
                this.state.hash = url.hash;
            }
        });
    }
}
exports._UrlState = _UrlState;
exports.UrlState = new _UrlState();
exports.onUrlChange = exports.UrlState.onUrlChange.bind(exports.UrlState);
exports.observeUrl = exports.UrlState.observeUrl.bind(exports.UrlState);
exports.followUrl = exports.UrlState.followUrl.bind(exports.UrlState);
exports.replaceUrl = exports.UrlState.replaceUrl.bind(exports.UrlState);
//# sourceMappingURL=Navigation.js.map