import { IElProps, Innards, NameOrPropsOrInnards, PropsOrInnards, ManyStaticInnards, BaseNode } from './El-Tool';
import { Source } from 'helium-sdx';
/*****************************
* REGULAR CREATION SHORTHANDS
******************************/
/** Typical first factory arg.  Either className string, or factory props */
/** Typical second factory arg.  Either factory props, or innards to append */
/** Typical factory arguments */
export declare type StandardArgs<PROP = IElProps, INNARD = Innards> = [] | [NameOrPropsOrInnards<PROP>] | [NameOrPropsOrInnards<PROP>, PropsOrInnards<PROP>] | [NameOrPropsOrInnards<PROP>, PropsOrInnards<PROP>, INNARD];
/** Typical factory arguments */
export declare type NoClassArgs<PROP = IElProps, INNARD = Innards> = [] | [PropsOrInnards<PROP>] | [PropsOrInnards<PROP>, INNARD];
export declare type CoP<PROP = IElProps> = NameOrPropsOrInnards<PROP>;
export declare type PoI<PROP = IElProps> = PropsOrInnards<PROP>;
export declare type Inn = Innards;
declare type ElFunction<OUT extends ChildNode, PROPS extends IElProps<OUT> = IElProps<OUT>, INN = Innards> = {
    (): OUT;
    (className: string): OUT;
    (props: PROPS): OUT;
    (className: string, props: PROPS): OUT;
    (innards: Exclude<INN, string | BaseNode>): OUT;
    (className: string, innards: INN): OUT;
    (props: PROPS, innards: INN): OUT;
    (className: string, props: PROPS, innards: INN): OUT;
    (nameOrPropsOrInnards?: string | PROPS | Exclude<INN, string | BaseNode>, propsOrInnards?: PROPS | Exclude<INN, BaseNode>, innards?: INN): OUT;
};
/** Helper function for any factories which have no special props */
export declare function _simpleFactory<K extends keyof HTMLElementTagNameMap, PROP = IElProps>(tagName: K, [arg1, arg2, arg3]: StandardArgs<PROP>): HTMLElementTagNameMap[K];
/** Creates a div from the StandardArgs.
 * @importance 21 */
export declare const div: ElFunction<HTMLDivElement, IElProps<HTMLDivElement>, Innards>;
/** Creates a <span> element
 * @importance 14
*/
export declare const span: ElFunction<HTMLSpanElement, IElProps<HTMLSpanElement>, Innards>;
/** Creates a <pre> element
 * @importance 13
*/
export declare const pre: ElFunction<HTMLPreElement, IElProps<HTMLPreElement>, Innards>;
/** Creates a <ul> element
 * @importance 13
*/
export declare const ul: ElFunction<HTMLUListElement, IElProps<HTMLUListElement>, Innards>;
/** Creates a <ol> element
 * @importance 13
*/
export declare const ol: ElFunction<HTMLOListElement, IElProps<HTMLOListElement>, Innards>;
/** Creates a <li> element
 * @importance 13
*/
export declare const li: ElFunction<HTMLLIElement, IElProps<HTMLLIElement>, Innards>;
export declare type TableRows = ManyStaticInnards | Array<ManyStaticInnards>;
export declare type TableInnards = TableRows | (() => TableRows);
export declare function table(namePropChild?: CoP | TableInnards, propChild?: IElProps | TableInnards, child?: TableInnards): HTMLTableElement;
export declare function tr(namePropChild?: CoP | ManyStaticInnards, propChild?: IElProps | ManyStaticInnards, child?: ManyStaticInnards): HTMLTableRowElement;
/** Creates a <td> element
 * @importance 13
*/
export declare function td(...[classOrPropOrChild, propOrChild, child]: StandardArgs): HTMLTableDataCellElement;
export interface IImageProps extends IElProps<HTMLImageElement> {
    src: string;
}
/** Factory function img() can take special props
 * @importance 14
 * @prop.src Is the src attribute for an <img src=""> element*/
export declare function img(...[classOrPropOrChild, propOrChild, child]: StandardArgs<IImageProps>): HTMLImageElement;
export interface IButtonProps extends IElProps<HTMLButtonElement> {
    onPush: () => any;
}
/** Factory function button() can take special props
 * @importance 14
 * @prop.onPush Is a function which will get called onTap or onClick*/
export declare function button(...[classOrPropOrChild, propOrChild, child]: StandardArgs<IButtonProps>): HTMLButtonElement;
/** Factory function select() can take special props
 * @importance 13
 * @prop.onValueChange Is a function which will get called whenever the value of the input changes*/
export interface ISelectElProps extends IElProps<HTMLSelectElement> {
    onInput?: (value: string) => any;
}
declare type Printable = string | number;
export declare type ISelectElInnards = Array<HTMLOptionElement | Printable | (() => Printable)>;
export declare function select(classNameOrProps?: string | ISelectElProps | ISelectElInnards, propsOrInnards?: ISelectElProps | ISelectElInnards, maybeInnards?: ISelectElInnards): HTMLSelectElement;
/** Factory function inputTextbox() can take special props
 * @importance 13
 * @prop.onValueChange Is a function which will get called whenever the value of the input changes*/
export interface IInputTextboxProps extends IElProps<HTMLInputElement> {
    type?: "number" | "text";
    onInput?: (value: string) => any;
    onValueEntered?: (value: string) => any;
}
export declare type InputTextboxInnards = string | Source<string> | (() => string);
export declare function inputTextbox(classNameOrProps?: string | IInputTextboxProps | InputTextboxInnards, propsOrInnards?: IInputTextboxProps | InputTextboxInnards, maybeInnards?: InputTextboxInnards): HTMLInputElement;
/** Creates a <i> element
 * @importance 14
*/
export declare const italic: ElFunction<HTMLElement, IElProps<HTMLElement>, Innards>;
/** Creates a <b> element
 * @importance 14
*/
export declare const bold: ElFunction<HTMLElement, IElProps<HTMLElement>, Innards>;
/** Create an unmodified hr element
 * @importance 13
*/
export declare const hr: ElFunction<HTMLHRElement, IElProps<HTMLHRElement>, never>;
/** Create an unmodified br element
 * @importance 14
*/
export declare function br(size?: number, props?: IElProps<HTMLBRElement>): HTMLBRElement;
/** Creates a <h1> element
 * @importance 13
*/
export declare const h1: ElFunction<HTMLHeadingElement, IElProps<HTMLHeadingElement>, Innards>;
/** Creates a <h2> element
 * @importance 13
*/
export declare const h2: ElFunction<HTMLHeadingElement, IElProps<HTMLHeadingElement>, Innards>;
/** Creates a <h3> element
 * @importance 13
*/
export declare const h3: ElFunction<HTMLHeadingElement, IElProps<HTMLHeadingElement>, Innards>;
/** Creates a <h4> element
 * @importance 13
*/
export declare const h4: ElFunction<HTMLHeadingElement, IElProps<HTMLHeadingElement>, Innards>;
/** Creates a <h5> element
 * @importance 13
*/
export declare const h5: ElFunction<HTMLHeadingElement, IElProps<HTMLHeadingElement>, Innards>;
/** Creates a <h6> element
 * @importance 13
*/
export declare const h6: ElFunction<HTMLHeadingElement, IElProps<HTMLHeadingElement>, never>;
export {};
