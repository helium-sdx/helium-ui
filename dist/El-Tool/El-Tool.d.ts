/**************************
*. BASIC HELPER FUNCTIONS
***************************/
/** Short for document.getElementById()
 * @importance 14
 * @returnVar elementOfId */
export declare function byId(id: string): HTMLElement;
export declare function on(eventName: string, ref: EventTarget, cb: (ev: Event) => any): void;
/** Short for document.addEventListener("DOMContentLoaded").
 * It is recommended you start rendering before the dom is ready, then use this
 * event listener to add your content to the page.
 * @importance 20
 * @eg const appUi = div("App", "I'm the app");\
 * onDomReady(() => document.body.appendChild(appUi)));
*/
export declare function onDomReady(cb: () => void): void;
/** Acts similar to array.splice, but for an Element.classList. Can add/remove multiple
 * class names at once with either an array or string with spaces (eg. "big red dog")
 * @importance 10
 * @returnVar sameElement
 * @param.removeClasses classes which will be removed
 * @param.addClasses classes which element will have after function runs */
export declare function classSplice<ROOT extends Element = Element>(element: ROOT, removeClasses: string | string[] | null, addClasses?: string | string[]): ROOT;
/** Short for classSplice(element, null, addClasses).  Can be used to
 * add a class to something without affecting its render code.
 * @importance 15
 * @returnVar sameElement
 * @eg const sectionUi = renderSection();\
 * div("Eg", addClass("sectionName", sectionUi)) */
export declare function addClass(addClasses: string | string[], element: Element): Element;
/** Boolean switcher for adding and removing classes to Element.classList.
 * @importance 15
 * @eg haveClass(div, "selected", div === selectedItem)
 * @param.className class or classes to add/remove.  Can be array, or space separated string for groups.
 * @param.addNotRemove whether the element should or should not have these classes.
 * @returnVar sameElement*/
export declare function haveClass<ROOT extends Element = Element>(element: ROOT, className: string | string[], addNotRemove: boolean): ROOT;
export declare function ddxClass(target: HTMLElement, ddx: () => string | string[]): void;
/** Shorthand way to remove all children from an element
 * @importance 6
*/
export declare function removeChildren<ROOT extends BaseNode = BaseNode>(parent: ROOT): ROOT;
/** Shorthand way to set styles on element
 * @importance 11
 * @param.style Can be string to overwrite all inline styles (<a style="...">)
 * or an object using camel case to only overwrite specific properties.
 * @eg setStyle(body, "color: #111; background-color: orange;");\
 * setStyle(body, {\
 *   color: "#111",\
 *   backgroundColor: "orange",\
 * }); */
export declare function setStyle<ROOT extends HTMLElement = HTMLElement>(root: ROOT, style: Partial<CSSStyleDeclaration> | string): ROOT;
export interface IToucherPosition {
    top: number;
    left: number;
}
export declare type ToucherMoveEvent = (MouseEvent | TouchEvent) & {
    positions: Array<IToucherPosition>;
};
export declare function onToucherMove(root: Element, cb: (ev: ToucherMoveEvent) => EventResponse): void;
export declare type ToucherEnterExitEvent = (MouseEvent | TouchEvent) & {
    isEnterEvent: boolean;
};
export declare function onToucherEnterExit(root: Element, cb: (ev: ToucherEnterExitEvent) => EventResponse): void;
export declare function onTouch(root: Element, cb: (ev: MouseEvent) => EventResponse): void;
/**************************
*. TYPES / INTERFACES
***************************/
export declare type RawHTML = {
    hackableHTML: string;
};
export declare type BaseNode = ChildNode;
export declare type StaticInnard = string | BaseNode | RawHTML | number | null | false | void | undefined | (Array<StaticInnard> | (() => StaticInnard));
export declare type SingleInnard = StaticInnard;
declare type NonStringStaticInnard = Exclude<StaticInnard, string>;
export declare type ManyStaticInnards = SingleInnard | Array<SingleInnard>;
export declare type Innards = ManyStaticInnards | (() => ManyStaticInnards);
export declare type NonPrimativeInnards = BaseNode | Array<SingleInnard> | (() => ManyStaticInnards);
export declare type AugmentCb<CB_ARGS extends Array<any> = [], CB_RETURN = void> = (ref: Element, ...args: CB_ARGS) => CB_RETURN;
declare type EventResponse = boolean | void | any;
/**************************
*. TYPES CHECKS
***************************/
/** Check if thing is DOM appendable node
 * @returnVar checkMeIsNode */
export declare function isNode(checkMe: any): checkMe is BaseNode;
export declare function isHTMLElement(checkMe: any): checkMe is HTMLElement;
export declare function isTextNode(checkMe: ChildNode): checkMe is Text;
/** Check if thing reprensents raw html.  WARNING: Always double check raw html for injection.
 * @returnVar checkMeIsRawHTML */
export declare function isRawHTML(checkMe: any): checkMe is RawHTML;
/** Check if thing represents rendering props for el-tool
 * @returnVar checkMeIsProps */
export declare function isProps(checkMe: any): checkMe is IElProps;
/*************************
*. MAIN CREATION FN
*************************/
/** Main function at the root of el-tool (and Helium).  First standardizes output,
 * into rendering properties and innards-to-append.  Next it creates an HTMLElement
 * and begins modifying it based off the props.  Then it resolves innards to
 * an Element list and appends them as children.  And finally, it returns the HTMLElement
 * it has created, modified, and added children to.
 * @importance 16
 * @param.tagName The name of the element to create (eg. "div" -> <div>).
 * @param.propsOrInnards Either the props by which to modify the element, or innards to append.
 * @param.innards Either the innards to append, or empty.*/
export declare function el<K extends keyof HTMLElementTagNameMap>(tagName: K, propsOrInnards?: IElProps | Innards, innards?: Innards): HTMLElementTagNameMap[K];
export declare type NameOrPropsOrInnards<PROPTYPE extends IElProps> = string | PROPTYPE | (NonStringStaticInnard) | Array<SingleInnard>;
export declare type PropsOrInnards<PROPTYPE extends IElProps> = PROPTYPE | Innards;
/** Takes standard three arguments and returns a object with unambiguos props and innards.
 * Helper function for el() and factories such as div, span, etc.
 * @importance 16
 * @returnVar {props, innards}*/
export declare function standardizeInput<PROPTYPE extends IElProps, INNARDS = Innards>(nameOrPropsOrInnards?: string | PROPTYPE | INNARDS, propsOrInnards?: PROPTYPE | INNARDS, maybeInnards?: INNARDS, isInnardsRazor?: (checkMe: PROPTYPE | INNARDS) => checkMe is INNARDS): {
    props: PROPTYPE;
    innards: INNARDS;
};
declare type EventHandler<TARGET extends EventTarget, EVENT_TYPES extends Event> = (event: EVENT_TYPES & {
    currentTarget: TARGET;
}) => EventResponse;
export interface IElProps<NODE extends BaseNode = HTMLElement> {
    id?: string;
    class?: string;
    title?: string;
    attr?: {
        [key: string]: string;
    };
    style?: Partial<CSSStyleDeclaration> | string;
    on?: {
        [key: string]: EventHandler<NODE, Event>;
    };
    onPush?: EventHandler<NODE, MouseEvent | TouchEvent>;
    onTouch?: EventHandler<NODE, MouseEvent>;
    onToucherMove?: EventHandler<NODE, ToucherMoveEvent>;
    onToucherEnterExit?: EventHandler<NODE, ToucherEnterExitEvent>;
    onClick?: EventHandler<NODE, MouseEvent>;
    onMouseDown?: EventHandler<NODE, MouseEvent>;
    onKeyDown?: EventHandler<NODE, KeyboardEvent>;
    onHoverChange?: (isHovered: boolean, event?: MouseEvent) => EventResponse;
    this?: {
        [key: string]: any;
    };
    innards?: Innards;
    ref?: (ref: NODE) => any;
    ddxClass?: () => string | string[];
    ddx?: (ref: NODE, keep?: {
        [key: string]: any;
    }) => any;
}
export declare function applyProps(props: IElProps, out: HTMLElement, innardsCb?: (ref: HTMLElement) => any): HTMLElement;
/** Removes all children, then appends innards
 * @related El-Tool-append
 * @importance 15
 * @eg setInnards(document.body, [\
 *   div("Section", "This is the first section"),\
 *   div("Section", "This is the second section"),\
 * ]);
*/
export declare function setInnards(root: BaseNode, innards: Innards): void;
/** Appends innards to root element.
 * @importance 14
 * @param.before Optional element to insert before.*/
export declare function append<ROOT extends BaseNode>(root: ROOT, innards: Innards, before?: ChildNode): ROOT;
/** Takes innards and converts them to DOM elements.  If the typeof innards is a function
 * that function will be managed by a RerenderReplacer
 * @returnVar childNodes */
export declare function convertInnardsToElements(innards: Innards): ChildNode[];
export declare function convertInnardsToRenders(innards: Innards): Array<ChildNode | ChildNode[]>;
/** Converts a single item from an innards array into DOM elements
 * @returnVar childNodes */
export declare function convertSingleInnardToNodes(child: SingleInnard): ChildNode[];
export {};
