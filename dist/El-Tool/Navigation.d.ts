import { IElProps } from "./El-Tool";
import { StandardArgs } from "./Factories";
/** Factory function a() can take special props
 * @importance 14
 * @prop.href Is the href attribute for an <a href=""> element*/
export interface IAnchorProps extends IElProps {
    href?: string | (() => string);
    target?: "_blank";
    disableHref?: boolean;
    allowTextSelect?: boolean;
}
export declare function anchor(...[classOrProp, propOrChild, child]: StandardArgs<IAnchorProps>): HTMLAnchorElement;
export declare function a(...args: StandardArgs<IAnchorProps>): HTMLAnchorElement;
export declare function externalLink(...[classOrProp, propOrChild, child]: StandardArgs<IAnchorProps>): HTMLAnchorElement;
/** locationchange is triggered for every location change and
 * followurl is triggered when a url change is attempted (can be cancelled)
 */
export declare type NavigationEventName = "locationchange" | "followurl";
export declare class NavigationEvent extends Event {
    static createFullUrl(urlPart: string): URL;
    newUrl: URL;
    pageRefreshCancelled: boolean;
    hashFocusCancelled: boolean;
    constructor(name: NavigationEventName, props: EventInit, newUrl?: string);
    preventPageRefresh: () => void;
    preventHashFocus: () => void;
}
export interface IUrlUpdateArgs {
    urlPart: string;
    preventHashFocus?: boolean;
    mode?: "push" | "replace";
    deferLocationChange?: boolean;
}
export declare class _UrlState {
    protected state: import("../Sourcify").SourcifiedObject<{
        url: URL;
        urlParams: Record<string, string>;
        hash: string;
    }>;
    constructor();
    getUrl(): import("../Sourcify").SourcifiedObject<URL>;
    get href(): string;
    get host(): string;
    get hostname(): string;
    get pathname(): string;
    get hash(): string;
    get urlParams(): import("../Sourcify").SourcifiedObject<Record<string, string>>;
    followUrl(newUrl: string): "CAPTURED" | "BLOCKED" | "DEFERRED";
    update(argsOrUrl: string | IUrlUpdateArgs): "CAPTURED" | "BLOCKED" | "DEFERRED";
    replaceUrl(newUrl: string): "CAPTURED" | "BLOCKED" | "DEFERRED";
    onUrlChange(cb: (ev: NavigationEvent) => any): void;
    observeUrl(cb: (ev: NavigationEvent) => any): void;
    protected newNavEvent(name: NavigationEventName, newUrl: string, dispatch?: boolean): NavigationEvent;
    protected initListeners(): void;
}
export declare const UrlState: _UrlState;
export declare const onUrlChange: (cb: (ev: NavigationEvent) => any) => void;
export declare const observeUrl: (cb: (ev: NavigationEvent) => any) => void;
export declare const followUrl: (newUrl: string) => "CAPTURED" | "BLOCKED" | "DEFERRED";
export declare const replaceUrl: (newUrl: string) => "CAPTURED" | "BLOCKED" | "DEFERRED";
