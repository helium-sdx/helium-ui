"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.h6 = exports.h5 = exports.h4 = exports.h3 = exports.h2 = exports.h1 = exports.br = exports.hr = exports.bold = exports.italic = exports.inputTextbox = exports.select = exports.button = exports.img = exports.td = exports.tr = exports.table = exports.li = exports.ol = exports.ul = exports.pre = exports.span = exports.div = exports._simpleFactory = void 0;
const El_Tool_1 = require("./El-Tool");
const helium_sdx_1 = require("helium-sdx");
/** Helper function for any factories which have no special props */
function _simpleFactory(tagName, [arg1, arg2, arg3]) {
    const { props, innards } = El_Tool_1.standardizeInput(arg1, arg2, arg3);
    return El_Tool_1.el(tagName, props, innards);
}
exports._simpleFactory = _simpleFactory;
/** Creates a div from the StandardArgs.
 * @importance 21 */
exports.div = function (...args) {
    return _simpleFactory("div", args);
};
/** Creates a <span> element
 * @importance 14
*/
exports.span = function (...args) {
    return _simpleFactory("span", args);
};
/** Creates a <pre> element
 * @importance 13
*/
exports.pre = function (...args) {
    return _simpleFactory("pre", args);
};
/** Creates a <ul> element
 * @importance 13
*/
exports.ul = function (...args) {
    return _simpleFactory("ul", args);
};
/** Creates a <ol> element
 * @importance 13
*/
exports.ol = function (...args) {
    return _simpleFactory("ol", args);
};
/** Creates a <li> element
 * @importance 13
*/
exports.li = function (...args) {
    return _simpleFactory("li", args);
};
// const by: TableInnards = () => test;
function table(namePropChild, propChild, child) {
    const { props, innards } = El_Tool_1.standardizeInput(namePropChild, propChild, child);
    const rows = El_Tool_1.convertInnardsToRenders(innards);
    return El_Tool_1.el("table", props, rows.map((row) => toTr(row)));
    function toTr(row) {
        if (Array.isArray(row) && row.length === 1) {
            row = row.pop();
        }
        if (El_Tool_1.isHTMLElement(row) && (row.tagName === "TR" || row.nodeName === "#comment")) {
            return row;
        }
        if (typeof row === "function") {
            return () => toTr(row());
        }
        return tr(row);
    }
}
exports.table = table;
function tr(namePropChild, propChild, child) {
    const { props, innards } = El_Tool_1.standardizeInput(namePropChild, propChild, child);
    const cols = El_Tool_1.convertInnardsToRenders(innards);
    return El_Tool_1.el("tr", props, cols.map((data) => toTd(data)));
    function toTd(data) {
        if (Array.isArray(data) && data.length === 1) {
            data = data.pop();
        }
        if (El_Tool_1.isHTMLElement(data) && (data.tagName === "TD" || data.nodeName === "#comment")) {
            return data;
        }
        if (typeof data === "function") {
            return () => toTd(data());
        }
        return El_Tool_1.el("td", data);
    }
}
exports.tr = tr;
/** Creates a <td> element
 * @importance 13
*/
function td(...[classOrPropOrChild, propOrChild, child]) {
    return _simpleFactory("td", [classOrPropOrChild, propOrChild, child]);
}
exports.td = td;
/** Factory function img() can take special props
 * @importance 14
 * @prop.src Is the src attribute for an <img src=""> element*/
function img(...[classOrPropOrChild, propOrChild, child]) {
    const { props, innards } = El_Tool_1.standardizeInput(classOrPropOrChild, propOrChild, child);
    props.attr = props.attr || {};
    props.attr.src = props.src;
    return El_Tool_1.el("img", props, innards);
}
exports.img = img;
/** Factory function button() can take special props
 * @importance 14
 * @prop.onPush Is a function which will get called onTap or onClick*/
function button(...[classOrPropOrChild, propOrChild, child]) {
    return _simpleFactory("button", [classOrPropOrChild, propOrChild, child]);
}
exports.button = button;
function select(classNameOrProps, propsOrInnards, maybeInnards) {
    const { props, innards } = El_Tool_1.standardizeInput(classNameOrProps, propsOrInnards, maybeInnards, (a) => Array.isArray(a));
    props.on = props.on || {};
    let currentVal;
    if (props.onInput) {
        const { onInput } = props;
        const onInputListener = props.on.input;
        props.on.input = (ev) => {
            const newVal = out.value;
            if (newVal !== currentVal) {
                currentVal = newVal;
                onInput(newVal);
            }
            return !onInputListener || onInputListener(ev);
        };
    }
    const out = El_Tool_1.el("select", props, innards.map((innard => {
        let option;
        if (innard instanceof HTMLElement) {
            option = innard;
        }
        else if (typeof innard === "function") {
            option = El_Tool_1.el("option", { ddx: (ref) => {
                    ref.innerText = String(innard());
                    if (ref.selected) {
                        // update value??
                    }
                } });
        }
        else {
            option = El_Tool_1.el("option", String(innard));
        }
        if (option.selected) {
            currentVal = option.value;
        }
        return option;
    })));
    return out;
}
exports.select = select;
function inputTextbox(classNameOrProps, propsOrInnards, maybeInnards) {
    const { props, innards } = El_Tool_1.standardizeInput(classNameOrProps, propsOrInnards, maybeInnards, (a) => a instanceof helium_sdx_1.Source);
    props.on = props.on || {};
    if (innards instanceof helium_sdx_1.Source) {
        const onInputArg = props.onInput;
        props.onInput = (value) => {
            innards.set(value);
            onInputArg && onInputArg(value);
        };
    }
    if (props.onInput) {
        const { onInput } = props;
        const onInputListener = props.on.input;
        let oldVal = innards;
        props.on.input = (ev) => {
            const newVal = out.value;
            if (newVal !== oldVal) {
                oldVal = newVal;
                onInput(newVal);
            }
            return !onInputListener || onInputListener(ev);
        };
    }
    if (props.onValueEntered) {
        const onChange = props.on.change;
        const { onValueEntered } = props;
        props.on.change = (ev) => {
            onValueEntered(ev.target.value);
            return !onChange || onChange(ev);
        };
    }
    const out = El_Tool_1.el("input", props);
    out.type = props.type || "text";
    if (innards) {
        if (typeof innards === "string") {
            out.value = innards;
        }
        else if (innards instanceof helium_sdx_1.Source) {
            helium_sdx_1.derive(() => out.value = innards.get());
        }
        else {
            helium_sdx_1.derive(() => out.value = innards());
        }
    }
    return out;
}
exports.inputTextbox = inputTextbox;
/** Creates a <i> element
 * @importance 14
*/
exports.italic = function (...args) {
    return _simpleFactory("i", args);
};
/** Creates a <b> element
 * @importance 14
*/
exports.bold = function (...args) {
    return _simpleFactory("b", args);
};
/** Create an unmodified hr element
 * @importance 13
*/
exports.hr = function (...args) {
    return _simpleFactory("hr", args);
};
/** Create an unmodified br element
 * @importance 14
*/
function br(size, props = {}) {
    if (!size) {
        return El_Tool_1.el("br", props);
    }
    props.style = {
        display: "block",
        height: `${size}em`
    };
    return El_Tool_1.el("em-br", props);
}
exports.br = br;
/** Creates a <h1> element
 * @importance 13
*/
exports.h1 = function (...args) {
    return _simpleFactory("h1", args);
};
/** Creates a <h2> element
 * @importance 13
*/
exports.h2 = function (...args) {
    return _simpleFactory("h2", args);
};
/** Creates a <h3> element
 * @importance 13
*/
exports.h3 = function (...args) {
    return _simpleFactory("h3", args);
};
/** Creates a <h4> element
 * @importance 13
*/
exports.h4 = function (...args) {
    return _simpleFactory("h4", args);
};
/** Creates a <h5> element
 * @importance 13
*/
exports.h5 = function (...args) {
    return _simpleFactory("h5", args);
};
/** Creates a <h6> element
 * @importance 13
*/
exports.h6 = function (...args) {
    return _simpleFactory("h6", args);
};
//# sourceMappingURL=Factories.js.map