"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.convertSingleInnardToNodes = exports.convertInnardsToRenders = exports.convertInnardsToElements = exports.append = exports.setInnards = exports.applyProps = exports.standardizeInput = exports.el = exports.isProps = exports.isRawHTML = exports.isTextNode = exports.isHTMLElement = exports.isNode = exports.onTouch = exports.onToucherEnterExit = exports.onToucherMove = exports.setStyle = exports.removeChildren = exports.ddxClass = exports.haveClass = exports.addClass = exports.classSplice = exports.onDomReady = exports.on = exports.byId = void 0;
const Derivations_1 = require("../Derivations");
const HeliumBase_1 = require("../HeliumBase");
/**************************
*. BASIC HELPER FUNCTIONS
***************************/
/** Short for document.getElementById()
 * @importance 14
 * @returnVar elementOfId */
function byId(id) { return document.getElementById(id); }
exports.byId = byId;
function on(eventName, ref, cb) {
    ref.addEventListener(eventName, cb);
}
exports.on = on;
/** Short for document.addEventListener("DOMContentLoaded").
 * It is recommended you start rendering before the dom is ready, then use this
 * event listener to add your content to the page.
 * @importance 20
 * @eg const appUi = div("App", "I'm the app");\
 * onDomReady(() => document.body.appendChild(appUi)));
*/
function onDomReady(cb) {
    if (domIsReady) {
        cb();
        return;
    }
    window.addEventListener("DOMContentLoaded", cb);
}
exports.onDomReady = onDomReady;
let domIsReady = false;
(typeof window !== "undefined") && onDomReady(() => domIsReady = true);
/** Acts similar to array.splice, but for an Element.classList. Can add/remove multiple
 * class names at once with either an array or string with spaces (eg. "big red dog")
 * @importance 10
 * @returnVar sameElement
 * @param.removeClasses classes which will be removed
 * @param.addClasses classes which element will have after function runs */
function classSplice(element, removeClasses, addClasses) {
    if (removeClasses) {
        const remList = Array.isArray(removeClasses) ? removeClasses : removeClasses.split(/\s+/g);
        remList.filter((it) => !!it).forEach((className) => element.classList.remove(className));
    }
    if (addClasses) {
        const addList = Array.isArray(addClasses) ? addClasses : addClasses.split(/\s+/g);
        addList.filter((it) => !!it).forEach((className) => element.classList.add(className));
    }
    return element;
}
exports.classSplice = classSplice;
/** Short for classSplice(element, null, addClasses).  Can be used to
 * add a class to something without affecting its render code.
 * @importance 15
 * @returnVar sameElement
 * @eg const sectionUi = renderSection();\
 * div("Eg", addClass("sectionName", sectionUi)) */
function addClass(addClasses, element) {
    return classSplice(element, null, addClasses);
}
exports.addClass = addClass;
/** Boolean switcher for adding and removing classes to Element.classList.
 * @importance 15
 * @eg haveClass(div, "selected", div === selectedItem)
 * @param.className class or classes to add/remove.  Can be array, or space separated string for groups.
 * @param.addNotRemove whether the element should or should not have these classes.
 * @returnVar sameElement*/
function haveClass(element, className, addNotRemove) {
    return addNotRemove ? (classSplice(element, null, className)) : (classSplice(element, className, undefined));
}
exports.haveClass = haveClass;
function ddxClass(target, ddx) {
    let lastClassList;
    Derivations_1.anchorDeriverToDomNode(target, () => {
        const removeUs = lastClassList;
        classSplice(target, removeUs, lastClassList = ddx());
    }, "props.ddxClass").run();
}
exports.ddxClass = ddxClass;
/** Shorthand way to remove all children from an element
 * @importance 6
*/
function removeChildren(parent) {
    let child = parent.firstChild;
    while (!!child) {
        child.remove();
        child = parent.firstChild;
    }
    return parent;
}
exports.removeChildren = removeChildren;
/** Shorthand way to set styles on element
 * @importance 11
 * @param.style Can be string to overwrite all inline styles (<a style="...">)
 * or an object using camel case to only overwrite specific properties.
 * @eg setStyle(body, "color: #111; background-color: orange;");\
 * setStyle(body, {\
 *   color: "#111",\
 *   backgroundColor: "orange",\
 * }); */
function setStyle(root, style) {
    if (typeof style === "string") {
        root.style.cssText = style;
    }
    else {
        Object.keys(style).forEach((styleName) => root.style[styleName] = style[styleName]);
    }
    return root;
}
exports.setStyle = setStyle;
function onToucherMove(root, cb) {
    const getPosFromClientXY = (x, y) => {
        const clientRect = root.getBoundingClientRect();
        return {
            left: x - clientRect.left,
            top: y - clientRect.top,
        };
    };
    root.addEventListener("mousemove", (ev) => {
        const toucherMoveEvent = ev;
        toucherMoveEvent.positions = [
            getPosFromClientXY(ev.clientX, ev.clientY)
        ];
        cb(toucherMoveEvent);
    });
    root.addEventListener("touchmove", (ev) => {
        const toucherMoveEvent = ev;
        const clientRect = root.getBoundingClientRect();
        toucherMoveEvent.positions = Array.from(ev.touches).map((touch) => getPosFromClientXY(touch.clientX, touch.clientY));
        cb(toucherMoveEvent);
        ev.preventDefault();
    });
}
exports.onToucherMove = onToucherMove;
function onToucherEnterExit(root, cb) {
    const modEvent = (ev, isEnterEvent) => {
        ev.isEnterEvent = isEnterEvent;
        return ev;
    };
    root.addEventListener("mouseenter", (ev) => cb(modEvent(ev, true)));
    root.addEventListener("mouseleave", (ev) => cb(modEvent(ev, false)));
    root.addEventListener("touchstart", (ev) => {
        cb(modEvent(ev, true));
        ev.preventDefault();
    });
    root.addEventListener("touchend", (ev) => {
        cb(modEvent(ev, false));
        ev.preventDefault();
    });
}
exports.onToucherEnterExit = onToucherEnterExit;
function onTouch(root, cb) {
    root.addEventListener("mousedown", (event) => {
        if (event.buttons == 1 || event.buttons == 3) {
            cb(event);
        }
    });
    root.addEventListener("touchstart", (event) => cb(event));
}
exports.onTouch = onTouch;
/**************************
*. TYPES CHECKS
***************************/
/** Check if thing is DOM appendable node
 * @returnVar checkMeIsNode */
function isNode(checkMe) {
    return checkMe instanceof Node;
}
exports.isNode = isNode;
function isHTMLElement(checkMe) {
    return "nodeName" in checkMe;
}
exports.isHTMLElement = isHTMLElement;
function isTextNode(checkMe) {
    return isNode(checkMe) && checkMe.nodeType === checkMe.TEXT_NODE;
}
exports.isTextNode = isTextNode;
/** Check if thing reprensents raw html.  WARNING: Always double check raw html for injection.
 * @returnVar checkMeIsRawHTML */
function isRawHTML(checkMe) {
    return (checkMe && typeof checkMe === "object") && checkMe.hasOwnProperty("hackableHTML");
}
exports.isRawHTML = isRawHTML;
/** Check if thing represents rendering props for el-tool
 * @returnVar checkMeIsProps */
function isProps(checkMe) {
    return (checkMe && typeof checkMe === "object")
        && !Array.isArray(checkMe)
        && !isNode(checkMe)
        && !isRawHTML(checkMe);
}
exports.isProps = isProps;
function el(tagName, propsOrInnards, innards) {
    const input = standardizeInput(null, propsOrInnards, innards);
    return __createEl(tagName, input.props, input.innards);
}
exports.el = el;
/** Takes standard three arguments and returns a object with unambiguos props and innards.
 * Helper function for el() and factories such as div, span, etc.
 * @importance 16
 * @returnVar {props, innards}*/
function standardizeInput(nameOrPropsOrInnards, propsOrInnards, maybeInnards, isInnardsRazor) {
    let innards = maybeInnards;
    let props;
    let name;
    if (nameOrPropsOrInnards) {
        if (typeof nameOrPropsOrInnards === "string") {
            name = nameOrPropsOrInnards;
        }
        else {
            if (maybeInnards !== undefined) {
                throw new Error("Can not define first and third argument if first is not a string");
            }
            maybeInnards = propsOrInnards;
            propsOrInnards = nameOrPropsOrInnards;
        }
    }
    if (propsOrInnards) {
        const isInnards = isInnardsRazor ? isInnardsRazor(propsOrInnards) : !isProps(propsOrInnards);
        if (isInnards) {
            if (maybeInnards !== undefined) {
                throw new Error("Can not define third argument if second is innards");
            }
            innards = propsOrInnards;
        }
        else {
            props = propsOrInnards;
            innards = maybeInnards;
        }
    }
    props = props || {};
    if (name) {
        props.class = props.class || "";
        props.class += (props.class ? " " : "") + name;
    }
    return { props, innards };
}
exports.standardizeInput = standardizeInput;
function applyProps(props, out, innardsCb) {
    if (props) {
        // event stuff
        if (props.on) {
            const eventListeners = props.on || {};
            Object.keys(eventListeners).forEach((eventType) => out.addEventListener(eventType, eventListeners[eventType]));
        }
        if (props.onClick) {
            out.addEventListener("click", props.onClick);
        }
        if (props.onKeyDown) {
            out.addEventListener("keydown", props.onKeyDown);
        }
        if (props.onMouseDown) {
            out.addEventListener("mousedown", props.onMouseDown);
        }
        if (props.onPush) {
            out.classList.add("onPush");
            out.addEventListener("click", props.onPush);
        }
        if (props.onTouch) {
            onTouch(out, props.onTouch);
        }
        if (props.onToucherMove) {
            onToucherMove(out, props.onToucherMove);
        }
        if (props.onToucherEnterExit) {
            onToucherEnterExit(out, props.onToucherEnterExit);
        }
        if (props.onHoverChange) {
            out.addEventListener("mouseover", (ev) => props.onHoverChange(true, ev));
            out.addEventListener("mouseout", (ev) => props.onHoverChange(false, ev));
        }
        // element html props, and element js props
        if (props.title) {
            out.title = props.title;
        }
        if (props.attr) {
            const attrs = props.attr || {};
            Object.keys(attrs).forEach((attr) => out.setAttribute(attr, attrs[attr]));
        }
        if (props.this) {
            const thisProps = props.this || {};
            Object.keys(thisProps).forEach((prop) => out[prop] = thisProps[prop]);
        }
        // classing
        if (props.class) {
            addClass(props.class, out);
        }
        if (props.id) {
            out.id = props.id;
        }
        // inline styles
        if (props.style) {
            setStyle(out, props.style);
        }
        // innards alternative
        if (props.innards !== undefined) {
            append(out, props.innards);
        }
    }
    // add all chidren, or innards
    if (innardsCb !== undefined) {
        innardsCb(out);
    }
    if (props) {
        if (props.ref) {
            props.ref(out);
        }
        if (props.ddxClass) {
            ddxClass(out, props.ddxClass);
        }
        if (props.ddx) {
            const keep = {};
            Derivations_1.anchorDeriverToDomNode(out, () => props.ddx(out, keep), "props.ddx").run();
        }
    }
    return out;
}
exports.applyProps = applyProps;
function __createEl(tagName, props, innards) {
    const out = document.createElement(tagName);
    applyProps(props, out, () => {
        if (innards !== undefined) {
            append(out, innards);
        }
    });
    if (HeliumBase_1.isHeliumHazed(out)) {
        out._helium.frozen = true;
    }
    return out;
}
let innardsWarningGiven;
/** Removes all children, then appends innards
 * @related El-Tool-append
 * @importance 15
 * @eg setInnards(document.body, [\
 *   div("Section", "This is the first section"),\
 *   div("Section", "This is the second section"),\
 * ]);
*/
function setInnards(root, innards) {
    if (root === document.body && !window._heliumNoWarnSetInnardsDocBody) {
        console.warn([
            `Suggestion: avoid using setInnards() on document body, and instead use append().`,
            `If you do not, anything added to the page outside of your root render is unsafe.  To disable this warning,`,
            `set window._heliumNoWarnSetInnardsDocBody = true`,
        ].join(" "));
    }
    removeChildren(root);
    append(root, innards);
}
exports.setInnards = setInnards;
/** Appends innards to root element.
 * @importance 14
 * @param.before Optional element to insert before.*/
function append(root, innards, before) {
    const addUs = convertInnardsToElements(innards);
    if (before) {
        addUs.forEach((node) => {
            try {
                root.insertBefore(node, before);
            }
            catch (err) {
                console.error("Can not insert childNode into: ", root);
                console.error("Tried to insert: ", node);
                console.error(err);
            }
        });
    }
    else {
        addUs.forEach((node) => {
            try {
                root.appendChild(node);
            }
            catch (err) {
                console.error("Can not append childNode to: ", root);
                console.error("Tried to append: ", node);
                console.error(err);
            }
        });
    }
    return root;
}
exports.append = append;
/** Takes innards and converts them to DOM elements.  If the typeof innards is a function
 * that function will be managed by a RerenderReplacer
 * @returnVar childNodes */
function convertInnardsToElements(innards) {
    const renders = convertInnardsToRenders(innards);
    const out = [];
    renders.forEach((render) => {
        if (Array.isArray(render)) {
            render.forEach((node) => out.push(node));
        }
        else {
            out.push(render);
        }
    });
    return out;
}
exports.convertInnardsToElements = convertInnardsToElements;
function convertInnardsToRenders(innards) {
    if (innards === undefined || innards === null) {
        return [];
    }
    if (typeof innards === "function") {
        const refresher = new Derivations_1.RerenderReplacer(innards);
        return refresher.getRender();
    }
    let childList = Array.isArray(innards) ? innards : [innards];
    return childList.flat().map((child) => convertSingleInnardToNodes(child));
}
exports.convertInnardsToRenders = convertInnardsToRenders;
/** Converts a single item from an innards array into DOM elements
 * @returnVar childNodes */
function convertSingleInnardToNodes(child) {
    if (typeof child === "function") {
        const replacer = new Derivations_1.RerenderReplacer(child);
        return replacer.getRender();
    }
    else {
        // return _convertSingleStaticInnardToNodes(child);
        let out = [];
        if (child === undefined || child === null) {
            return [];
        }
        if (typeof child === "number") {
            child = child + "";
        }
        if (isRawHTML(child)) {
            const template = document.createElement('div');
            template.innerHTML = child.hackableHTML;
            template.getRootNode();
            Array.from(template.childNodes).forEach((child) => out.push(child));
        }
        else if (typeof child === "string") {
            out.push(new Text(child));
        }
        else if (child) {
            if (Array.isArray(child)) {
                out = out.concat(convertSingleInnardToNodes(child));
            }
            else {
                out.push(child);
            }
        }
        return out;
    }
}
exports.convertSingleInnardToNodes = convertSingleInnardToNodes;
//# sourceMappingURL=El-Tool.js.map