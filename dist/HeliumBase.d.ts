/** Any object with storage area for helium logic.  Storage area is
 * always called "obj._helium"
 * @prop.rerenderReplacer Replacer placeholders have this property
 * @prop.freeze Function, or list of functions to called when element is frozen
 * @prop.unfreeze Function, or list of functions to called when element is unfrozen
 * @prop.moveMutation Optimization bool, used to ignore add/remove mutations when replacers shuffle items
 * @prop.isReplacer True if element is a replacer placeholder. */
export declare type IHeliumHazed<BASE> = BASE & {
    _helium: {
        rerenderReplacer?: any;
        freeze?: Array<() => any>;
        unfreeze?: Array<() => any>;
        frozen?: boolean;
        moveMutation?: boolean;
        isReplacer?: boolean;
    };
};
/** Add rookie._helium = {} if it doesn't exist already */
export declare function heliumHaze<BASE>(rookie: BASE): IHeliumHazed<BASE>;
/** Check if thing has storage area for Helium logic */
export declare function isHeliumHazed<BASE>(checkMe: BASE): checkMe is IHeliumHazed<BASE>;
export declare function changeFreezeState<TYPE>(hazed: IHeliumHazed<TYPE>, shouldBeFrozen: boolean): void;
