import { fusebox } from "fuse-box";

const bundler = fusebox({
  cache: false,
  target: "browser",
  webIndex: true,
  devServer: true,
  entry: "./TestPage.ts"
})

bundler.runDev();