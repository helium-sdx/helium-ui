(function() {
  var f = window.__fuse = window.__fuse || {};
  var modules = f.modules = f.modules || {}; f.dt = function (x) { return x && x.__esModule ? x : { "default": x }; };

  f.bundle = function(collection, fn) {
    for (var num in collection) {
      modules[num] = collection[num];
    }
    fn ? fn() : void 0;
  };
  f.c = {};
  f.r = function(id) {
    var cached = f.c[id];
    if (cached) return cached.m.exports;
    var module = modules[id];
    if (!module) {
      
      throw new Error('Module ' + id + ' was not found');
    }
    cached = f.c[id] = {};
    cached.exports = {};
    cached.m = { exports: cached.exports };
    module(f.r, cached.exports, cached.m);
    return cached.m.exports;
  }; 
})();
__fuse.bundle({

// test_page/TestPage.ts @1
1: function(__fusereq, exports, module){
exports.__esModule = true;
var src_1 = __fusereq(2);
const state = src_1.Sourcify({
  hat: "fez"
});
const app = renderApp();
src_1.onDomReady(() => {
  src_1.append(document.body, app);
  setTimeout(() => {
    state.hat = undefined;
  }, 1000);
});
function renderApp() {
  return src_1.div("TestApp", () => {
    return new URL("http://lala.com");
  });
}

},

// src/index.ts @2
2: function(__fusereq, exports, module){
exports.__esModule = true;
Object.assign(exports, __fusereq(3));
Object.assign(exports, __fusereq(4));
Object.assign(exports, __fusereq(5));
Object.assign(exports, __fusereq(6));
Object.assign(exports, __fusereq(7));
Object.assign(exports, __fusereq(8));

},

// src/El-Tool/index.ts @3
3: function(__fusereq, exports, module){
exports.__esModule = true;
Object.assign(exports, __fusereq(17));
Object.assign(exports, __fusereq(18));
Object.assign(exports, __fusereq(19));

},

// src/Derivations/index.ts @4
4: function(__fusereq, exports, module){
exports.__esModule = true;
Object.assign(exports, __fusereq(9));
Object.assign(exports, __fusereq(10));

},

// src/Sourcify/index.ts @5
5: function(__fusereq, exports, module){
exports.__esModule = true;
Object.assign(exports, __fusereq(11));
Object.assign(exports, __fusereq(12));
Object.assign(exports, __fusereq(13));
Object.assign(exports, __fusereq(14));

},

// src/HeliumBase.ts @6
6: function(__fusereq, exports, module){
function heliumHaze(rookie) {
  rookie._helium = rookie._helium || ({});
  return rookie;
}
exports.heliumHaze = heliumHaze;
function isHeliumHazed(checkMe) {
  return checkMe._helium;
}
exports.isHeliumHazed = isHeliumHazed;
function changeFreezeState(hazed, shouldBeFrozen) {
  const helium = hazed._helium;
  if (helium.frozen === shouldBeFrozen) {
    return;
  }
  helium.frozen = shouldBeFrozen;
  const fns = shouldBeFrozen ? helium.freeze : helium.unfreeze;
  if (fns) {
    fns.forEach(fn => fn());
  }
}
exports.changeFreezeState = changeFreezeState;

},

// src/Augments/index.ts @7
7: function(__fusereq, exports, module){
exports.__esModule = true;
Object.assign(exports, __fusereq(15));
Object.assign(exports, __fusereq(16));

},

// src/Derivations/Derivers/index.ts @9
9: function(__fusereq, exports, module){
exports.__esModule = true;
Object.assign(exports, __fusereq(23));
Object.assign(exports, __fusereq(24));
Object.assign(exports, __fusereq(25));

},

// src/Derivations/Sources/index.ts @10
10: function(__fusereq, exports, module){
exports.__esModule = true;
Object.assign(exports, __fusereq(27));
Object.assign(exports, __fusereq(28));
Object.assign(exports, __fusereq(26));

},

// src/Sourcify/SourceProxyObject.ts @11
11: function(__fusereq, exports, module){
exports.__esModule = true;
var SourceMap_1 = __fusereq(26);
var helium_sdx_1 = __fusereq(8);
class SourceProxyObject extends helium_sdx_1.SourceProxyObject {
  constructor(target) {
    super(target, new SourceMap_1.SourceMap());
  }
  getSrcMap() {
    return this.srcMap;
  }
  renderMap(renderFn) {
    this.refreshAll();
    return this.getSrcMap().renderMap(renderFn);
  }
  renderMapKeyed(renderFn) {
    this.refreshAll();
    return this.getSrcMap().renderMapKeyed(renderFn);
  }
}
exports.SourceProxyObject = SourceProxyObject;
;
exports.ASSERT_SHALLOW_TYPE_MATCH = {};

},

// src/Sourcify/SourceProxyArray.ts @12
12: function(__fusereq, exports, module){
exports.__esModule = true;
var SourceArray_1 = __fusereq(28);
var helium_sdx_1 = __fusereq(8);
class SourceProxyArray extends helium_sdx_1.SourceProxyArray {
  constructor(target) {
    super(target, new SourceArray_1.SourceArray());
  }
  getSrcArray() {
    return this.srcArray;
  }
  renderMap(renderFn) {
    this.refreshAll();
    return this.getSrcArray().renderMap(renderFn);
  }
  renderMapKeyed(renderFn) {
    this.refreshAll();
    return this.getSrcArray().renderMapKeyed(renderFn);
  }
}
exports.SourceProxyArray = SourceProxyArray;
exports.ASSERT_SHALLOW_TYPE_MATCH_ARRAY = {};

},

// src/Sourcify/SourceProxyShallowTypes.ts @13
13: function(__fusereq, exports, module){
function __() {
  ;
  const b = {};
  function testFn(testMe) {
    return null;
  }
  const out = b.a[3];
  const la = b.a.overwrite([]);
  const ba = b.a.sort();
  ba.renderMap(() => null);
  la.renderMap(() => null);
  const a = b.a.filter(() => true);
  const c = a[0];
  const d = b.get("a");
}

},

// src/Sourcify/SourceProxyUtilities.ts @14
14: function(__fusereq, exports, module){
exports.__esModule = true;
var El_Tool_1 = __fusereq(17);
var helium_sdx_1 = __fusereq(8);
let SourcificationManagerModded = false;
function Sourcify(target) {
  if (!SourcificationManagerModded) {
    const {SourceProxyArray} = __fusereq(12);
    const {SourceProxyObject} = __fusereq(11);
    helium_sdx_1.SourcificationManager.createSourceProxyArray = item => new SourceProxyArray(item);
    helium_sdx_1.SourcificationManager.createSourceProxyObject = item => new SourceProxyObject(item);
    SourcificationManagerModded = true;
  }
  return helium_sdx_1.Sourcify(target);
}
exports.Sourcify = Sourcify;
function Unsourcify(target) {
  return helium_sdx_1.Unsourcify(target);
}
exports.Unsourcify = Unsourcify;
function isProxied(checkMe) {
  return helium_sdx_1.isProxied(checkMe);
}
exports.isProxied = isProxied;
exports.renderMap = renderMap;
exports.renderMap = renderMap;
function renderMap(obj, renderFn) {
  return _anyRMap(false, obj, renderFn);
}
exports.renderMap = renderMap;
exports.renderMapKeyed = renderMapKeyed;
exports.renderMapKeyed = renderMapKeyed;
function renderMapKeyed(obj, renderFn) {
  return _anyRMap(true, obj, renderFn);
}
exports.renderMapKeyed = renderMapKeyed;
function _anyRMap(withKey, obj, renderFn) {
  if (isProxied(obj)) {
    if (withKey) {
      return obj.rkmap(renderFn);
    }
    return obj.rmap(renderFn);
  } else if (obj && typeof obj === "object") {
    const out = [];
    let renders;
    if (Array.isArray(obj)) {
      renders = obj.map((value, index) => renderFn(value, index));
    } else {
      renders = Object.entries(obj).map(([key, value]) => renderFn(value, key));
    }
    renders.forEach(render => {
      const nodesList = El_Tool_1.convertInnardsToElements(render);
      if (!nodesList || !nodesList.length) {
        return;
      }
      nodesList.forEach(node => out.push(node));
    });
    return out;
  }
}

},

// src/Augments/OnIntersect.ts @15
15: function(__fusereq, exports, module){
exports.__esModule = true;
var Factories_1 = __fusereq(18);
var El_Tool_1 = __fusereq(17);
let styleEl;
function assertStyling() {
  if (styleEl) {
    return;
  }
  styleEl = El_Tool_1.el("style");
  styleEl.innerHTML = `
    .hintRelative {
      position: relative;
    }

    .CoverCheckerPositioner {
      position: absolute;
      top: 0px;
      left: 0px;
      right: 0px;
      bottom: 0px;
      pointer-events: none;
      visibility: hidden;
    }
    
    .CoverChecker {
      position: sticky;
      height: 100vh;
      width: 100vw;
      top: 0px; 
    }
  `;
  document.head.prepend(styleEl);
}
function addIntersectionObserver(nodes, init, cb) {
  let thresholds;
  const isObject = init => {
    return init && typeof init === "object" && !Array.isArray(init);
  };
  if (isObject(init)) {
    thresholds = init.threshold;
  }
  if (!Array.isArray(thresholds)) {
    thresholds = [thresholds];
  }
  let startLength = thresholds.length;
  const normalThresholds = thresholds.map(it => {
    switch (it) {
      case "contained":
      case "maxFill":
        return 1;
      case "any":
        return 0;
    }
    return it;
  }).filter(it => typeof it === "number");
  const needsCoverMod = startLength > normalThresholds.length;
  if (!Array.isArray(nodes)) {
    nodes = [nodes];
  }
  if (needsCoverMod) {
    assertStyling();
    const checkUs = nodes.map(node => {
      let checkMe;
      const addMe = Factories_1.div("CoverCheckerPositioner", checkMe = Factories_1.div("CoverChecker"));
      node.classList.add("hintRelative");
      node.prepend(addMe);
      return checkMe;
    });
    const watcher = new IntersectionObserver(cb, {
      threshold: 1
    });
    checkUs.forEach(node => watcher.observe(node));
  }
  if (normalThresholds.length === 0) {
    return;
  }
  const options = isObject(init) ? init : {};
  options.threshold = normalThresholds;
  const watcher = new IntersectionObserver(cb, options);
  setTimeout(() => {
    nodes.forEach(node => watcher.observe(node));
  }, 0);
}
exports.addIntersectionObserver = addIntersectionObserver;
function onIntersect(init, cb) {
  return ref => addIntersectionObserver(ref, init, (entries, observer) => {
    cb(ref, entries[0], observer);
  });
}
exports.onIntersect = onIntersect;

},

// src/Augments/BlockEvent.ts @16
16: function(__fusereq, exports, module){
function blockEvent(eventName) {
  return ref => ref.addEventListener(eventName, ev => {
    ev.preventDefault();
    return false;
  });
}
exports.blockEvent = blockEvent;

},

// src/El-Tool/El-Tool.ts @17
17: function(__fusereq, exports, module){
exports.__esModule = true;
var Derivations_1 = __fusereq(4);
var HeliumBase_1 = __fusereq(6);
function byId(id) {
  return document.getElementById(id);
}
exports.byId = byId;
function on(eventName, ref, cb) {
  ref.addEventListener(eventName, cb);
}
exports.on = on;
function onDomReady(cb) {
  if (domIsReady) {
    cb();
    return;
  }
  window.addEventListener("DOMContentLoaded", cb);
}
exports.onDomReady = onDomReady;
let domIsReady = false;
typeof window !== "undefined" && onDomReady(() => domIsReady = true);
function classSplice(element, removeClasses, addClasses) {
  if (removeClasses) {
    const remList = Array.isArray(removeClasses) ? removeClasses : removeClasses.split(/\s+/g);
    remList.filter(it => !!it).forEach(className => element.classList.remove(className));
  }
  if (addClasses) {
    const addList = Array.isArray(addClasses) ? addClasses : addClasses.split(/\s+/g);
    addList.filter(it => !!it).forEach(className => element.classList.add(className));
  }
  return element;
}
exports.classSplice = classSplice;
function addClass(addClasses, element) {
  return classSplice(element, null, addClasses);
}
exports.addClass = addClass;
function haveClass(element, className, addNotRemove) {
  return addNotRemove ? classSplice(element, null, className) : classSplice(element, className, undefined);
}
exports.haveClass = haveClass;
function ddxClass(target, ddx) {
  let lastClassList;
  Derivations_1.anchorDeriverToDomNode(target, () => {
    const removeUs = lastClassList;
    classSplice(target, removeUs, lastClassList = ddx());
  }, "props.ddxClass").run();
}
exports.ddxClass = ddxClass;
function removeChildren(parent) {
  let child = parent.firstChild;
  while (!!child) {
    child.remove();
    child = parent.firstChild;
  }
  return parent;
}
exports.removeChildren = removeChildren;
function setStyle(root, style) {
  if (typeof style === "string") {
    root.style.cssText = style;
  } else {
    Object.keys(style).forEach(styleName => root.style[styleName] = style[styleName]);
  }
  return root;
}
exports.setStyle = setStyle;
function onToucherMove(root, cb) {
  const getPosFromClientXY = (x, y) => {
    const clientRect = root.getBoundingClientRect();
    return {
      left: x - clientRect.left,
      top: y - clientRect.top
    };
  };
  root.addEventListener("mousemove", ev => {
    const toucherMoveEvent = ev;
    toucherMoveEvent.positions = [getPosFromClientXY(ev.clientX, ev.clientY)];
    cb(toucherMoveEvent);
  });
  root.addEventListener("touchmove", ev => {
    const toucherMoveEvent = ev;
    const clientRect = root.getBoundingClientRect();
    toucherMoveEvent.positions = Array.from(ev.touches).map(touch => getPosFromClientXY(touch.clientX, touch.clientY));
    cb(toucherMoveEvent);
    ev.preventDefault();
  });
}
exports.onToucherMove = onToucherMove;
function onToucherEnterExit(root, cb) {
  const modEvent = (ev, isEnterEvent) => {
    ev.isEnterEvent = isEnterEvent;
    return ev;
  };
  root.addEventListener("mouseenter", ev => cb(modEvent(ev, true)));
  root.addEventListener("mouseleave", ev => cb(modEvent(ev, false)));
  root.addEventListener("touchstart", ev => {
    cb(modEvent(ev, true));
    ev.preventDefault();
  });
  root.addEventListener("touchend", ev => {
    cb(modEvent(ev, false));
    ev.preventDefault();
  });
}
exports.onToucherEnterExit = onToucherEnterExit;
function onTouch(root, cb) {
  root.addEventListener("mouseover", event => {
    if (event.buttons == 1 || event.buttons == 3) {
      cb(event);
    }
  });
}
exports.onTouch = onTouch;
function isNode(checkMe) {
  return checkMe instanceof Node;
}
exports.isNode = isNode;
function isHTMLElement(checkMe) {
  return ("nodeName" in checkMe);
}
exports.isHTMLElement = isHTMLElement;
function isTextNode(checkMe) {
  return isNode(checkMe) && checkMe.nodeType === checkMe.TEXT_NODE;
}
exports.isTextNode = isTextNode;
function isRawHTML(checkMe) {
  return checkMe && typeof checkMe === "object" && checkMe.hasOwnProperty("hackableHTML");
}
exports.isRawHTML = isRawHTML;
function isProps(checkMe) {
  return checkMe && typeof checkMe === "object" && !Array.isArray(checkMe) && !isNode(checkMe) && !isRawHTML(checkMe);
}
exports.isProps = isProps;
exports.el = el;
function el(tagName, propsOrInnards, innards) {
  const input = standardizeInput(null, propsOrInnards, innards);
  return __createEl(tagName, input.props, input.innards);
}
exports.el = el;
function standardizeInput(nameOrPropsOrInnards, propsOrInnards, maybeInnards, isInnardsRazor) {
  let innards = maybeInnards;
  let props;
  let name;
  if (nameOrPropsOrInnards) {
    if (typeof nameOrPropsOrInnards === "string") {
      name = nameOrPropsOrInnards;
    } else {
      if (maybeInnards !== undefined) {
        throw new Error("Can not define first and third argument if first is not a string");
      }
      maybeInnards = propsOrInnards;
      propsOrInnards = nameOrPropsOrInnards;
    }
  }
  if (propsOrInnards) {
    const isInnards = isInnardsRazor ? isInnardsRazor(propsOrInnards) : !isProps(propsOrInnards);
    if (isInnards) {
      if (maybeInnards !== undefined) {
        throw new Error("Can not define third argument if second is innards");
      }
      innards = propsOrInnards;
    } else {
      props = propsOrInnards;
      innards = maybeInnards;
    }
  }
  props = props || ({});
  if (name) {
    props.class = props.class || "";
    props.class += (props.class ? " " : "") + name;
  }
  return {
    props,
    innards
  };
}
exports.standardizeInput = standardizeInput;
function applyProps(props, out, innardsCb) {
  if (props) {
    if (props.on) {
      const eventListeners = props.on || ({});
      Object.keys(eventListeners).forEach(eventType => out.addEventListener(eventType, eventListeners[eventType]));
    }
    if (props.onClick) {
      out.addEventListener("click", props.onClick);
    }
    if (props.onKeyDown) {
      out.addEventListener("keydown", props.onKeyDown);
    }
    if (props.onMouseDown) {
      out.addEventListener("mousedown", props.onMouseDown);
    }
    if (props.onPush) {
      out.classList.add("onPush");
      out.addEventListener("click", props.onPush);
    }
    if (props.onTouch) {
      onTouch(out, props.onTouch);
    }
    if (props.onToucherMove) {
      onToucherMove(out, props.onToucherMove);
    }
    if (props.onToucherEnterExit) {
      onToucherEnterExit(out, props.onToucherEnterExit);
    }
    if (props.onHoverChange) {
      out.addEventListener("mouseover", ev => props.onHoverChange(true, ev));
      out.addEventListener("mouseout", ev => props.onHoverChange(false, ev));
    }
    if (props.title) {
      out.title = props.title;
    }
    if (props.attr) {
      const attrs = props.attr || ({});
      Object.keys(attrs).forEach(attr => out.setAttribute(attr, attrs[attr]));
    }
    if (props.this) {
      const thisProps = props.this || ({});
      Object.keys(thisProps).forEach(prop => out[prop] = thisProps[prop]);
    }
    if (props.class) {
      addClass(props.class, out);
    }
    if (props.id) {
      out.id = props.id;
    }
    if (props.style) {
      setStyle(out, props.style);
    }
    if (props.innards !== undefined) {
      append(out, props.innards);
    }
  }
  if (innardsCb !== undefined) {
    innardsCb(out);
  }
  if (props) {
    if (props.ref) {
      props.ref(out);
    }
    if (props.ddxClass) {
      ddxClass(out, props.ddxClass);
    }
    if (props.ddx) {
      const keep = {};
      Derivations_1.anchorDeriverToDomNode(out, () => props.ddx(out, keep), "props.ddx").run();
    }
  }
  return out;
}
exports.applyProps = applyProps;
function __createEl(tagName, props, innards) {
  const out = document.createElement(tagName);
  applyProps(props, out, () => {
    if (innards !== undefined) {
      append(out, innards);
    }
  });
  if (HeliumBase_1.isHeliumHazed(out)) {
    out._helium.frozen = true;
  }
  return out;
}
let innardsWarningGiven;
function setInnards(root, innards) {
  if (root === document.body && !window._heliumNoWarnSetInnardsDocBody) {
    console.warn([`Suggestion: avoid using setInnards() on document body, and instead use append().`, `If you do not, anything added to the page outside of your root render is unsafe.  To disable this warning,`, `set window._heliumNoWarnSetInnardsDocBody = true`].join(" "));
  }
  removeChildren(root);
  append(root, innards);
}
exports.setInnards = setInnards;
function append(root, innards, before) {
  const addUs = convertInnardsToElements(innards);
  if (before) {
    addUs.forEach(node => {
      try {
        root.insertBefore(node, before);
      } catch (err) {
        console.error("Can not insert childNode into: ", root);
        console.error("Tried to insert: ", node);
        console.error(err);
      }
    });
  } else {
    addUs.forEach(node => {
      try {
        root.appendChild(node);
      } catch (err) {
        console.error("Can not append childNode to: ", root);
        console.error("Tried to append: ", node);
        console.error(err);
      }
    });
  }
  return root;
}
exports.append = append;
function convertInnardsToElements(innards) {
  const renders = convertInnardsToRenders(innards);
  const out = [];
  renders.forEach(render => {
    if (Array.isArray(render)) {
      render.forEach(node => out.push(node));
    } else {
      out.push(render);
    }
  });
  return out;
}
exports.convertInnardsToElements = convertInnardsToElements;
function convertInnardsToRenders(innards) {
  if (innards === undefined || innards === null) {
    return [];
  }
  if (typeof innards === "function") {
    const refresher = new Derivations_1.RerenderReplacer(innards);
    return refresher.getRender();
  }
  let childList = Array.isArray(innards) ? innards : [innards];
  return childList.flat().map(child => convertSingleInnardToNodes(child));
}
exports.convertInnardsToRenders = convertInnardsToRenders;
function convertSingleInnardToNodes(child) {
  if (typeof child === "function") {
    const replacer = new Derivations_1.RerenderReplacer(child);
    return replacer.getRender();
  } else {
    let out = [];
    if (child === undefined || child === null) {
      return [];
    }
    if (typeof child === "number") {
      child = child + "";
    }
    if (isRawHTML(child)) {
      const template = document.createElement('div');
      template.innerHTML = child.hackableHTML;
      template.getRootNode();
      Array.from(template.childNodes).forEach(child => out.push(child));
    } else if (typeof child === "string") {
      out.push(new Text(child));
    } else if (child) {
      if (Array.isArray(child)) {
        out = out.concat(convertSingleInnardToNodes(child));
      } else {
        out.push(child);
      }
    }
    return out;
  }
}
exports.convertSingleInnardToNodes = convertSingleInnardToNodes;

},

// src/El-Tool/Factories.ts @18
18: function(__fusereq, exports, module){
exports.__esModule = true;
var El_Tool_1 = __fusereq(17);
var helium_sdx_1 = __fusereq(8);
function _simpleFactory(tagName, [arg1, arg2, arg3]) {
  const {props, innards} = El_Tool_1.standardizeInput(arg1, arg2, arg3);
  return El_Tool_1.el(tagName, props, innards);
}
exports._simpleFactory = _simpleFactory;
exports.div = function (...args) {
  return _simpleFactory("div", args);
};
exports.span = function (...args) {
  return _simpleFactory("span", args);
};
exports.pre = function (...args) {
  return _simpleFactory("pre", args);
};
exports.ul = function (...args) {
  return _simpleFactory("ul", args);
};
exports.ol = function (...args) {
  return _simpleFactory("ol", args);
};
exports.li = function (...args) {
  return _simpleFactory("li", args);
};
function table(namePropChild, propChild, child) {
  const {props, innards} = El_Tool_1.standardizeInput(namePropChild, propChild, child);
  const rows = El_Tool_1.convertInnardsToRenders(innards);
  return El_Tool_1.el("table", props, rows.map(row => toTr(row)));
  function toTr(row) {
    if (Array.isArray(row) && row.length === 1) {
      row = row.pop();
    }
    if (El_Tool_1.isHTMLElement(row) && (row.tagName === "TR" || row.nodeName === "#comment")) {
      return row;
    }
    if (typeof row === "function") {
      return () => toTr(row());
    }
    return tr(row);
  }
}
exports.table = table;
function tr(namePropChild, propChild, child) {
  const {props, innards} = El_Tool_1.standardizeInput(namePropChild, propChild, child);
  const cols = El_Tool_1.convertInnardsToRenders(innards);
  return El_Tool_1.el("tr", props, cols.map(data => toTd(data)));
  function toTd(data) {
    if (Array.isArray(data) && data.length === 1) {
      data = data.pop();
    }
    if (El_Tool_1.isHTMLElement(data) && (data.tagName === "TD" || data.nodeName === "#comment")) {
      return data;
    }
    if (typeof data === "function") {
      return () => toTd(data());
    }
    return El_Tool_1.el("td", data);
  }
}
exports.tr = tr;
function td(...[classOrPropOrChild, propOrChild, child]) {
  return _simpleFactory("td", [classOrPropOrChild, propOrChild, child]);
}
exports.td = td;
function img(...[classOrPropOrChild, propOrChild, child]) {
  const {props, innards} = El_Tool_1.standardizeInput(classOrPropOrChild, propOrChild, child);
  props.attr = props.attr || ({});
  props.attr.src = props.src;
  return El_Tool_1.el("img", props, innards);
}
exports.img = img;
function button(...[classOrPropOrChild, propOrChild, child]) {
  return _simpleFactory("button", [classOrPropOrChild, propOrChild, child]);
}
exports.button = button;
function select(classNameOrProps, propsOrInnards, maybeInnards) {
  const {props, innards} = El_Tool_1.standardizeInput(classNameOrProps, propsOrInnards, maybeInnards, a => Array.isArray(a));
  props.on = props.on || ({});
  let currentVal;
  if (props.onInput) {
    const {onInput} = props;
    const onInputListener = props.on.input;
    props.on.input = ev => {
      const newVal = out.value;
      if (newVal !== currentVal) {
        currentVal = newVal;
        onInput(newVal);
      }
      return !onInputListener || onInputListener(ev);
    };
  }
  const out = El_Tool_1.el("select", props, innards.map(innard => {
    let option;
    if (innard instanceof HTMLElement) {
      option = innard;
    } else if (typeof innard === "function") {
      option = El_Tool_1.el("option", {
        ddx: ref => {
          ref.innerText = String(innard());
          if (ref.selected) {}
        }
      });
    } else {
      option = El_Tool_1.el("option", String(innard));
    }
    if (option.selected) {
      currentVal = option.value;
    }
    return option;
  }));
  return out;
}
exports.select = select;
function inputTextbox(classNameOrProps, propsOrInnards, maybeInnards) {
  const {props, innards} = El_Tool_1.standardizeInput(classNameOrProps, propsOrInnards, maybeInnards, a => a instanceof helium_sdx_1.Source);
  props.on = props.on || ({});
  if (innards instanceof helium_sdx_1.Source) {
    const onInputArg = props.onInput;
    props.onInput = value => {
      innards.set(value);
      onInputArg && onInputArg(value);
    };
  }
  if (props.onInput) {
    const {onInput} = props;
    const onInputListener = props.on.input;
    let oldVal = innards;
    props.on.input = ev => {
      const newVal = out.value;
      if (newVal !== oldVal) {
        oldVal = newVal;
        onInput(newVal);
      }
      return !onInputListener || onInputListener(ev);
    };
  }
  if (props.onValueEntered) {
    const onChange = props.on.change;
    const {onValueEntered} = props;
    props.on.change = ev => {
      onValueEntered(ev.target.value);
      return !onChange || onChange(ev);
    };
  }
  const out = El_Tool_1.el("input", props);
  out.type = props.type || "text";
  if (innards) {
    if (typeof innards === "string") {
      out.value = innards;
    } else if (innards instanceof helium_sdx_1.Source) {
      helium_sdx_1.derive(() => out.value = innards.get());
    } else {
      helium_sdx_1.derive(() => out.value = innards());
    }
  }
  return out;
}
exports.inputTextbox = inputTextbox;
exports.italic = function (...args) {
  return _simpleFactory("i", args);
};
exports.bold = function (...args) {
  return _simpleFactory("b", args);
};
exports.hr = function (...args) {
  return _simpleFactory("hr", args);
};
function br(size, props = {}) {
  if (!size) {
    return El_Tool_1.el("br", props);
  }
  props.style = {
    display: "block",
    height: `${size}em`
  };
  return El_Tool_1.el("em-br", props);
}
exports.br = br;
exports.h1 = function (...args) {
  return _simpleFactory("h1", args);
};
exports.h2 = function (...args) {
  return _simpleFactory("h2", args);
};
exports.h3 = function (...args) {
  return _simpleFactory("h3", args);
};
exports.h4 = function (...args) {
  return _simpleFactory("h4", args);
};
exports.h5 = function (...args) {
  return _simpleFactory("h5", args);
};
exports.h6 = function (...args) {
  return _simpleFactory("h6", args);
};

},

// src/El-Tool/Navigation.ts @19
19: function(__fusereq, exports, module){
exports.__esModule = true;
var El_Tool_1 = __fusereq(17);
var Derivations_1 = __fusereq(4);
var helium_sdx_1 = __fusereq(8);
var Sourcify_1 = __fusereq(5);
function anchor(...[classOrProp, propOrChild, child]) {
  const {props, innards} = El_Tool_1.standardizeInput(classOrProp, propOrChild, child);
  props.attr = props.attr || ({});
  const {href} = props;
  if (!href) {
    props.attr.href = "#";
  } else if (typeof href === "string") {
    props.attr.href = href;
  }
  if (props.target) {
    props.attr.target = props.target;
  }
  const ref = El_Tool_1.el("a", props, innards);
  if (typeof href === "function") {
    Derivations_1.anchorDeriverToDomNode(ref, () => {
      ref.href = href();
    });
  }
  if (props.allowTextSelect) {
    El_Tool_1.setStyle(ref, {
      "-webkit-user-drag": "none"
    });
    let selectedText = "";
    El_Tool_1.on("mousedown", ref, ev => selectedText = window.getSelection().toString());
    El_Tool_1.on("click", ref, ev => {
      if (selectedText !== window.getSelection().toString()) {
        ev.preventDefault();
      }
    });
  }
  El_Tool_1.on("click", ref, ev => {
    if (ref.href) {
      if (props.disableHref) {
        return ev.preventDefault();
      }
      if (ev.metaKey || ref.target === "_blank") {
        window.open(ref.href, '_blank');
        return ev.preventDefault();
      } else if (exports.UrlState._updateUrl({
        urlPart: ref.href,
        mode: "push",
        linkElementClickEvent: true
      }) !== "PASS_THROUGH") {
        return ev.preventDefault();
      }
    }
  });
  return ref;
}
exports.anchor = anchor;
function a(...args) {
  console.warn(`helium function "a" has been deprecated.  Please use "anchor" instead.`);
  return anchor(...args);
}
exports.a = a;
function extLink(...[classOrProp, propOrChild, child]) {
  const {props, innards} = El_Tool_1.standardizeInput(classOrProp, propOrChild, child);
  props.target = "_blank";
  return a(props, innards);
}
exports.extLink = extLink;
class NavigationEvent extends Event {
  static createFullUrl(urlPart) {
    try {
      return new URL(urlPart);
    } catch (err) {
      return new URL(urlPart, location.origin);
    }
  }
  constructor(name, props, newUrl) {
    super(name, props);
    this.pageRefreshCancelled = false;
    this.hashFocusCancelled = false;
    this.preventPageRefresh = () => {
      this.pageRefreshCancelled = true;
    };
    this.preventHashFocus = () => {
      this.hashFocusCancelled = true;
    };
    this.newUrl = NavigationEvent.createFullUrl(newUrl);
  }
}
exports.NavigationEvent = NavigationEvent;
function dispatchNavEvent(name, newUrl) {
  const ev = new NavigationEvent(name, {
    cancelable: true
  }, newUrl);
  return window.dispatchEvent(ev) && ev;
}
function dispatchUrlUpdate(firstEvent, newUrl) {
  return dispatchNavEvent(firstEvent, newUrl) && dispatchNavEvent("locationchange", newUrl);
}
class _UrlState {
  constructor() {
    this.state = Sourcify_1.Sourcify({
      url: new URL(location.href),
      urlParams: {},
      hash: ""
    });
    this.initListeners();
  }
  getUrl() {
    return this.state.url;
  }
  get href() {
    return this.getUrl().href;
  }
  get host() {
    return this.getUrl().host;
  }
  get hostname() {
    return this.getUrl().hostname;
  }
  get pathname() {
    return this.getUrl().pathname;
  }
  get hash() {
    return this.state.hash;
  }
  get urlParams() {
    return this.state.urlParams;
  }
  followUrl(newUrl) {
    return this._updateUrl({
      urlPart: newUrl,
      mode: "push"
    });
  }
  replaceUrl(newUrl) {
    return this._updateUrl({
      urlPart: newUrl,
      mode: "replace"
    });
  }
  onUrlChange(cb) {
    window.addEventListener("locationchange", cb);
  }
  observeUrl(cb) {
    this.onUrlChange(cb);
    const ev = new NavigationEvent(name, {
      cancelable: true
    }, window.location.href);
    cb(ev);
  }
  _updateUrl(args) {
    const evResponse = dispatchUrlUpdate("followurl", args.urlPart);
    if (!evResponse) {
      return "BLOCKED";
    }
    if (evResponse.pageRefreshCancelled) {
      if (evResponse.newUrl.href !== location.href) {
        if (location.host !== evResponse.newUrl.host) {
          window.location.href = evResponse.newUrl.href;
          return;
        } else {
          if (args.mode === "push") {
            history.pushState(null, "", args.urlPart);
          } else if (args.mode === "replace") {
            history.replaceState(null, "", args.urlPart);
          }
          this.state.set("url", new URL(args.urlPart));
        }
      }
      return "CAPTURED";
    }
    if (args.linkElementClickEvent) {
      return "PASS_THROUGH";
    } else {
      window.location.href = NavigationEvent.createFullUrl(args.urlPart).href;
    }
  }
  initListeners() {
    window.addEventListener('popstate', ev => {
      dispatchNavEvent("locationchange", location.href);
    });
    setTimeout(() => {
      El_Tool_1.onDomReady(() => {
        setTimeout(() => {
          const hash = location.hash;
          const target = El_Tool_1.byId(hash.slice(1));
          if (!!target) {
            target.scrollIntoView({
              behavior: "auto"
            });
          }
        }, 200);
      });
    }, 0);
    let lastHash;
    this.onUrlChange(ev => {
      if (ev.hashFocusCancelled !== true && ev.newUrl.hash !== lastHash) {
        lastHash = ev.newUrl.hash;
        requestAnimationFrame(() => requestAnimationFrame(() => {
          const target = El_Tool_1.byId(lastHash.slice(1));
          if (!!target) {
            target.scrollIntoView({
              behavior: "auto"
            });
          }
        }));
      }
    });
    helium_sdx_1.derive({
      anchor: "NO_ANCHOR",
      fn: () => {
        const url = this.getUrl();
        const urlParams = new URLSearchParams(url.search);
        urlParams.forEach((value, key) => {
          this.state.urlParams[key] = value;
        });
        this.state.hash = url.hash;
      }
    });
  }
}
exports._UrlState = _UrlState;
exports.UrlState = new _UrlState();
exports.onUrlChange = exports.UrlState.onUrlChange.bind(exports.UrlState);
exports.observeUrl = exports.UrlState.observeUrl.bind(exports.UrlState);
exports.followUrl = exports.UrlState.followUrl.bind(exports.UrlState);
exports.replaceUrl = exports.UrlState.replaceUrl.bind(exports.UrlState);

},

// src/Derivations/Derivers/RerenderReplacer.ts @23
23: function(__fusereq, exports, module){
exports.__esModule = true;
var El_Tool_1 = __fusereq(17);
var HeliumBase_1 = __fusereq(6);
var DomAnchoredDeriver_1 = __fusereq(24);
var helium_sdx_1 = __fusereq(8);
const REMOVE_NODE = false;
const ADD_NODE = true;
class RerenderReplacer {
  constructor(rerenderFn) {
    this.rerenderFn = rerenderFn;
    this.id = "ddx_" + RerenderReplacer.nextId++;
    this.preConstructHook();
    if (helium_sdx_1.DerivationManager.disabled()) {
      this.currentRender = El_Tool_1.convertInnardsToElements(this.rerenderFn());
    } else {
      this.placeHolder = document.createComment(this.id);
      const helium = HeliumBase_1.heliumHaze(this.placeHolder)._helium;
      helium.rerenderReplacer = this;
      helium.isReplacer = true;
      const domAnchorKit = DomAnchoredDeriver_1.anchorDeriverToDomNode(this.placeHolder, this.derive.bind(this), "RerenderReplacer");
      this.getAnchorReport = domAnchorKit.report;
      domAnchorKit.run();
    }
  }
  preConstructHook() {}
  getRender() {
    return this.currentRender;
  }
  derive() {
    const placehold = this.placeHolder;
    const newRender = El_Tool_1.convertInnardsToElements(this.rerenderFn());
    newRender.push(this.placeHolder);
    const oldRender = this.currentRender;
    this.currentRender = newRender;
    const parent = placehold.parentElement;
    if (!parent || !oldRender) {
      return;
    }
    const addRemoveMap = new Map();
    oldRender.forEach(node => addRemoveMap.set(node, REMOVE_NODE));
    newRender.forEach(node => {
      if (addRemoveMap.has(node)) {
        addRemoveMap.delete(node);
      } else {
        addRemoveMap.set(node, ADD_NODE);
      }
    });
    Array.from(addRemoveMap.entries()).forEach(([node, addRemove]) => {
      if (addRemove === REMOVE_NODE && node.parentElement) {
        node.parentElement.removeChild(node);
        if (HeliumBase_1.isHeliumHazed(node) && node._helium.isReplacer) {
          node._helium.rerenderReplacer.removeAll();
        }
      }
    });
    const currentNodes = parent.childNodes;
    let parentIndex = Array.from(currentNodes).indexOf(placehold);
    let lastNode;
    const length = newRender.length;
    for (let i = 0; i < length; i++) {
      const addMe = newRender[length - 1 - i];
      const current = currentNodes[parentIndex - i];
      if (addMe === current) {} else {
        parent.insertBefore(addMe, lastNode);
        if (addRemoveMap.get(addMe) !== ADD_NODE) {
          HeliumBase_1.heliumHaze(addMe)._helium.moveMutation = true;
        } else {
          parentIndex++;
        }
      }
      lastNode = addMe;
    }
  }
  removeAll() {
    this.currentRender.forEach(node => {
      if (node.parentElement) {
        node.parentElement.removeChild(node);
      }
    });
  }
}
RerenderReplacer.nextId = 0;
exports.RerenderReplacer = RerenderReplacer;
;
class RenderRecycler extends RerenderReplacer {
  preConstructHook() {
    const renders = [];
    const rootRenderFn = this.rerenderFn;
    this.rerenderFn = () => {
      let cachedRender = renders.find(prevRender => {
        const sourceVals = Array.from(prevRender.sourceVals.entries());
        for (const [source, val] of sourceVals) {
          if (val !== source.peek()) {
            return false;
          }
        }
        for (const [source, val] of sourceVals) {
          source.get();
        }
        return true;
      });
      if (!cachedRender) {
        renders.push(cachedRender = {
          sourceVals: new Map(),
          render: rootRenderFn()
        });
        const {scope} = this.getAnchorReport({
          scope: true
        });
        const sources = scope.getSourceDependencies();
        sources.forEach(source => cachedRender.sourceVals.set(source, source.peek()));
      } else {
        console.log("Reusing cached render!", cachedRender);
      }
      return cachedRender.render;
    };
  }
}
exports.RenderRecycler = RenderRecycler;
function recycle(renderFn) {
  const replacer = new RenderRecycler(renderFn);
  return replacer.getRender();
}
exports.recycle = recycle;

},

// src/Derivations/Derivers/DomAnchoredDeriver.ts @24
24: function(__fusereq, exports, module){
exports.__esModule = true;
var HeliumBase_1 = __fusereq(6);
var helium_sdx_1 = __fusereq(8);
function anchorDeriverToDomNode(node, ddx, type) {
  let scope;
  setupNodeAnchorMutationObserver();
  return {
    run: () => {
      const helium = HeliumBase_1.heliumHaze(node)._helium;
      const anchor = new helium_sdx_1.AnchorNode();
      anchor.details = [`(Right Click > Reveal in Elements Panel).  Issue occurred in render preceding: `, node];
      const deriveFn = () => {
        const onPage = node.getRootNode() === document;
        if (!onPage) {
          HeliumBase_1.changeFreezeState(node, true);
        }
        ddx();
      };
      scope = new helium_sdx_1.DeriverScope({
        fn: deriveFn,
        batching: "singleFrame",
        anchor
      });
      (helium.unfreeze = helium.unfreeze || []).push(() => {
        anchor.setStatus(helium_sdx_1.AnchorStatus.NORMAL);
      });
      (helium.freeze = helium.freeze || []).push(() => {
        anchor.setStatus(helium_sdx_1.AnchorStatus.FROZEN);
      });
      scope.runDdx();
    },
    report: req => ({
      ...req.scope && ({
        scope
      })
    })
  };
}
exports.anchorDeriverToDomNode = anchorDeriverToDomNode;
let mutationsBuffer = [];
let bufferTimeout;
let mutationObserver;
function setupNodeAnchorMutationObserver() {
  if (typeof MutationObserver === "undefined") {
    return;
  }
  if (mutationObserver) {
    return;
  }
  mutationObserver = new MutationObserver(mutationsList => {
    mutationsList.forEach(mutation => {
      if (mutation.type !== 'childList') {
        return;
      }
      mutationsBuffer.push(mutation);
    });
    if (bufferTimeout) {
      clearTimeout(bufferTimeout);
    }
    bufferTimeout = setTimeout(() => {
      bufferTimeout = undefined;
      const oldBuffer = mutationsBuffer;
      mutationsBuffer = [];
      const haveNode = new Map();
      for (const mutation of oldBuffer) {
        mutation.removedNodes.forEach(node => haveNode.set(node, false));
        mutation.addedNodes.forEach(node => haveNode.set(node, true));
      }
      const addedNodes = [];
      const removedNodes = [];
      Array.from(haveNode.entries()).forEach(([node, shouldHave]) => {
        if (HeliumBase_1.isHeliumHazed(node) && node._helium.moveMutation) {
          return node._helium.moveMutation = false;
        }
        shouldHave ? addedNodes.push(node) : removedNodes.push(node);
      });
      helium_sdx_1.aLog("treeMutations", "Adding:", addedNodes);
      helium_sdx_1.aLog("treeMutations", "Removing:", removedNodes);
      addedNodes.forEach(node => {
        HeliumBase_1.heliumHaze(node);
        permeate(node, child => {
          if (HeliumBase_1.isHeliumHazed(child)) {
            HeliumBase_1.changeFreezeState(child, false);
          }
        });
      });
      removedNodes.forEach(node => {
        HeliumBase_1.heliumHaze(node);
        permeate(node, child => {
          if (HeliumBase_1.isHeliumHazed(child)) {
            HeliumBase_1.changeFreezeState(child, true);
          }
        });
      });
    }, 10);
  });
  var config = {
    childList: true,
    subtree: true
  };
  if (typeof window !== "undefined") {
    const tryAddObserver = () => mutationObserver.observe(document.body, config);
    if (document && document.body) {
      tryAddObserver();
    } else {
      document.addEventListener("DOMContentLoaded", () => tryAddObserver());
    }
  }
  function permeate(root, cb) {
    cb(root);
    root.childNodes.forEach(child => permeate(child, cb));
  }
}

},

// src/Derivations/Derivers/LocalStorage.ts @25
25: function(__fusereq, exports, module){
exports.__esModule = true;
var helium_sdx_1 = __fusereq(8);
function syncWithLocalStorage(id, storeMe) {
  const init = localStorage.getItem(id);
  if (init) {
    try {
      const startingData = JSON.parse(init);
      storeMe.upsert(startingData);
    } catch (err) {
      console.warn("Could not parse locally stored data", err);
    }
  }
  helium_sdx_1.derive({
    anchor: "NO_ANCHOR",
    fn: () => {
      const toStore = storeMe.toObject();
      try {
        localStorage.setItem(id, JSON.stringify(toStore));
      } catch (err) {
        console.warn("Could not store", err);
        console.log(toStore);
      }
    }
  });
}
exports.syncWithLocalStorage = syncWithLocalStorage;

},

// src/Derivations/Sources/SourceMap.ts @26
26: function(__fusereq, exports, module){
exports.__esModule = true;
var RerenderReplacer_1 = __fusereq(23);
var helium_sdx_1 = __fusereq(8);
class SourceMap extends helium_sdx_1.SourceMap {
  renderMap(renderFn) {
    return this._mapReplacers(false, renderFn);
  }
  renderMapKeyed(renderFn) {
    return this._mapReplacers(true, renderFn);
  }
  _mapReplacers(withKey, renderFn) {
    const entryReplacers = new Map();
    const orderer = new RerenderReplacer_1.RerenderReplacer(() => {
      const out = [];
      const keys = this.keys();
      for (const key of keys) {
        if (entryReplacers.has(key) === false) {
          const source = this.getSource(key);
          entryReplacers.set(key, new RerenderReplacer_1.RerenderReplacer(() => renderFn(source.get(), withKey ? key : undefined)));
        }
        const nodeList = entryReplacers.get(key).getRender();
        for (const node of nodeList) {
          out.push(node);
        }
      }
      return out;
    });
    return orderer.getRender();
  }
}
exports.SourceMap = SourceMap;

},

// src/Derivations/Sources/SourceBase.ts @27
27: function(__fusereq, exports, module){
},

// src/Derivations/Sources/SourceArray.ts @28
28: function(__fusereq, exports, module){
exports.__esModule = true;
var RerenderReplacer_1 = __fusereq(23);
var helium_sdx_1 = __fusereq(8);
class SourceArray extends helium_sdx_1.SourceArray {
  renderMap(renderFn) {
    return this._mapReplacers(false, renderFn);
  }
  renderMapKeyed(renderFn) {
    return this._mapReplacers(true, renderFn);
  }
  _mapReplacers(withIndex, renderFn) {
    const obsToRerenderMap = new Map();
    const listReplacer = new RerenderReplacer_1.RerenderReplacer(() => {
      this.orderMatters();
      const out = [];
      const length = this.length;
      for (let i = 0; i < length; i++) {
        const observable = withIndex ? this._getIndexObserver(i) : this.getSource(i);
        if (obsToRerenderMap.has(observable) === false) {
          obsToRerenderMap.set(observable, new RerenderReplacer_1.RerenderReplacer(() => {
            const value = observable.get();
            if (withIndex) {
              return renderFn(this.noDeletedConstant(value.get()), i);
            } else {
              return renderFn(this.noDeletedConstant(value));
            }
          }));
        }
        const nodes = obsToRerenderMap.get(observable).getRender();
        nodes.forEach(node => out.push(node));
      }
      return out;
    });
    return listReplacer.getRender();
  }
}
exports.SourceArray = SourceArray;

}
})
//# sourceMappingURL=app.js.map