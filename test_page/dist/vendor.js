__fuse.bundle({

// node_modules/helium-sdx/dist/index.js @8
8: function(__fusereq, exports, module){
"use strict";
function __export(m) {
  for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", {
  value: true
});
__export(__fusereq(20));
__export(__fusereq(21));
__export(__fusereq(22));

},

// node_modules/helium-sdx/dist/Derivations/index.js @20
20: function(__fusereq, exports, module){
"use strict";
function __export(m) {
  for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", {
  value: true
});
__export(__fusereq(34));
__export(__fusereq(35));
__export(__fusereq(36));
__export(__fusereq(37));
__export(__fusereq(38));

},

// node_modules/helium-sdx/dist/Sourcify/index.js @21
21: function(__fusereq, exports, module){
"use strict";
function __export(m) {
  for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", {
  value: true
});
__export(__fusereq(29));
__export(__fusereq(30));
__export(__fusereq(31));
__export(__fusereq(32));
__export(__fusereq(33));

},

// node_modules/helium-sdx/dist/AnotherLogger.js @22
22: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const logPrepends = {
  freeze: "❄️",
  unfreeze: "🔥",
  dirty: "💩",
  sourceUpdated: "📦",
  treeMutations: "🌲",
  scopes: "🔎",
  sourceRequest: "🧲",
  debugDeriver: "🐞"
};
const showLogTypes = {
  freeze: false,
  unfreeze: false,
  dirty: false,
  sourceUpdated: false,
  treeMutations: false,
  scopes: false,
  sourceRequest: false,
  debugDeriver: false
};
function showLoggingType(type, shouldShow = true) {
  showLogTypes[type] = shouldShow;
}
exports.showLoggingType = showLoggingType;
function showMiscLoggingType(type, shouldShow = true) {
  showLogTypes[type] = shouldShow;
}
exports.showMiscLoggingType = showMiscLoggingType;
function aLog(...args) {
  args = args.map(arg => arg && typeof arg === "object" && arg._static || arg);
  if (args.length <= 1) {
    return console.log(args[0]);
  }
  const type = args[0];
  const shouldShow = showLogTypes[type];
  if (shouldShow === false) {
    return;
  }
  const prepend = logPrepends[type];
  if (prepend) {
    args.shift();
    if (typeof args[0] === "string") {
      args[0] = `${prepend} ${args[0]}`;
    } else {
      args.unshift(prepend);
    }
  }
  console.log(...args);
}
exports.aLog = aLog;
if (typeof window !== "undefined") {
  window.aLog = aLog;
}

},

// node_modules/helium-sdx/dist/Sourcify/SourceProxyObject.js @29
29: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const SourceProxyBase_1 = __fusereq(31);
const SourceMap_1 = __fusereq(39);
const SourcificationManager_1 = __fusereq(32);
const SourceProxyUtilities_1 = __fusereq(33);
const BasicDerivers_1 = __fusereq(40);
class SourceProxyObject extends SourceProxyBase_1.SourceProxyBase {
  constructor(target, srcMap = new SourceMap_1.SourceMap()) {
    super(target);
    this.srcMap = srcMap;
    this[Symbol.iterator] = () => {
      this.refreshAll();
      return this.srcMap.iterator();
    };
    this._fresh = false;
  }
  bind(key, bindTo) {
    this.assertKey(key);
    if (!bindTo) {
      const src = this.srcMap.getSource(key);
      BasicDerivers_1.derive(() => this.set(key, src.get()));
      return src;
    }
    BasicDerivers_1.derive(() => this.set(key, bindTo.get()));
    BasicDerivers_1.derive(() => bindTo.set(this.get(key)));
  }
  delete(index) {
    this.srcMap.delete(index);
    return delete this.target[index];
  }
  get(key) {
    this.assertKey(key);
    return this.srcMap.get(key);
  }
  has(key) {
    this.assertKey(key);
    return this.srcMap.has(key);
  }
  keys() {
    this.refreshAll();
    return this.srcMap.keys();
  }
  _propertyList() {
    return this.keys();
  }
  overwrite(newMe) {
    const extraKeys = new Map(Array.from(Object.entries(this.target)));
    Object.keys(newMe).forEach(key => {
      this.set(key, newMe[key]);
      extraKeys.delete(key);
    });
    for (const key of extraKeys.keys()) {
      this.delete(key);
    }
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  peek(key) {
    this.assertKey(key);
    return this.srcMap.peek(key);
  }
  set(key, value) {
    if (SourceProxyUtilities_1.isProxied(value)) {
      this.target[key] = value.toObjectNoDeps();
      this.srcMap.set(key, value);
    } else {
      this.target[key] = value;
      this.refresh(key);
    }
  }
  size() {
    this.refreshAll();
    return this.srcMap.size();
  }
  toObject() {
    const out = {};
    this.refreshAll();
    this.srcMap.forEach((item, key) => {
      if (item && typeof item === "object" && ("toObject" in item)) {
        item = item.toObject();
      }
      out[key] = item;
    });
    return out;
  }
  upsert(items, shallow = false) {
    Object.keys(items).forEach(_key => {
      const key = _key;
      const value = items[key];
      if (!shallow) {
        const current = this.peek(key);
        if (value && typeof value === "object" && !Array.isArray(value) && current && ("upsert" in current)) {
          current.upsert(value);
          return;
        }
      }
      this.set(key, items[key]);
    });
  }
  assertKey(key) {
    if ((key in this.target) === this.srcMap.peekHas(key)) {
      return;
    }
    this.refresh(key);
  }
  refresh(key) {
    let value = this.target[key];
    if (value && typeof value === "object") {
      const peek = this.srcMap.peek(key);
      if (SourceProxyUtilities_1.isProxied(peek) && peek.toObjectNoDeps() === value) {
        return;
      }
      value = SourceProxyUtilities_1.Sourcify(value);
    }
    this.srcMap.set(key, value);
  }
  refreshAll(type) {
    if (this._fresh && !type) {
      return;
    }
    Object.keys(this.target).forEach(key => {
      if (type === "HARD") {
        this.refresh(key);
      } else {
        this.assertKey(key);
      }
    });
    this._fresh = true;
  }
}
exports.SourceProxyObject = SourceProxyObject;
;
exports.ASSERT_SHALLOW_TYPE_MATCH = {};

},

// node_modules/helium-sdx/dist/Sourcify/SourceProxyArray.js @30
30: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const SourceProxyBase_1 = __fusereq(31);
const SourceArray_1 = __fusereq(44);
const SourcificationManager_1 = __fusereq(32);
const SourceProxyUtilities_1 = __fusereq(33);
class SourceProxyArray extends SourceProxyBase_1.SourceProxyBase {
  constructor(target, srcArray = new SourceArray_1.SourceArray()) {
    super(target);
    this.srcArray = srcArray;
    this[Symbol.iterator] = () => {
      this.refreshAll();
      return this.srcArray.iterator();
    };
    this._fresh = false;
  }
  get length() {
    this.assertIndex(this.target.length - 1);
    return this.srcArray.length;
  }
  concat(items) {
    this.refreshAll();
    const out = this.toObject();
    items.forEach(it => out.push(SourceProxyUtilities_1.Sourcify(it)));
    return SourceProxyUtilities_1.Sourcify(out);
  }
  delete(index) {
    this.srcArray.delete(index);
    return delete this.target[index];
  }
  entries() {
    this.refreshAll();
    return this.srcArray.entries();
  }
  clear() {
    while (this.target.length) {
      this.target.pop();
    }
    this.srcArray.clear();
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  every(cb) {
    this.refreshAll();
    return this.srcArray.every(cb);
  }
  fill(value, start, end) {
    value = SourceProxyUtilities_1.Sourcify(value);
    end = end !== undefined ? end : this.target.length;
    for (let i = start || 0; i < end; i++) {
      this.set(i, value);
    }
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  filter(cb) {
    this.refreshAll();
    return SourceProxyUtilities_1.Sourcify(this.srcArray.filter(cb));
  }
  find(cb) {
    this.refreshAll();
    return this.srcArray.find(cb);
  }
  findIndex(cb) {
    this.refreshAll();
    return this.srcArray.findIndex(cb);
  }
  flat(depth) {
    this.refreshAll();
    return SourceProxyUtilities_1.Sourcify(this.srcArray.flat(depth));
  }
  forEach(cb) {
    this.refreshAll();
    return this.srcArray.forEach(cb);
  }
  forEachPart(cb) {
    this.refreshAll();
    return this.srcArray.forEachPart(cb);
  }
  get(index) {
    this.assertIndex(index);
    return this.srcArray.get(index);
  }
  getPart(index) {
    this.assertIndex(index);
    return this.srcArray.getPart(index);
  }
  has(index) {
    this.assertIndex(index);
    return this.srcArray.has(index);
  }
  indexOf(target) {
    this.refreshAll();
    return this.srcArray.indexOf(target);
  }
  includes(target, fromIndex) {
    this.refreshAll();
    return this.srcArray.includes(target, fromIndex);
  }
  join(separator) {
    this.refreshAll();
    return this.srcArray.join(separator);
  }
  keys() {
    this.refreshAll();
    return this.srcArray.keys();
  }
  _propertyList() {
    this.refreshAll();
    const out = this.srcArray.keys().map(it => String(it));
    out.push("length");
    return out;
  }
  map(cb) {
    this.refreshAll();
    return SourceProxyUtilities_1.Sourcify(this.srcArray.map(cb));
  }
  overwrite(newSelf) {
    const oldLength = this.target.length;
    for (let i = 0; i < oldLength; i++) {
      if (i < newSelf.length) {
        this.set(i, newSelf[i]);
      } else {
        this.delete(i);
      }
    }
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  partsSlice(start, end) {
    this.refresh(start, end !== undefined ? end : -1);
    return SourceProxyUtilities_1.Sourcify(this.srcArray.partsSlice(start, end));
  }
  peekLength() {
    return this.target.length;
  }
  peek(index) {
    this.assertIndex(index);
    return this.srcArray.peek(index);
  }
  push(value) {
    const length = this.target.length;
    this.set(length, value);
  }
  pop() {
    this.assertIndex(this.target.length - 1);
    this.target.pop();
    return this.srcArray.pop();
  }
  reduce(cb) {
    this.refreshAll();
    return this.srcArray.reduce(cb);
  }
  remove(item, count = -1) {
    this.refreshAll();
    let targetItemType = item;
    if (SourceProxyUtilities_1.isProxied(item)) {
      targetItemType = item.toObjectNoDeps();
    }
    let removeCount = 0;
    for (let i = 0; i < this.target.length && (count < 0 || removeCount < count); i++) {
      if (this.target[i] === targetItemType) {
        removeCount++;
        this.target.splice(i, 1);
        i -= 1;
      }
    }
    this.srcArray.remove(item, count);
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  reverse() {
    this.refreshAll();
    this.target.reverse();
    this.srcArray.reverse();
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  slice(start, end) {
    this.refresh(start, end !== undefined ? end : -1);
    return SourceProxyUtilities_1.Sourcify(this.srcArray.slice(start, end));
  }
  set(index, value) {
    if (SourceProxyUtilities_1.isProxied(value)) {
      this.target[index] = value.toObjectNoDeps();
      this.srcArray.set(index, value);
    } else {
      this.target[index] = value;
      this.refresh(index);
    }
  }
  shift() {
    this.assertIndex(0);
    this.target.shift();
    return this.srcArray.shift();
  }
  some(cb) {
    this.refreshAll();
    return this.srcArray.some(cb);
  }
  sort(sortFn) {
    this.refreshAll();
    this.target.sort(sortFn);
    this.srcArray.sort(sortFn);
    return SourcificationManager_1.HELIUM_PROXY_OUT;
  }
  splice(start, deleteCount, ...items) {
    this.target.splice(start, deleteCount, ...(items || []).map(it => SourceProxyUtilities_1.isProxied(it) ? it.toObjectNoDeps() : it));
    return SourceProxyUtilities_1.Sourcify(this.srcArray.splice(start, deleteCount, ...items || []));
  }
  toObject() {
    const out = [];
    this.refreshAll();
    this.srcArray.forEach((item, index) => {
      if (item && typeof item === "object" && ("toObject" in item)) {
        item = item.toObject();
      }
      out[index] = item;
    });
    return out;
  }
  unshift(item) {
    this.target.unshift(SourceProxyUtilities_1.Unsourcify(item));
    return this.srcArray.unshift(SourceProxyUtilities_1.Sourcify(item));
  }
  assertIndex(index) {
    if (index >= this.target.length) {
      return;
    }
    if (!!((index in this.target)) === !!this.srcArray.peekHas(index)) {
      return;
    }
    this.refresh(index);
  }
  refresh(start, end) {
    if (end === undefined) {
      end = start + 1;
    } else if (end === -1) {
      end = this.target.length - 1;
    }
    for (let i = start; i < end; i++) {
      let value = this.target[i];
      if (value && typeof value === "object") {
        const peek = this.srcArray.peek(i);
        if (SourceProxyUtilities_1.isProxied(peek) && peek.toObjectNoDeps() === value) {
          return;
        }
        value = SourceProxyUtilities_1.Sourcify(value);
      }
      this.srcArray.set(i, value);
    }
  }
  refreshAll(type) {
    if (this._fresh && !type) {
      return;
    }
    for (let i = 0; i < this.target.length; i++) {
      if (type === "HARD") {
        this.refresh(i);
      } else {
        this.assertIndex(i);
      }
    }
    this._fresh = true;
  }
}
exports.SourceProxyArray = SourceProxyArray;
exports.ASSERT_SHALLOW_TYPE_MATCH_ARRAY = {};

},

// node_modules/helium-sdx/dist/Sourcify/SourceProxyBase.js @31
31: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
class SourceProxyBase {
  constructor(target) {
    this.target = target;
  }
}
exports.SourceProxyBase = SourceProxyBase;

},

// node_modules/helium-sdx/dist/Sourcify/SourcificationManager.js @32
32: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const SourceProxyArray_1 = __fusereq(30);
const SourceProxyObject_1 = __fusereq(29);
exports.HELIUM_PROXY_OUT = {};
class SourcificationManager {
  constructor(item) {
    this.setProxy(item);
    if (Array.isArray(item)) {
      this.type = "ARR";
      this.srcMgmt = SourcificationManager.createSourceProxyArray(item);
    } else {
      this.type == "OBJ";
      this.srcMgmt = SourcificationManager.createSourceProxyObject(item);
    }
  }
  get proxy() {
    return this._proxy;
  }
  autocorrectPropName(propName) {
    if (typeof propName === "symbol") {
      return undefined;
    }
    if (this.type === "ARR") {
      let asNum;
      if (!isNaN(asNum = parseInt(propName))) {
        return asNum;
      } else {
        return undefined;
      }
    }
    return propName;
  }
  setProxy(item) {
    this._proxy = new Proxy(item, {
      deleteProperty: (_, propName) => {
        propName = this.autocorrectPropName(propName);
        if (propName === undefined) {
          return false;
        }
        return this.srcMgmt.delete(propName);
      },
      get: (_, propName) => {
        switch (propName) {
          case "getProxyManager":
            return () => this;
          case "toObjectNoDeps":
            return () => item;
          case "pretend":
            return () => this._proxy;
        }
        const correctedName = this.autocorrectPropName(propName);
        if (correctedName !== undefined && this.srcMgmt.has(correctedName)) {
          return this.srcMgmt.get(correctedName);
        }
        if ((propName in this.srcMgmt)) {
          if (typeof this.srcMgmt[propName] === "function") {
            return (...args) => {
              const out = this.srcMgmt[propName](...args);
              if (out === exports.HELIUM_PROXY_OUT) {
                return this._proxy;
              }
              return out;
            };
          }
          return this.srcMgmt[propName];
        }
      },
      has: (_, propName) => {
        switch (propName) {
          case "getProxyManager":
          case "toObjectNoDeps":
            return true;
        }
        if ((propName in this.srcMgmt)) {
          return true;
        }
        propName = this.autocorrectPropName(propName);
        if (propName === undefined) {
          return false;
        }
        return this.srcMgmt.has(propName);
      },
      ownKeys: _ => {
        return this.srcMgmt._propertyList();
      },
      set: (_, propName, value) => {
        propName = this.autocorrectPropName(propName);
        if (propName === undefined) {
          throw new Error(`Can not modify array prototype "${propName}" of type: ${typeof propName}`);
        }
        this.srcMgmt.set(propName, value);
        return true;
      }
    });
  }
}
exports.SourcificationManager = SourcificationManager;
SourcificationManager.createSourceProxyArray = item => new SourceProxyArray_1.SourceProxyArray(item);
SourcificationManager.createSourceProxyObject = item => new SourceProxyObject_1.SourceProxyObject(item);

},

// node_modules/helium-sdx/dist/Sourcify/SourceProxyUtilities.js @33
33: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const SourcificationManager_1 = __fusereq(32);
function Sourcify(target) {
  if (isProxied(target)) {
    return target;
  } else if (typeof target !== "object") {
    return target;
  } else if ((/^(object|array)$/i).test(target.constructor.name) === false) {
    return target;
  }
  const mgmt = new SourcificationManager_1.SourcificationManager(target);
  return mgmt.proxy;
}
exports.Sourcify = Sourcify;
function Unsourcify(target) {
  if (isProxied(target)) {
    return target.toObjectNoDeps();
  }
  return target;
}
exports.Unsourcify = Unsourcify;
function isProxied(checkMe) {
  return checkMe && typeof checkMe === "object" && ("getProxyManager" in checkMe);
}
exports.isProxied = isProxied;

},

// node_modules/helium-sdx/dist/Derivations/AnchorNode.js @34
34: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
var AnchorStatus;
(function (AnchorStatus) {
  AnchorStatus["NORMAL"] = "n";
  AnchorStatus["FROZEN"] = "f";
})(AnchorStatus = exports.AnchorStatus || (exports.AnchorStatus = {}));
class AnchorNode {
  constructor() {
    this.status = AnchorStatus.NORMAL;
    this.children = [];
  }
  getStatus() {
    return this.status;
  }
  setStatus(newStat, notifyChildren = true) {
    if (newStat === this.status) {
      return false;
    }
    this.status = newStat;
    if (notifyChildren) {
      this.children.forEach(child => child.setStatus(this.status));
    }
    return true;
  }
  isFrozen() {
    return this.status === AnchorStatus.FROZEN;
  }
  statusIsNormal() {
    return this.status === AnchorStatus.NORMAL;
  }
  addChild(child) {
    this.children.push(child);
  }
}
exports.AnchorNode = AnchorNode;

},

// node_modules/helium-sdx/dist/Derivations/Derivers/index.js @35
35: function(__fusereq, exports, module){
"use strict";
function __export(m) {
  for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", {
  value: true
});
__export(__fusereq(41));
__export(__fusereq(40));

},

// node_modules/helium-sdx/dist/Derivations/DerivationManager.js @36
36: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const AnotherLogger_1 = __fusereq(22);
const DerivationBatcher_1 = __fusereq(42);
class __DerivationManager {
  constructor() {
    this.deriverStack = [];
    this.lastSourceId = 0;
    this.sourceToDeriverMap = new Map();
    this.knownScopes = new Map();
    this.batcher = new DerivationBatcher_1.DerivationBatcher(this);
    this.completedDeriversBatch = [];
    const existing = window.DerivationManager;
    if (existing) {
      return existing;
    }
    window.DerivationManager = this;
  }
  disabled() {
    return this.lastSourceId === 0;
  }
  debugDeriver() {
    if (!this.currentDeriver) throw new Error("Current Scope is undefined");
    AnotherLogger_1.showLoggingType("debugDeriver");
    this.targetDebugDeriver = this.currentDeriver;
  }
  isDebugDeriver(deriver = this.targetDebugDeriver) {
    return this.currentDeriver === deriver;
  }
  createSourceId(append) {
    return `s${this.lastSourceId++}` + (append ? `_${append}` : "");
  }
  sourceRequested(source) {
    if (!this.currentDeriver) {
      return;
    }
    const sourceId = source.getSourceId();
    if (this.isDebugDeriver()) {
      AnotherLogger_1.aLog("debugDeriver", `GET: Source requested ${sourceId}`);
    }
    AnotherLogger_1.aLog("sourceRequest", `${this.currentDeriver.id}: Source requested ${sourceId}`);
    this.currentDeriver.addSourceDependency(source);
  }
  sourceUpdated(source, equalValue) {
    const sourceId = source.getSourceId();
    AnotherLogger_1.aLog("sourceUpdated", `Update: ${sourceId}`);
    const toBeUpdated = this.popDeriversDependentOnSource(sourceId);
    toBeUpdated && toBeUpdated.forEach(deriver => {
      if (deriver.ignoresEqualValues() && equalValue) {
        this.makeDeriverDependOnSource(deriver, sourceId);
        return;
      }
      this.batcher.addDeriverWithReason(deriver, source);
    });
  }
  _runAsDeriver(deriver, ddx) {
    let startDeriver = this.currentDeriver;
    this.deriverStack.push(this.currentDeriver = deriver);
    AnotherLogger_1.aLog("scopes", "Running Scope", deriver && deriver.id);
    if (this.isDebugDeriver()) {
      AnotherLogger_1.aLog("debugDeriver", "<Rerunning Deriver>");
    }
    ddx();
    this.deriverStack.pop();
    this.currentDeriver = startDeriver;
    if (!!deriver) {
      this.completedDeriversBatch.push(deriver);
      if (!this.currentDeriver) {
        this.completedDeriversBatch.reverse().forEach(deriver => {
          deriver.getSourceDependencyIds().forEach(sourceId => {
            this.makeDeriverDependOnSource(deriver, sourceId);
          });
        });
        this.completedDeriversBatch = [];
      }
    }
  }
  _getCurrentDeriver() {
    return this.currentDeriver;
  }
  popDeriversDependentOnSource(sourceId) {
    const deriversPerSource = this.sourceToDeriverMap.get(sourceId);
    if (!deriversPerSource) {
      return;
    }
    const out = Array.from(deriversPerSource.keys());
    this.sourceToDeriverMap.delete(sourceId);
    return out;
  }
  makeDeriverDependOnSource(deriver, sourceId) {
    if (this.sourceToDeriverMap.has(sourceId) === false) {
      this.sourceToDeriverMap.set(sourceId, new Map());
    }
    return this.sourceToDeriverMap.get(sourceId).set(deriver);
  }
  dropDeriver(deriver) {
    if (deriver.statusIsNormal()) {
      throw Error("Will not drop an status normal scope");
    }
    this.knownScopes.delete(deriver);
  }
}
exports.__DerivationManager = __DerivationManager;
exports.DerivationManager = new __DerivationManager();

},

// node_modules/helium-sdx/dist/Derivations/DeriverScope.js @37
37: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const DerivationManager_1 = __fusereq(36);
const AnchorNode_1 = __fusereq(34);
let SCOPE_ID = 0;
class DeriverScope extends AnchorNode_1.AnchorNode {
  constructor(props) {
    super();
    this.props = props;
    this.sourceDeps = new Map();
    this.id = String(SCOPE_ID++);
    this.creationStack = new Error().stack;
    this.dirty = false;
    this.store = {};
    props.anchor = props.anchor || DerivationManager_1.DerivationManager._getCurrentDeriver() || "NO_ANCHOR";
    props.batching = props.batching || "stack";
    if (typeof props.anchor !== "string") {
      props.anchor.addChild(this);
    }
  }
  addSourceDependency(source) {
    this.sourceDeps.set(source);
  }
  runDdx() {
    this.dropChildren();
    if (this.isFrozen()) {
      DerivationManager_1.DerivationManager.dropDeriver(this);
      return this.dirty = true;
    }
    this.dirty = false;
    try {
      DerivationManager_1.DerivationManager._runAsDeriver(this, () => {
        this.sourceDeps = new Map();
        this.props.fn({
          scope: this,
          store: this.store
        });
      });
    } catch (err) {
      const details = typeof this.props.anchor === "object" && this.props.anchor.details;
      if (details) {
        if (Array.isArray(details)) {
          console.error(...details);
        } else {
          console.error(details);
        }
      }
      console.error(err);
    }
  }
  getSourceDependencies() {
    return Array.from(this.sourceDeps.keys());
  }
  getSourceDependencyIds() {
    return Array.from(this.sourceDeps.keys()).map(source => source.getSourceId());
  }
  hasDep(source) {
    return this.sourceDeps.has(source);
  }
  getDeriver() {
    return this.props.fn;
  }
  getStore() {
    return this.store;
  }
  getBatching() {
    return this.props.batching;
  }
  ignoresEqualValues() {
    return this.props.allowEqualValueUpdates !== true;
  }
  unfreeze() {
    return this.setStatus(AnchorNode_1.AnchorStatus.NORMAL);
  }
  setStatus(status) {
    const didChange = super.setStatus(status, false);
    if (didChange) {
      if (this.statusIsNormal() && this.dirty) {
        this.runDdx();
      } else {
        this.children.forEach(child => child.setStatus(this.status));
      }
    }
    return didChange;
  }
  dropChildren() {
    this.children.forEach(child => child.setStatus(AnchorNode_1.AnchorStatus.FROZEN));
    this.children = [];
  }
}
exports.DeriverScope = DeriverScope;

},

// node_modules/helium-sdx/dist/Derivations/Sources/index.js @38
38: function(__fusereq, exports, module){
"use strict";
function __export(m) {
  for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", {
  value: true
});
__export(__fusereq(43));
__export(__fusereq(44));
__export(__fusereq(39));
__export(__fusereq(45));
__export(__fusereq(46));

},

// node_modules/helium-sdx/dist/Derivations/Sources/SourceMap.js @39
39: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const Source_1 = __fusereq(43);
const BasicDerivers_1 = __fusereq(40);
class SourceMapBase {
  constructor(entries, args) {
    this.args = args;
    this._map = new Map();
    if (entries) {
      throw new Error("TODO");
    }
  }
  getSource(key) {
    return this.getSetSource({
      key
    });
  }
  get(key) {
    return this.getSource(key).get();
  }
  set(key, value, forceUpdates) {
    this.getSetSource({
      key,
      value
    });
  }
  _static() {
    return this._map;
  }
  peek(key) {
    if (this._map.has(key)) {
      return this._map.get(key).peek();
    }
  }
  getSetSource(args) {
    const key = args.key;
    const setNotGet = ("value" in args);
    const keyDoesNotExist = this._map.has(key) === false;
    if (keyDoesNotExist) {
      let idAppend = "mval";
      if (this.args && this.args.id) {
        idAppend = `${this.args.id}_${idAppend}`;
      }
      if (typeof key === "string" || typeof key === "number") {
        idAppend += `_${key}`;
      }
      this._map.set(key, new Source_1.Source(args.value, {
        idAppend
      }));
      if (setNotGet) {
        return;
      }
    } else if (setNotGet) {
      this._map.get(key).set(args.value);
      return;
    }
    return this._map.get(key);
  }
}
exports.SourceMapBase = SourceMapBase;
class SourceMap extends SourceMapBase {
  constructor() {
    super(...arguments);
    this._keys = new Source_1.Source([], {
      idAppend: "mapKeys"
    });
    this._has = new SourceMapBase(null, {
      id: "has"
    });
    this._size = new Source_1.Source(0);
  }
  forEach(cb) {
    this.keys().forEach(key => cb(this.get(key), key));
  }
  entries() {
    throw new Error("TODO");
  }
  values() {
    throw new Error("TODO");
  }
  iterator() {
    const self = this;
    return iterate();
    function* iterate() {
      for (const it of self.keys()) {
        yield self.get(it);
      }
    }
  }
  toObject() {
    const out = {};
    this.forEach((value, key) => {
      if (typeof key === "string" || typeof key === "number") {
        out[key] = value;
      }
    });
    return out;
  }
  size() {
    return this._size.get();
  }
  set(key, value, forceUpdates) {
    let keysChanged = false;
    if (!this.peekHas(key)) {
      keysChanged = true;
    }
    this._has.set(key, true);
    super.set(key, value, forceUpdates);
    if (keysChanged) {
      this.updateKeys();
    }
  }
  delete(key) {
    if (!this.peekHas(key)) {
      return;
    }
    this._has.set(key, false);
    super.set(key, undefined);
    this.updateKeys();
  }
  clear() {
    this.keys().forEach(key => this.delete(key));
  }
  getnit(key, value) {
    if (this.peekHas(key)) {
      return this.get(key);
    }
    this.set(key, value);
    return value;
  }
  has(key) {
    return !!this._has.get(key);
  }
  keys() {
    return this._keys.get();
  }
  isEmpty() {
    return !this._keys.get().length;
  }
  updateKeys() {
    BasicDerivers_1.noDeps(() => {
      const keys = Array.from(this._map.keys()).filter(key => this.peekHas(key));
      this._keys.set(keys);
      this._size.set(keys.length);
    });
  }
  peekHas(key) {
    return !!this._has.peek(key);
  }
}
exports.SourceMap = SourceMap;

},

// node_modules/helium-sdx/dist/Derivations/Derivers/BasicDerivers.js @40
40: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const DeriverScope_1 = __fusereq(37);
const DerivationManager_1 = __fusereq(36);
function derive(fnOrProps, batching) {
  let props;
  if (typeof fnOrProps === "function") {
    props = {
      fn: fnOrProps
    };
  } else {
    props = fnOrProps;
  }
  props.batching = props.batching || batching;
  const scope = new DeriverScope_1.DeriverScope(props);
  scope.runDdx();
  return scope;
}
exports.derive = derive;
function deriveAnchorless(fnOrProps, batching) {
  const props = typeof fnOrProps === "object" ? fnOrProps : {
    fn: fnOrProps
  };
  props.anchor = "NO_ANCHOR";
  derive(props, batching);
}
exports.deriveAnchorless = deriveAnchorless;
function noDeps(cb) {
  let out;
  DerivationManager_1.DerivationManager._runAsDeriver(undefined, () => out = cb());
  return out;
}
exports.noDeps = noDeps;

},

// node_modules/helium-sdx/dist/Derivations/Derivers/DeriverSwitch.js @41
41: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const BasicDerivers_1 = __fusereq(40);
function deriverSwitch(args) {
  const {watch, responses} = args;
  const noWay = responses.find(it => !(("match" in it) || ("test" in it)));
  if (!!noWay) {
    console.error(noWay);
    throw new Error(`Response has no ability to match item.  Perhaps use "DEFAULT_RESPONSE" as match field.`);
  }
  responses.sort((a, b) => {
    const aIsDefault = a.test === "DEFAULT_RESPONSE";
    const bIsDefault = b.test === "DEFAULT_RESPONSE";
    if (aIsDefault && !bIsDefault) {
      return 1;
    }
    if (!aIsDefault && bIsDefault) {
      return -1;
    }
    return 0;
  });
  let lastResponse;
  BasicDerivers_1.derive({
    ...args.ddxProps,
    fn: () => {
      let value;
      if (typeof watch === "function") {
        value = watch();
      } else {
        value = watch.get();
      }
      BasicDerivers_1.noDeps(async () => {
        for (const resp of responses) {
          const {match, test} = resp;
          let tryMe;
          if (test === "DEFAULT_RESPONSE") {
            tryMe = resp;
          } else if (value === match) {
            tryMe = resp;
          } else if (test instanceof RegExp) {
            if (test.test(String(value))) {
              tryMe = resp;
            }
          } else if (typeof test === "function") {
            if (await test(value)) {
              tryMe = resp;
            }
          }
          if (!!tryMe) {
            if (tryMe === lastResponse && !args.retriggerSameResponse) {
              return;
            }
            const attempt = await tryMe.action(value);
            if (attempt === "NO_SWITCH_NEEDED") {
              return;
            }
            if (attempt !== "TRY_NEXT_ACTION") {
              return lastResponse = resp;
            }
          }
        }
      });
    }
  });
}
exports.deriverSwitch = deriverSwitch;

},

// node_modules/helium-sdx/dist/Derivations/DerivationBatcher.js @42
42: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const AnotherLogger_1 = __fusereq(22);
class DerivationBatcher {
  constructor(manager) {
    this.manager = manager;
    this.typeToBatchListMap = new Map();
    this.batchTimeouts = new Map();
  }
  addDeriverWithReason(deriver, source) {
    const reasons = this.getDeriverReasons(deriver, true);
    reasons.set(source);
    if (this.manager.isDebugDeriver(deriver)) {
      AnotherLogger_1.aLog("debugDeriver", "DDX: Source dependency updating: ", source.getSourceId());
    }
    this.assertBatchRunning(deriver);
  }
  getDeriverReasons(deriver, force = false) {
    const batchType = deriver.getBatching();
    let batchList = this.typeToBatchListMap.get(batchType);
    if (!batchList) {
      if (!force) {
        return;
      }
      this.typeToBatchListMap.set(batchType, batchList = new Map());
    }
    let reasons = batchList.get(deriver);
    if (!reasons) {
      if (!force) {
        return;
      }
      batchList.set(deriver, reasons = new Map());
    }
    return reasons;
  }
  assertBatchRunning(deriver) {
    const batchType = deriver.getBatching();
    if (batchType === "none") {
      return deriver.runDdx();
    }
    if (this.batchTimeouts.has(batchType)) {
      return;
    }
    const start = timeout => this.batchTimeouts.set(batchType, timeout);
    const end = (() => {
      this.batchTimeouts.delete(batchType);
      this.runBatch(batchType);
    }).bind(this);
    if (typeof batchType === "number") {
      return start(setTimeout(end, batchType));
    }
    switch (batchType) {
      case "singleFrame":
        start(requestAnimationFrame(end));
        return;
      case "doubleFrame":
        start(requestAnimationFrame(() => requestAnimationFrame(end)));
        return;
      case "stack":
        start(setTimeout(end, 0));
        return;
      case "oneSec":
        start(setTimeout(end, 1000));
        return;
      case "fiveSec":
        start(setTimeout(end, 5 * 1000));
        return;
      case "tenSec":
        start(setTimeout(end, 10 * 1000));
        return;
      case "twentySec":
        start(setTimeout(end, 20 * 1000));
        return;
      default:
        throw new Error(`Unknown batchType ${batchType}`);
    }
  }
  runBatch(batchType) {
    AnotherLogger_1.aLog("sourceUpdated", "<Start Source Update Batch>");
    let batchList = this.typeToBatchListMap.get(batchType);
    if (!batchList) {
      return;
    }
    const deriverList = Array.from(batchList.entries()).filter(([deriver, reasons]) => {
      for (const source of reasons.keys()) {
        if (deriver.hasDep(source)) {
          return true;
        }
      }
    }).map(([deriver, reasons]) => {
      if (this.manager.isDebugDeriver(deriver)) {
        AnotherLogger_1.aLog("debugDeriver", `DDX: Deriving for change in source(s): ${Array.from(reasons.keys()).join(", ")}`);
      }
      return deriver;
    });
    batchList.clear();
    deriverList.forEach(deriver => {
      deriver.dropChildren();
    });
    deriverList.forEach(deriver => {
      deriver.runDdx();
    });
  }
}
exports.DerivationBatcher = DerivationBatcher;

},

// node_modules/helium-sdx/dist/Derivations/Sources/Source.js @43
43: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const DerivationManager_1 = __fusereq(36);
const BasicDerivers_1 = __fusereq(40);
function source(value) {
  return new Source(value);
}
exports.source = source;
class Source {
  constructor(value, args = {}) {
    this.value = value;
    this.args = args;
    this.sourceId = DerivationManager_1.DerivationManager.createSourceId(args.idAppend);
  }
  get() {
    DerivationManager_1.DerivationManager.sourceRequested(this);
    return this.value;
  }
  peek() {
    return this.value;
  }
  getSourceId() {
    return this.sourceId;
  }
  set(value, forceUpdates = false) {
    const equalValue = !forceUpdates && this.value === value;
    this.value = value;
    this.dispatchUpdateNotifications(equalValue);
  }
  bindTo(bindTo) {
    BasicDerivers_1.derive(() => this.set(bindTo.get()));
    BasicDerivers_1.derive(() => bindTo.set(this.get()));
  }
  dispatchUpdateNotifications(equalValue = false) {
    DerivationManager_1.DerivationManager.sourceUpdated(this, equalValue);
  }
}
exports.Source = Source;

},

// node_modules/helium-sdx/dist/Derivations/Sources/SourceArray.js @44
44: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const Source_1 = __fusereq(43);
const SourceMap_1 = __fusereq(39);
const DerivationManager_1 = __fusereq(36);
const BasicDerivers_1 = __fusereq(40);
class SourceArrayBase {
  constructor(array) {
    this.DELETED = {
      DELETED: true
    };
    if (array) {
      this._array = array.map(value => new Source_1.Source(value, {
        idAppend: "aval"
      }));
      this._length = new Source_1.Source(array.length, {
        idAppend: "alen"
      });
    } else {
      this._array = [];
      this._length = new Source_1.Source(0, {
        idAppend: "alen"
      });
    }
  }
  get length() {
    return this._length.get();
  }
  delete(index) {
    this.getSource(index).set(this.DELETED);
  }
  forEachPart(cb) {
    for (let i = 0; i < this.length; i++) {
      cb(this.getPart(i), i);
    }
  }
  getPart(index) {
    return this.noDeletedConstant(this.getSource(index).get());
  }
  noDeletedConstant(out) {
    return out === this.DELETED ? undefined : out;
  }
  getSource(index, noInit = false) {
    if (!this._array[index] && !noInit) {
      this._array[index] = new Source_1.Source(this.DELETED, {
        idAppend: "aval"
      });
    }
    return this._array[index];
  }
  peek(index) {
    if (typeof index === "number" && this._array[index]) {
      return this.noDeletedConstant(this._array[index].peek());
    }
  }
  peekHas(index) {
    return index < this.peekLength() && this._array[index] && this._array[index].peek() !== this.DELETED;
  }
  peekLength() {
    return this._length.peek();
  }
  pop() {
    const index = this.peekLength() - 1;
    const value = this._array.pop().get();
    this._length.set(index);
    this.syncIndex(index);
    return this.noDeletedConstant(value);
  }
  push(value) {
    const index = this.peekLength();
    this.set(index, value);
    this._length.set(index + 1);
    this.syncIndex(index);
  }
  remove(item, limit = -1) {
    let count = 0;
    const length = this._length.peek();
    for (let i = 0; i < length - count; i++) {
      if (limit > -1 && count >= limit) {
        break;
      }
      if (this.peek(i) === item) {
        this._array.splice(i, 1);
        i--;
        count++;
      }
    }
    this._length.set(length - count);
    this.onArrangementUpdate();
  }
  set(index, value, forceUpdates) {
    if (!this._array[index]) {
      this._array[index] = new Source_1.Source(value, {
        idAppend: "aval"
      });
    } else {
      this._array[index].set(value, forceUpdates);
    }
    if (index + 1 > this.peekLength()) {
      this._length.set(index + 1);
      this.syncIndex(index);
    }
  }
  shift() {
    const length = this.peekLength();
    if (!length) {
      return undefined;
    }
    const out = this._array.shift();
    this._length.set(length - 1);
    this.onArrangementUpdate();
    return this.noDeletedConstant(out.get());
  }
  partsSlice(start, end) {
    return this._array.slice(start, end).map(it => this.noDeletedConstant(it.get()));
  }
  toObjectNoDeps() {
    return this._array.map(item => item.peek());
  }
  unshift(value) {
    this._array.unshift(new Source_1.Source(value, {
      idAppend: "aval"
    }));
    this._length.set(this.peekLength() + 1);
    this.onArrangementUpdate();
  }
  onArrangementUpdate() {}
  syncIndex(index) {}
}
exports.SourceArrayBase = SourceArrayBase;
class SourceArray extends SourceArrayBase {
  constructor() {
    super(...arguments);
    this.lastUpdate = new Source_1.Source(undefined, {
      idAppend: "aupt"
    });
    this.anchorDeriver = DerivationManager_1.DerivationManager._getCurrentDeriver();
  }
  lastIndexOf() {
    throw new Error("TODO");
  }
  reduceRight() {
    throw new Error("TODO");
  }
  flatMap() {
    throw new Error("TODO");
  }
  concat(items) {
    return this.toObject().concat(items);
  }
  copyWithin(target, start, end) {
    const length = this.peekLength();
    end = end !== undefined ? end : length;
    let copyIndex = start || 0;
    for (let i = target || 0; i < length && copyIndex < end; (i++, copyIndex++)) {
      this.set(i, this.peek(copyIndex));
    }
    return this;
  }
  every(cb) {
    const length = this.peekLength();
    for (let i = 0; i < length; i++) {
      if (cb(this.getPart(i)) !== true) {
        return false;
      }
    }
    return true;
  }
  clear() {
    while (this._array.length) {
      this._array.pop();
      this.syncIndex(this._array.length);
    }
    this._length.set(0);
    return this;
  }
  entries() {
    const out = [];
    this.forEach((item, index) => out.push([index, item]));
    return out;
  }
  fill(value, start, end) {
    const length = this.peekLength();
    for (let i = start || 0; i < end || length; i++) {
      this.set(i, value);
    }
  }
  filter(cb) {
    const out = [];
    this.forEach(item => {
      if (cb(item)) {
        out.push(item);
      }
    });
    return out;
  }
  find(cb) {
    const index = this.findIndex(cb);
    return index === -1 ? undefined : this.getPart(index);
  }
  findIndex(cb) {
    this.orderMatters();
    const length = this.peekLength();
    for (let i = 0; i < length; i++) {
      const value = this.getPart(i);
      if (cb(value)) {
        return i;
      }
    }
    return -1;
  }
  flat(depth) {
    this.orderMatters();
    return this.toObject().flat(depth);
  }
  forEach(cb) {
    this.orderMatters();
    return this.forEachPart(cb);
  }
  has(index) {
    const indexSrc = this._getIndexObserver(index).get();
    const value = indexSrc.get();
    return value !== this.DELETED;
  }
  iterator() {
    const self = this;
    void this.length;
    return iterate();
    function* iterate() {
      for (let i = 0; i < self.length; i++) {
        yield self.get(i);
      }
    }
  }
  get(index) {
    return this.noDeletedConstant(this._getIndexObserver(index).get().get());
  }
  includes(item, fromIndex) {
    if (fromIndex !== undefined) {
      throw new Error("TODO: implement fromIndex");
    }
    this.lengthMatters();
    return this.indexOf(item) !== -1;
  }
  indexOf(item) {
    this.orderMatters();
    const length = this.peekLength();
    for (let i = 0; i < length; i++) {
      if (this.getPart(i) === item) {
        return i;
      }
    }
    this.lengthMatters();
    return -1;
  }
  join(separator) {
    this.orderMatters();
    this.lengthMatters();
    return this._array.map(it => it.get()).join(separator);
  }
  keys() {
    const out = [];
    const length = this.length;
    for (let i = 0; i < length; i++) {
      let item = this._getIndexObserver(i);
      if (item.get() && item.get().get() !== this.DELETED) {
        out.push(i);
      }
    }
    return out;
  }
  map(cb) {
    const out = [];
    this.forEach(item => out.push(cb(item)));
    return out;
  }
  reduce(cb) {
    let out;
    this.forEach((item, index) => {
      if (index === 0) {
        return out = item;
      }
      out = cb(out, item);
    });
    return out;
  }
  reverse() {
    const activeValues = this._array.splice(0, this._length.peek());
    this._array.splice(0, 0, ...activeValues.reverse());
    this.onArrangementUpdate();
    return this;
  }
  slice(start, end) {
    const length = this.peekLength();
    const out = [];
    for (let i = start; end !== undefined && i < end && i < length; i++) {
      out.push(this.get(i));
    }
    return out;
  }
  some(cb) {
    return this.findIndex(cb) !== -1;
  }
  sort(cb) {
    cb = cb || ((l, r) => {
      if (l < r) {
        return -1;
      }
      if (l > r) {
        return 1;
      }
      return 0;
    });
    const activeValues = this._array.splice(0, this._length.peek());
    this._array.splice(0, 0, ...activeValues.sort((a, b) => cb(a.peek(), b.peek())));
    this.onArrangementUpdate();
    return this;
  }
  splice(start, deleteCount, ...items) {
    if (start < 0) {
      throw new Error("TODO");
    }
    const length = this._length.peek();
    if (deleteCount === undefined) {
      deleteCount = length - start;
    }
    start = Math.min(start, length);
    const numDeleted = Math.min(length - start, deleteCount);
    const numAdded = items.length;
    const out = this._array.splice(start, deleteCount, ...items.map(value => new Source_1.Source(value)));
    this._length.set(Math.max(start, length) + (numAdded - numDeleted));
    this.onArrangementUpdate();
    return out.map(it => it.peek());
  }
  toObject() {
    this.orderMatters();
    const out = [];
    this.forEach(item => out.push(item));
    return out;
  }
  values() {
    return this.toObject();
  }
  onArrangementUpdate() {
    this.lastUpdate.set(Date.now());
    if (this.indexMap) {
      BasicDerivers_1.noDeps(() => Array.from(this.indexMap._static().keys()).forEach(key => this.indexMap.set(key, this.getSource(key))));
    }
  }
  syncIndex(index) {
    const indexedSrc = this.indexMap && this.indexMap.peek(this._array.length);
    if (indexedSrc) {
      if (this.peekLength() <= index) {
        indexedSrc.set(this.DELETED);
      } else {
        indexedSrc.set(this.peek(index));
      }
    }
  }
  _getIndexObserver(index) {
    if (!this.indexMap) {
      this.indexMap = new SourceMap_1.SourceMapBase(null, {
        id: "ai"
      });
      BasicDerivers_1.derive({
        fn: () => {
          this.orderMatters();
          Array.from(this.indexMap._static().keys()).forEach(key => this.indexMap.set(key, this.getSource(key)));
        },
        anchor: this.anchorDeriver
      });
    }
    if (!this.indexMap._static().has(index)) {
      BasicDerivers_1.noDeps(() => this.indexMap.set(index, this.getSource(index)));
    }
    return this.indexMap.getSource(index);
  }
  orderMatters() {
    void this.lastUpdate.get();
  }
  lengthMatters() {
    void this.length;
  }
}
exports.SourceArray = SourceArray;

},

// node_modules/helium-sdx/dist/Derivations/Sources/DdxSource.js @45
45: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const Source_1 = __fusereq(43);
const BasicDerivers_1 = __fusereq(40);
class DdxSource {
  constructor(ddx) {
    this.source = new Source_1.Source();
    BasicDerivers_1.derive(() => {
      this.source.set(ddx());
    });
  }
  get() {
    return this.source.get();
  }
  peek() {
    return this.source.peek();
  }
  getSource() {
    return this.source;
  }
}
exports.DdxSource = DdxSource;

},

// node_modules/helium-sdx/dist/Derivations/Sources/PromiseSource.js @46
46: function(__fusereq, exports, module){
"use strict";
Object.defineProperty(exports, "__esModule", {
  value: true
});
const Source_1 = __fusereq(43);
const DerivationManager_1 = __fusereq(36);
function promiseToSource(props, promise) {
  const promSource = new PromiseSource(props, promise);
  return promSource.get();
}
exports.promiseToSource = promiseToSource;
class PromiseSource {
  constructor(props, promise) {
    this.ddxScope = props.ddxScope || DerivationManager_1.DerivationManager._getCurrentDeriver();
    const ddxStore = this.ddxScope.getStore();
    ddxStore.promiseSources = ddxStore.promiseSources || ({});
    let stor = ddxStore.promiseSources[props.name];
    if (stor) {
      this.source = stor.source;
      this.promise = stor.promise;
    } else {
      ddxStore.promiseSources[props.name] = stor = {
        promise: this.promise = promise,
        source: this.source = new Source_1.Source()
      };
      promise.then(value => this.source.set(value)).catch(console.error);
    }
  }
  get() {
    return this.source.get();
  }
}
exports.PromiseSource = PromiseSource;

}
}, function(){
__fuse.r(1)
})