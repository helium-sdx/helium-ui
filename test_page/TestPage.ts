import { onDomReady, div, append, Sourcify, derive } from "../src";

const state = Sourcify({
  hat: "fez"
})

// derive(() => {
//   console.log(state.hat.slice(1));
// })

const app = renderApp();

onDomReady(() => {
  append(document.body, app);
  setTimeout(() => {
    state.hat = undefined;
  }, 1000)
})




function renderApp() {
  return div("TestApp", () => {
    return new URL("http://lala.com") as any;
    // return state.hat.slice(1);
  })
}