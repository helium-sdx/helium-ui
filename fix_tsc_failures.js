const { readFileSync, writeFileSync } = require("fs");
const { join } = require("path");


fixSourceProxyUtilities();
fixRootIndex();



function fixSourceProxyUtilities() {
  const fileFath = join(__dirname, "./dist/Sourcify/SourceProxyUtilities.d.ts");
  
  let rawText = String(readFileSync(fileFath));
  rawText = rawText.replace(
    "isProxied<TYPE>(checkMe: TYPE | Sourcified<TYPE>):",
    "isProxied<TYPE>(checkMe: TYPE | Sourcified<TYPE>):\n  // @ts-ignore\n "
  )
  writeFileSync(fileFath, rawText);
}


function fixRootIndex() {
  const fileFath = join(__dirname, "./dist/index.d.ts");
  
  let rawText = String(readFileSync(fileFath));
  rawText = rawText.replace(
    `export * from "helium-sdx";`,
    `// @ts-ignore\nexport * from "helium-sdx";`
  )
  writeFileSync(fileFath, rawText);
}