const browserEnv = require("browser-env");
browserEnv([
  "window", 
  "document", 
  "Event",
  "setTimeout",
  "location",
  "Node",
  "HTMLDivElement",
]);

global.requestAnimationFrame = (cb) => setTimeout(cb, 24);