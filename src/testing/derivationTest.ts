import { ExecutionContext } from "ava";
import { waitNFrames } from "./wait";
import { derive, DerivationManager, aLog } from "helium-sdx";

export async function deriverShouldRunNTimes(t: ExecutionContext<any>, num: number, ddx: () => any): Promise<void> {
  let count = 0;
  derive(() => {
    ddx();
    count++;
  });

  await waitNFrames(10);
  if (count === num) { 
    t.pass(); 
  } else {
    t.fail(`Deriver ran ${count} times instead of ${num}`);
  }
}




export interface IDeriverCheckArgs {
  t: ExecutionContext<any>,
  values: any[], 
  ddx: () => any,
  cb: () => any,
}


export function deriverShouldEqualValues(args: IDeriverCheckArgs): Promise<boolean> {
  return deriverCheck({
    ...args,
    matchType: "is"
  })
}

export function deriverShouldDeepEqualValues(args: IDeriverCheckArgs): Promise<boolean> {
  return deriverCheck({
    ...args,
    matchType: "deepEqual"
  })
}

function deriverCheck({t, matchType, values, ddx, cb}: IDeriverCheckArgs &{
  matchType: "is" | "deepEqual",
}): Promise<boolean> {
  let resolve: (value: boolean) => any;
  const out = new Promise<boolean>((res) => resolve = res);

  let count = 0;
  derive(() => {
    const value = ddx();
    t[matchType](values[count], value)
    count++;
  });
  setTimeout(() => {
    t.is(count, values.length);
    resolve(count === values.length);
  }, 500);

  cb();
  return out;
}


export interface IDeriverTestProps<VALUE> {
  ddx: () => VALUE;
  startingCheck?: (value: VALUE) => void;
  tests: IDeriverTest<VALUE>[];
  debug?: boolean;
}


export interface IDeriverTest<VALUE> {
  cb: () => void,
  checkValue: (value: VALUE) => void
}

export function derivationTest<VALUE>(
  t: ExecutionContext<any>, 
  {
    tests, 
    startingCheck,
    ddx,
    debug
  }: IDeriverTestProps<VALUE>
) {
  let resolve: () => any;
  const out = new Promise<boolean>((res) => resolve = res);

  const timeout = setTimeout(() => {
    t.fail("Derivation did not happen within 1s")
    resolve();
  }, 1000);

  let testNum = -1;
  let checkValue: (value: VALUE) => void = startingCheck;
  derive(async () => {
    if (debug && testNum == -1) {
      DerivationManager.debugDeriver(); 
      aLog("debugDeriver", "<First Pass>");
    }
    const value = ddx();
    if (checkValue) { checkValue(value); }
    
    testNum++;
    if (testNum == tests.length) {
      clearTimeout(timeout);
      await waitNFrames(4);
      return resolve();
    } else if (testNum > tests.length) {
      t.fail(`Test ran ${testNum} times instead of ${tests.length}`)
    }

    const nextTest = tests[testNum];
    await waitNFrames(2);
    
    checkValue = nextTest.checkValue;
    nextTest.cb();
  })

  return out;
}