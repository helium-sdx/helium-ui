export function wait(ms: number): Promise<void> {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  })
}



export function waitOneFrame(): Promise<void> {
  return wait(~~(1000/60));
}

export function waitNFrames(n: number): Promise<void> {
  return wait(n * (~~(1000/60)));
}