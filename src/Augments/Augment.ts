

export type ModType = "init" | "freeze" | "unfreeze";

export interface IModificationProps<STORE> {
  store: STORE;
  modType: ModType
}

export type ModFn<STORE> = (ref: HTMLElement, props: IModificationProps<STORE>) => void;

export class ElementAugment<STORE extends {} = {}> {
  protected _frozen = false;
  protected target: HTMLElement;
  protected store: STORE;

  constructor(
    protected modFn: ModFn<STORE>, 
    store: Partial<STORE> = {} as any
  ) {
    this.store = store as any;
  }

  public targetElement(modMe: HTMLElement) {
    this.target = modMe;
    this.runMod("init");
  }

  public get frozen() { return this._frozen; }

  public freeze() {
    if (this._frozen) { return; }
    this._frozen = true;
    this.runMod("freeze");
  }

  public unfreeze() {
    if (!this._frozen) { return; }
    this._frozen = false;
    this.runMod("unfreeze");
  }

  protected runMod(modType: ModType) {
    this.modFn(this.target, {
      store: this.store,
      modType
    })
  }
}