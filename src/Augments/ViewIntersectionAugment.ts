import { div } from '../El-Tool/Factories';
import { el, AugmentCb, BaseNode } from '../El-Tool/El-Tool';
import { ElementAugment } from './Augment';

export type ThresholdType = number | "any" | "covering" | "contained" | "maxFill";

export interface AugmentedIntersectionObserverInit extends Omit<IntersectionObserverInit, 'threshold'> {
  threshold?: ThresholdType | ThresholdType[];
}


export type InitArg = ThresholdType 
  | (ThresholdType[])
  | AugmentedIntersectionObserverInit;

export function addIntersectionObserver(
  nodes: Element | Element[],
  init: InitArg,
  cb: IntersectionObserverCallback,
) {
  let thresholds;
  const isObject = (init): init is AugmentedIntersectionObserverInit => {
    return (init && typeof init === "object") && !Array.isArray(init);
  };
  if (isObject(init)) {
    thresholds = init.threshold;
  }
  if (!Array.isArray(thresholds)) {
    thresholds = [thresholds];
  }

  let startLength = thresholds.length;
  const normalThresholds= thresholds.map((it) => {
    switch (it) {
      case "contained":
      case "maxFill": return 1;
      case "any": return 0;
    }
    return it;
  }).filter((it) => typeof it === "number") as number[];
  
  const needsCoverMod = startLength > normalThresholds.length;

  if (!Array.isArray(nodes)) {
    nodes = [nodes];
  }

  if (needsCoverMod) {
    assertStyling();
    const checkUs = nodes.map((node) => {
      let checkMe: HTMLElement;
      const addMe = div("CoverCheckerPositioner", 
        checkMe = div("CoverChecker")
      );
      node.classList.add("hintRelative");
      node.prepend(addMe);
      return checkMe;
    });
    const watcher = new IntersectionObserver(cb, {
      threshold: 1
    });
    checkUs.forEach((node) => watcher.observe(node));
  }

  if (normalThresholds.length === 0) {
    return;
  }

  const options = isObject(init) ? init as IntersectionObserverInit : {};
  options.threshold = normalThresholds;

  const watcher = new IntersectionObserver(cb, options);
  setTimeout(() => {
    (nodes as any).forEach((node) => watcher.observe(node));
  }, 0);
}




export type CbArg = AugmentCb<[IntersectionObserverEntry, IntersectionObserver]>;

export function intersectionAug(init: InitArg, cb: CbArg) {
  return new ElementAugment((ref, { modType }) => {
    if (modType !== "init") { return; }
    addIntersectionObserver(ref, init, (entries, observer) => {
      cb(ref, entries[0], observer);
    });
  })
}




let styleEl: HTMLStyleElement;
function assertStyling() {
  if (styleEl) { return; }
  styleEl = el("style");
  styleEl.innerHTML = `
    .hintRelative {
      position: relative;
    }

    .CoverCheckerPositioner {
      position: absolute;
      top: 0px;
      left: 0px;
      right: 0px;
      bottom: 0px;
      pointer-events: none;
      visibility: hidden;
    }
    
    .CoverChecker {
      position: sticky;
      height: 100vh;
      width: 100vw;
      top: 0px; 
    }
  `;
  document.head.prepend(styleEl);
}