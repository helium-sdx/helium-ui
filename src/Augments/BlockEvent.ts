import { ElementAugment } from "./Augment";


export interface IBlockEventAugProps {
  stopImmediatePropagation?: boolean;
  stopPropagation?: boolean;
  preventDefault?: boolean;
  returnFalse?: boolean;
}

/**
 * @importance 12
 * @param init 
 * @param cb 
 */
export function blockEventAug(eventType: string, props: IBlockEventAugProps = {}) {
  return new ElementAugment((ref, modProps) => {
    if (modProps.modType !== "init") { return; }

    ref.addEventListener(eventType, (ev) => { 
      if (props.preventDefault !== false) {
        ev.preventDefault(); 
      }
      if (props.stopPropagation !== false) {
        ev.stopPropagation(); 
      }
      if (props.stopImmediatePropagation !== false) {
        ev.stopImmediatePropagation(); 
      }
      if (props.returnFalse !== false) {
        return false; 
      }
    });
  })
}