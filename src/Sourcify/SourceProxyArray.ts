import { SourcifiedArray } from "./SourceProxyShallowTypes";
import { Innards } from "../El-Tool/El-Tool";
import { SourceArray } from "../Derivations/Sources/SourceArray";

import { 
  SourceProxyArray as _CoreSourceProxyArray, Sourcified,
} from "helium-sdx";




export class SourceProxyArray<TARGET extends any[]> extends _CoreSourceProxyArray<TARGET> {
  constructor(target: TARGET) {
    super(target, new SourceArray() as any);
  }
  
  protected getSrcArray() {
    return this.srcArray as SourceArray<Sourcified<TARGET[number]>>;
  }

  // TODO - test
  public renderMap(renderFn: (value: TARGET[number]) => Innards) {
    this.refreshAll();
    return this.getSrcArray().renderMap(renderFn);
  }

  // TODO - test
  public renderMapKeyed(renderFn: (value: TARGET[number]) => Innards) {
    this.refreshAll();
    return this.getSrcArray().renderMapKeyed(renderFn);
  }
}



export const ASSERT_SHALLOW_TYPE_MATCH_ARRAY: keyof (SourceProxyArray<string[]> & {
  getProxyManager: any;
  toObjectNoDeps: any;
  pretend: any;
  [key: number]: any;

  // TODO LIST
  toString: any;
  toLocaleString: any;
  lastIndexOf: any;
  reduceRight: any;
  copyWithin: any;
  values: any;
  flatMap: any;
}) = {} as keyof SourcifiedArray<string[]>;