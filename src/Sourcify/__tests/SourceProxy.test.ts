import test, { ExecutionContext } from "ava";
import { derivationTest } from "../../testing/derivationTest"
import { Sourcified } from "../SourceProxyShallowTypes";
import { Sourcify } from "../SourceProxyUtilities";



function createNewState(arr: (string | number)[] = ["a", "b", "c", "d"]) {
  return Sourcify({arr, b: true})
}


function assertStateProxySync(t: ExecutionContext, state: Sourcified<any>) {
  t.deepEqual(state.toObjectNoDeps(), state.toObject());
}


test('SourceProxy - Array - "in"', async (t) => {
  const state = createNewState([1, 2, 3]);
  t.is(1 in state.arr, true);
  t.is(9 in state.arr, false);
  assertStateProxySync(t, state);
});

test('SourceProxy - Object - "in"', async (t) => {
  const state = createNewState([1, 2, 3]);
  t.is("b" in state, true);
  t.is("doop" in state, false);
  assertStateProxySync(t, state);
});


test('SourceProxy - Array - "in" vs index', async (t) => {
  const state = createNewState([1, 2, 3]);
  await derivationTest(t, {
    ddx: () => (9 in state.arr),
    startingCheck: (value) => t.deepEqual(value, false),
    tests: [
      { cb: () => state.arr[9] = 9,
        checkValue: (value) => { 
          t.deepEqual(value, true);
          assertStateProxySync(t, state);
        }
      },
    ]
  });
});

test('SourceProxy - Object - "in" vs index', async (t) => {
  const state = createNewState([1, 2, 3]);
  await derivationTest(t, {
    ddx: () => "doop" in state,
    startingCheck: (value) => t.deepEqual(value, false),
    tests: [
      { cb: () => state["doop"] = 9,
        checkValue: (value) => { 
          t.deepEqual(value, true);
          assertStateProxySync(t, state);
        }
      },
    ]
  });
});


test('SourceProxy - Array - Object.keys() vs delete', async (t) => {
  const state = createNewState([1, 2, 3]);
  await derivationTest(t, {
    ddx: () => Object.keys(state.arr),
    startingCheck: (value) => t.deepEqual(value, ["0", "1", "2"]),
    tests: [
      { cb: () => delete state.arr[1],
        checkValue: (value) => { 
          t.deepEqual(value, ["0", "2"]);
          assertStateProxySync(t, state);
        }
      },
    ]
  });
});



test('SourceProxy - Object - Object.keys() vs delete', async (t) => {
  const state = createNewState([1, 2, 3]);
  await derivationTest(t, {
    ddx: () => Object.keys(state),
    startingCheck: (value) => t.deepEqual(value, ["arr", "b"]),
    tests: [
      { cb: () => delete state["arr"],
        checkValue: (value) => { 
          t.deepEqual(value, ["b"]);
          assertStateProxySync(t, state);
        }
      },
    ]
  });
});


test('SourceProxy - Array - Object.entries() vs delete', async (t) => {
  const state = createNewState([0, 1, 2]);
  await derivationTest(t, {
    ddx: () => Object.entries(state.arr),
    startingCheck: (value) => t.deepEqual(value, [["0", 0], ["1", 1], ["2", 2]]),
    tests: [
      { cb: () => delete state.arr[1],
        checkValue: (value) => { 
          t.deepEqual(value, [["0", 0], ["2", 2]]);
          assertStateProxySync(t, state);
        }
      },
    ]
  });
});

test('SourceProxy - Object - Object.entries() vs delete', async (t) => {
  const state = createNewState([]);
  const arr = state.arr;
  await derivationTest(t, {
    ddx: () => Object.entries(state),
    startingCheck: (value) => t.deepEqual(value, [["arr", arr], ["b", true]]),
    tests: [
      { cb: () => delete state.b,
        checkValue: (value) => { 
          t.deepEqual(value, [["arr", arr]]);
          assertStateProxySync(t, state);
        }
      },
    ]
  });
});


test('SourceProxy - Array - Object.values() vs push()', async (t) => {
  const state = createNewState([0, 1, 2]);
  await derivationTest(t, {
    ddx: () => Object.values(state.arr),
    startingCheck: (value) => t.deepEqual(value, [0, 1, 2]),
    tests: [
      { cb: () => state.arr.push(3),
        checkValue: (value) => { 
          t.deepEqual(value, [0, 1, 2, 3]);
          assertStateProxySync(t, state);
        }
      },
    ]
  });
});


test('SourceProxy - Array - delete', async (t) => {
  const state = createNewState([1, 2, 3]);
  await derivationTest(t, {
    ddx: () => (2 in state.arr),
    startingCheck: (value) => t.deepEqual(value, true),
    tests: [
      { cb: () => delete state.arr[2],
        checkValue: (value) => { 
          t.deepEqual(value, false);
          assertStateProxySync(t, state);
        }
      },
    ]
  });
});

test('SourceProxy - Object - delete', async (t) => {
  const state = createNewState([]);
  await derivationTest(t, {
    ddx: () => "b" in state,
    startingCheck: (value) => t.deepEqual(value, true),
    tests: [
      { cb: () => delete state.b,
        checkValue: (value) => { 
          t.deepEqual(value, false);
          assertStateProxySync(t, state);
        }
      },
    ]
  });
});



test('SourceProxy - Array - for of', async (t) => {
  const state = createNewState([1, 2, 3]);
  await derivationTest(t, {
    ddx: () => {
      for (const it of state.arr) {
        if (it === 3) { return true; }
      }
      return null;
    },
    startingCheck: (value) => t.deepEqual(value, true),
    tests: [
      { cb: () => delete state.arr[2],   
        checkValue: (value) => { 
          t.deepEqual(value, null);
          assertStateProxySync(t, state);
        }
      },
    ]
  });
});

test('SourceProxy - Object - for of', async (t) => {
  const state = createNewState([]);
  await derivationTest(t, {
    ddx: () => {
      for (const it of state) {
        if (it === true) { return true; }
      }
      return null;
    },
    startingCheck: (value) => t.deepEqual(value, true),
    tests: [
      { cb: () => delete state.b,   
        checkValue: (value) => { 
          t.deepEqual(value, null);
          assertStateProxySync(t, state);
        }
      },
    ]
  });
});


test('SourceProxy - Array - for in', async (t) => {
  const state = createNewState([1,1,1]);
  await derivationTest(t, {
    ddx: () => {
      for (const it in state.arr) {
        if (it === "3") { return true; }
      }
      return null;
    },
    startingCheck: (value) => t.deepEqual(value, null),
    tests: [
      { cb: () => state.arr[3] = 1,   
        checkValue: (value) => { 
          t.deepEqual(value, true);
          assertStateProxySync(t, state);
        }
      },
    ]
  });
});

test('SourceProxy - Object - for in', async (t) => {
  const state = createNewState([]);
  await derivationTest(t, {
    ddx: () => {
      for (const it in state) {
        if (it === "foo") { return true; }
      }
      return null;
    },
    startingCheck: (value) => t.deepEqual(value, null),
    tests: [
      { cb: () => state["foo"] = true,   
        checkValue: (value) => { 
          t.deepEqual(value, true);
          assertStateProxySync(t, state);
        }
      },
    ]
  });
});
