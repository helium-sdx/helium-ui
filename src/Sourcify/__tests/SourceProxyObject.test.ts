import test, { ExecutionContext } from "ava";
import { deriverShouldEqualValues, derivationTest } from "../../testing/derivationTest"
import { Sourcified } from "../../Sourcify/SourceProxyShallowTypes";
import { Sourcify } from "../SourceProxyUtilities";



function createNewState() {
  return Sourcify({
    obj: {
      a: "foo",
      b: "bar",
      child: {
        grandchild: "qux"
      }
    }
  })
}

function assertStateProxySync(t: ExecutionContext, state: Sourcified<any>) {
  t.deepEqual(state.toObjectNoDeps(), state.toObject());
}


test('SourceProxyObject - assign and get match', async (t) => {
  const state = createNewState();
  state.obj.a = "baz";
  t.is(state.obj.a, "baz");
});


test('SourceProxyObject - assign should run deriver', async (t) => {
  const state = createNewState();
  const out = await deriverShouldEqualValues({
    t,
    values: ["foo", "baz"], 
    ddx: () => state.obj.a,
    cb: () => state.obj.a = "baz",
  });
  t.is(out, true);
});


test('SourceProxyObject - parent assign should run deriver', async (t) => {
  const state = createNewState();
  await deriverShouldEqualValues({
    t,
    values: ["qux", "baz"], 
    ddx: () => state.obj.child.grandchild,
    cb: () => state.obj.set("child", { grandchild: "baz" }),
  });
});



test("SourceProxyObject - keys()", async (t) => {
  const state = createNewState();
  await derivationTest(t, {
    ddx: () => Array.from(Object.keys(state.obj)),
    startingCheck: (value) => t.deepEqual(value, ["a", "b", "child"]),
    tests: [
      { cb: () => state.obj["g"] = 9,
        checkValue: (value) => { 
          t.deepEqual(value, ["a", "b", "child", "g"]); 
          assertStateProxySync(t, state);
        }
      },
    ]
  })
})

