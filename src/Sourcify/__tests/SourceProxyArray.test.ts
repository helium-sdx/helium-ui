import test, { ExecutionContext } from "ava";
import { derivationTest, deriverShouldRunNTimes, deriverShouldEqualValues } from "../../testing/derivationTest"

import { waitNFrames } from "../../testing/wait";
import { Sourcified } from "../../Sourcify/SourceProxyShallowTypes";
import { Sourcify } from "../SourceProxyUtilities";
import { derive } from "helium-sdx";



function createNewState(arr: (string | number)[] = ["a", "b", "c", "d"]) {
  return Sourcify({arr})
}


function assertStateProxySync(t: ExecutionContext, state: Sourcified<{arr: any[]}>) {
  t.deepEqual(state.toObjectNoDeps(), state.toObject());
}


test('SourceProxyArray - assign and get match', async (t) => {
  const state = createNewState([1, 2, 3]);
  state.arr[1] = "baz";
  t.is(state.arr[1], "baz");
  assertStateProxySync(t, state);
});


test('SourceProxyArray - assign should run deriver', async (t) => {
  const state = createNewState([1, 2, 3]);
  await deriverShouldEqualValues({
    t,
    values: [2, 9], 
    ddx: () => state.arr[1],
    cb: () => state.arr[1] = 9,
  });

  assertStateProxySync(t, state);
});



test('SourceProxyArray - object assign to empty place should run deriver', async (t) => {
  const state = Sourcify({ 
    arr: [
      { a: true },
      { a: false }
    ]
  });
  await derivationTest(t, {
    ddx: () => state.arr[4],
    startingCheck: (value) => value === undefined,
    tests: [
      { cb: () => state.arr.set(4, {a: true}),
        checkValue: (value) => { 
          t.deepEqual(value.toObjectNoDeps(), { a: true }); 
        }
      },
    ]
  })
});


test('SourceProxyArray - grandparent assign should run deriver', async (t) => {
  const state = Sourcify({
    arr: [
      1,
      2,
      [ "a", "b"]
    ]
  })

  await deriverShouldEqualValues({
    t,
    values: ["b", "baz"], 
    ddx: () => {
      return state.arr[2][1];
    },
    cb: () => state.arr.overwrite([5, 5, ["ant", "baz"]]),
  });

  assertStateProxySync(t, state);
});


test("SourceProxyArray - toObject() always derive", async (t) => {
  const state = createNewState([1, 2, 3]);
  await derivationTest(t, {
    ddx: () => state.arr.toObject(),
    tests: [
      { cb: () => state.arr.set(1, 9),
        checkValue: (value) => { 
          t.deepEqual(value, [1, 9, 3]); 
          assertStateProxySync(t, state);
        }
      },
    ]
  })
});



test("SourceProxyArray - toObjectNoDeps() no derive", async (t) => {
  const state = createNewState([1, 2, 3, 4]);
  const out = deriverShouldRunNTimes(t, 1, () => state.arr.toObjectNoDeps())
  state.arr.push(5);
  await out;
  assertStateProxySync(t, state);
});


test('SourceProxyArray - pop()', async (t) => {
  const state = createNewState([1, 2, 3, 4]);
  await derivationTest(t, {
    ddx: () => state.arr.toObject(),
    tests: [
      { cb: () => state.arr.pop(),
        checkValue: (value) => { 
          t.deepEqual(value, [1, 2, 3]); 
          assertStateProxySync(t, state);
        }
      }
    ]
  })
});


test('SourceProxyArray - pop() vs indexing', async (t) => {
  const state = createNewState([1, 2, 3, 4]);
  const item = state.arr.getPart(3);
  await derivationTest(t, {
    ddx: () => [state.arr.get(1), state.arr.get(3), item],
    tests: [
      { cb: () => state.arr.pop(),
        checkValue: (value) => { 
          t.deepEqual(value, [2, undefined, 4]); 
          assertStateProxySync(t, state);
        }
      },
    ]
  })
});


test('SourceProxyArray - remove()', async (t) => {
  const state = createNewState([1, 2, 2, 3]);
  await derivationTest(t, {
    ddx: () => state.arr.toObject(),
    tests: [
      { cb: () => state.arr.remove(2),
        checkValue: (value) => { 
          t.deepEqual(value, [1, 3]); 
          assertStateProxySync(t, state);
        }
      }
    ]
  })
});


test('SourceProxyArray - remove() vs indexing', async (t) => {
  const state = createNewState([1, 2, 2, 3]);
  const item = state.arr.getPart(3);
  await derivationTest(t, {
    ddx: () => [state.arr.get(1), state.arr.get(3), item],
    tests: [
      { cb: () => state.arr.remove(2),
        checkValue: (value) => { 
          t.deepEqual(value, [3, undefined, 3]);
          assertStateProxySync(t, state);
        }
      },
    ]
  })
});


test('SourceProxyArray - shift()', async (t) => {
  const state = createNewState([1, 2, 3]);
  await derivationTest(t, {
    ddx: () => state.arr.toObject(),
    tests: [
      { cb: () => state.arr.shift(),
        checkValue: (value) => { 
          t.deepEqual(value, [2, 3]); 
          assertStateProxySync(t, state);
        }
      }
    ]
  })
});


test('SourceProxyArray - shift() vs indexing', async (t) => {
  const state = createNewState([1, 2, 3, 4]);
  const item = state.arr.getPart(3);
  await derivationTest(t, {
    ddx: () => [state.arr.get(1), state.arr.get(3), item],
    tests: [
      { cb: () => state.arr.shift(),
        checkValue: (value) => { 
          t.deepEqual(value, [3, undefined, 4]); 
          assertStateProxySync(t, state);
        }
      },
    ]
  })
});


test('SourceProxyArray - partsSlice() vs index', async (t) => {
  const state = createNewState([1, 2, 3, 4]);
  const out = deriverShouldRunNTimes(t, 2, () => state.arr.partsSlice(1, 3));
  state.arr.shift();
  await waitNFrames(2);
  state.arr.set(1, 9);
  await out;
  assertStateProxySync(t, state);
});


test('SourceProxyArray - slice() vs index', async (t) => {
  const state = createNewState([1, 2, 3, 4]);
  await derivationTest(t, {
    ddx: () => state.arr.slice(1, 3).toObjectNoDeps(),
    startingCheck: (value) => t.deepEqual(value, [2, 3]),
    tests: [
      { cb: () => state.arr.shift(),
        checkValue: (value) => {
          t.deepEqual(value, [3, 4]);
          assertStateProxySync(t, state);
        },
      },
    ]
  })
});


test('SourceProxyArray - sort() vs index', async (t) => {
  const state = createNewState([3, 2, 1]);
  await derivationTest(t, {
    ddx: () => state.arr.get(2),
    startingCheck: (value) => t.deepEqual(value, 1),
    tests: [
      { cb: () => state.arr.sort(),
        checkValue: (value) => { 
          t.deepEqual(value, 3); 
          assertStateProxySync(t, state);
        }
      },
    ]
  })
});


test('SourceProxyArray - includes()', async (t) => {
  const state = createNewState([3, 2, 1]);
  await derivationTest(t, {
    ddx: () => state.arr.includes(2),
    startingCheck: (value) => t.deepEqual(value, true),
    tests: [
      { cb: () => state.arr[1] = 9,
        checkValue: (value) => { 
          t.deepEqual(value, false); 
          assertStateProxySync(t, state);
        }
      },
    ]
  })
});


test('SourceProxyArray - find() and reverse()', async (t) => {
  const state = createNewState([1, 2, 3]);
  await derivationTest(t, {
    ddx: () => state.arr.find((it: number) => (it % 2 === 0)),
    startingCheck: (value) => t.deepEqual(value, 2),
    tests: [
      { cb: () => state.arr[0] = 4,
        checkValue: (value) => { t.deepEqual(value, 4); }
      },
      { cb: () => state.arr.reverse(),
        checkValue: (value) => {
          t.deepEqual(value, 2)
          assertStateProxySync(t, state);
        },
      },
    ]
  })
});

test('SourceProxyArray - findIndex()', async (t) => {
  const state = createNewState([1, 2, 3]);
  await derivationTest(t, {
    ddx: () => state.arr.findIndex((it: number) => (it % 2 === 0)),
    startingCheck: (value) => t.deepEqual(value, 1),
    tests: [
      { cb: () => state.arr[0] = 4,
        checkValue: (value) => { t.deepEqual(value, 0); }
      },
      { cb: () => state.arr.reverse(),
        checkValue: (value) => {
          t.deepEqual(value, 1)
          assertStateProxySync(t, state);
        },
      },
    ]
  })
});


test('SourceProxyArray - find() vs index', async (t) => {
  const state = createNewState([1, 2, 3, 4]);
  const out = deriverShouldRunNTimes(t, 2, () => {
    state.arr.find((it: number) => (it % 2 === 0))
  });
  state.arr.pop();  // should not run
  await waitNFrames(3);
  state.arr.set(1, 9);
  await out;
  assertStateProxySync(t, state);
});


test('SourceProxyArray - join()', async (t) => {
  const state = createNewState([1, 2, 3]);
  await derivationTest(t, {
    ddx: () => state.arr.join(""),
    startingCheck: (value) => t.deepEqual(value, "123"),
    tests: [
      { cb: () => state.arr[0] = 4,
        checkValue: (value) => { t.deepEqual(value, "423"); }
      },
      { cb: () => state.arr.reverse(),
        checkValue: (value) => {
          t.deepEqual(value, "324");
          assertStateProxySync(t, state);
        },
      },
    ]
  })
});


test("SourceProxyArray - length", async (t) => {
  const state = createNewState([1, 2, 3]);
  await derivationTest(t, {
    ddx: () => state.arr.length,
    tests: [
      { cb: () => state.arr.push(4),
        checkValue: (value) => { 
          t.deepEqual(value, 4); 
          assertStateProxySync(t, state);
        }
      },
    ]
  })
})


test("SourceProxyArray - clear()", async (t) => {
  const state = createNewState([1, 2, 3]);
  await derivationTest(t, {
    ddx: () => state.arr.join(""),
    tests: [
      { cb: () => state.arr.clear(),
        checkValue: (value) => { 
          t.deepEqual(value, ""); 
          assertStateProxySync(t, state);
        }
      },
    ]
  })
})


test("SourceProxyArray - filter()", async (t) => {
  const state = createNewState([1, 2, 2, 3]);
  await derivationTest(t, {
    ddx: () => state.arr.filter((it) => it !== 2).toObjectNoDeps(),
    tests: [
      { cb: () => state.arr.clear(),
        checkValue: (value) => { 
          t.deepEqual(value, []); 
          assertStateProxySync(t, state);
        }
      },
    ]
  })
})


test("SourceProxyArray - forEach()", async (t) => {
  const state = createNewState([1, 2, 2, 3]);
  await derivationTest(t, {
    ddx: () => {
      const out = [];
      state.arr.forEach((it: number) => out.push(it * 2));
      return out;
    },
    tests: [
      { cb: () => state.arr[2] = 4,
        checkValue: (value) => { 
          t.deepEqual(value, [2, 4, 8, 6]); 
          assertStateProxySync(t, state);
        }
      },
    ]
  })
})


test("SourceProxyArray - forEachPart()", async (t) => {
  const state = createNewState([1, 2, 2, 3]);
  const out = deriverShouldRunNTimes(t, 2, () => {
    let sum = 0;
    state.arr.forEachPart((it: number) => sum += it);
    return sum;
  });
  state.arr.set(2, 9);  
  await waitNFrames(5);
  state.arr.reverse(); // should not run
  await out;
  assertStateProxySync(t, state);
})


test("SourceProxyArray - indexOf()", async (t) => {
  const state = createNewState([1, 2, 2, 3]);
  await derivationTest(t, {
    ddx: () => state.arr.indexOf(3),
    tests: [
      { cb: () => state.arr.remove(2),
        checkValue: (value) => { 
          t.deepEqual(value, 1); 
          assertStateProxySync(t, state);
        }
      },
    ]
  })
});


test("SourceProxyArray - keys()", async (t) => {
  const state = createNewState([7, 8, 9]);
  await derivationTest(t, {
    ddx: () => state.arr.keys(),
    tests: [
      { cb: () => state.arr.remove(9),
        checkValue: (value) => { 
          t.deepEqual(value, [0, 1]); 
          assertStateProxySync(t, state);
        }
      },
    ]
  })
});


test("SourceProxyArray - entries()", async (t) => {
  const state = createNewState([7, 8, 9]);
  await derivationTest(t, {
    ddx: () => state.arr.entries(),
    tests: [
      { cb: () => state.arr.remove(9),
        checkValue: (value) => { 
          t.deepEqual(value, [[0, 7], [1, 8]]); 
          assertStateProxySync(t, state);
        }
      },
    ]
  })
});


test("SourceProxyArray - map()", async (t) => {
  const state = createNewState([1, 2, 3]);
  await derivationTest(t, {
    ddx: () => state.arr.map((it: number) => it + 2).toObjectNoDeps(),
    tests: [
      { cb: () => state.arr[2] = 5,
        checkValue: (value) => { 
          t.deepEqual(value, [3, 4, 7]); 
          assertStateProxySync(t, state);
        }
      },
    ]
  })
});


test("SourceProxyArray - splice()", async (t) => {
  const state = createNewState([1, 2, 3]);
  await derivationTest(t, {
    ddx: () => state.arr.toObject(),
    tests: [
      { cb: () => state.arr.splice(1, 1, ...[4, 4]),
        checkValue: (value) => { 
          t.deepEqual(value, [1, 4, 4, 3]); 
          assertStateProxySync(t, state);
        }
      },
    ]
  })
});



test("SourceProxyArray - fill()", async (t) => {
  const state = createNewState([1, 2, 3, 4]);
  await derivationTest(t, {
    ddx: () => state.arr.toObject(),
    tests: [
      { cb: () => state.arr.fill(7, 1, 3),
        checkValue: (value) => { 
          t.deepEqual(value, [1, 7, 7, 4]); 
          assertStateProxySync(t, state);
        }
      },
    ]
  })
});


test("SourceProxyArray - every()", async (t) => {
  const state = createNewState([1, 2, 3, 4]);
  await derivationTest(t, {
    ddx: () => state.arr.every((it) => it !== 3),
    startingCheck: (value) => t.deepEqual(value, false),
    tests: [
      { cb: () => state.arr[2] = 5,
        checkValue: (value) => { 
          t.deepEqual(value, true); 
          assertStateProxySync(t, state);
        }
      },
    ]
  })
});



test("SourceProxyArray - some()", async (t) => {
  const state = createNewState([1, 2, 3, 4]);
  await derivationTest(t, {
    ddx: () => state.arr.some((it) => it === 5),
    startingCheck: (value) => t.deepEqual(value, false),
    tests: [
      { cb: () => state.arr[2] = 5,
        checkValue: (value) => { 
          t.deepEqual(value, true); 
          assertStateProxySync(t, state);
        }
      },
    ]
  })
});


test("SourceProxyArray - reduce()", async (t) => {
  const state = createNewState([1, 2, 3, 4]);
  await derivationTest(t, {
    ddx: () => state.arr.reduce<number>((sum, val: number) => sum + val),
    startingCheck: (value) => t.deepEqual(value, 10),
    tests: [
      { cb: () => state.arr[2] = 5,
        checkValue: (value) => { 
          t.deepEqual(value, 12); 
          assertStateProxySync(t, state);
        }
      },
    ]
  })
});


test("SourceProxyArray - unshift()", async (t) => {
  const state = createNewState([1, 2, 3, 4]);
  await derivationTest(t, {
    ddx: () => state.arr.toObject(),
    tests: [
      { cb: () => state.arr.unshift(5),
        checkValue: (value) => { 
          t.deepEqual(value, [5, 1, 2, 3, 4]); 
          assertStateProxySync(t, state);
        }
      },
    ]
  })
});




test("SourceProxyArray - flat()", async (t) => {
  const state = Sourcify([1, 2, [1, 2], [3, 4]]);
  await derivationTest(t, {
    ddx: () => state.flat().toObject(),
    tests: [
      { cb: () => state[3][1] = 5,
        checkValue: (value) => { 
          t.deepEqual(value, [1, 2, 1, 2, 3, 5]); 
          t.deepEqual(state.toObjectNoDeps(), state.toObject());
        }
      },
    ]
  })
});


test("SourceProxyArray - concat()", async (t) => {
  const state = createNewState([1, 2, 3]);
  await derivationTest(t, {
    ddx: () => state.arr.concat([5, 6]).toObjectNoDeps(),
    tests: [
      { cb: () => state.arr[3] = 4,
        checkValue: (value) => { 
          t.deepEqual(value, [1, 2, 3, 4, 5, 6]); 
          t.deepEqual(state.toObjectNoDeps(), state.toObject());
        }
      },
    ]
  })
});


test("SourceProxyArray - deriver nesting should cease with scope loss", async (t) => {
  const state = createNewState([1, 2, 3]);
  let count = 0;
  
  derive(() => {
    const a = state.arr[0];
    if (a === 1) {
      state.arr.renderMapKeyed(() => {
        count++;
      })
    }
  });
  await waitNFrames(2);
  state.arr[0] = 2;
  await waitNFrames(5);
  t.is(count, 3);
});