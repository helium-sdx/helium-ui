import { Innards, convertInnardsToElements } from '../El-Tool/El-Tool';
import { RMapFn, RkMapFn, AnyRMapFn } from '../Derivations/Sources/SourceBase';
import { Sourcified, ProxiedAny } from '../Sourcify/SourceProxyShallowTypes';
import { 
  Sourcify as _CoreSourcify,
  isProxied as _CoreIsProxied,
  Unsourcify as _CoreUnsourcify,
  SourcificationManager
} from "helium-sdx";


// import { SourceProxyArray } from "./SourceProxyArray";
// import { SourceProxyObject } from "./SourceProxyObject";

// SourcificationManager.createSourceProxyArray = (item) => {
//   console.log("Creating better arry");
//   return new SourceProxyArray(item);
// };
// SourcificationManager.createSourceProxyObject = (item) => new SourceProxyObject(item);




let SourcificationManagerModded = false;
/** Takes an ordinary object and makes it act like a recursive source.
 * Changing any property will update any deriver which accessed
 * that property or a child of it.
 * @importance 22 */
// export function Sourcify<TYPE extends ProxiedAny<any>>(target: TYPE): Sourcified<TYPE[keyof TYPE]>;
export function Sourcify<TYPE extends object>(target: TYPE): Sourcified<TYPE> {
  if (!SourcificationManagerModded) {
    const { SourceProxyArray } = require("./SourceProxyArray");
    const { SourceProxyObject } = require("./SourceProxyObject");
    SourcificationManager.createSourceProxyArray = (item) => new SourceProxyArray(item);
    SourcificationManager.createSourceProxyObject = (item) => new SourceProxyObject(item);
    SourcificationManagerModded = true;
  }
  return _CoreSourcify(target) as any;
}



export function Unsourcify<TYPE>(target: TYPE | Sourcified<TYPE>): TYPE {
  return _CoreUnsourcify(target as any);
}


export function isProxied<TYPE>(checkMe: TYPE | Sourcified<TYPE>)
  // @ts-ignore 
  : checkMe is ProxiedAny<TYPE>
{
  return _CoreIsProxied(checkMe as any);
}








export function renderMap<TYPE extends Array<any>>(arr: TYPE | Sourcified<TYPE>, renderFn: RMapFn<TYPE[number]>): ChildNode[];
export function renderMap<TYPE extends object>(obj: TYPE | Sourcified<TYPE>, renderFn: RMapFn<TYPE[keyof TYPE]>): ChildNode[];
/**
 * Efficiently renders and manages every child of an object or array.
 * This function renders without key dependency, so items will be moved if their index changes but they will not be re-rendered.
 * @importance 18
 * @param.obj Anything which has children that can be iterated over.  Can be an array, an object, or a proxy.
 * @param.renderFn This function will get called for every child.  It only takes one argument which is the child
 * @eg const list = Sourcify(["a", "b", "c", "d"]);\
 * const listEl = div("List", rmap(list, (value) => \
 *   div("ListValue", value),\
 * ));\
 * 
 * const obj = Sourcify({ a: 1, b: 2, c: 3});\
 * const objEl = div("Obj", rmap(obj, (value) => \
 *   div("ObjValue", value),\
 * ));\
 */
export function renderMap<TYPE extends object>(
  obj: Sourcified<TYPE> | TYPE, 
  renderFn: RMapFn<TYPE>
): ChildNode[] {
  return _anyRMap(false, obj, renderFn);
}


export function renderMapKeyed<TYPE extends Array<any>>(arr: TYPE | Sourcified<TYPE>, renderFn: RkMapFn<number, TYPE[number]>): ChildNode[];
export function renderMapKeyed<TYPE extends object>(obj: TYPE | Sourcified<TYPE>, renderFn: RkMapFn<keyof TYPE, TYPE[keyof TYPE]>): ChildNode[];
/**
 * Efficiently renders and manages every child of an object or array.  This
 * function is key/index dependent, so changes to an items index will cause it to be rendered from scratch.  Only use
 * this function if you need to display the index somehow.  Otherwise, renderMap() can handle re-rendering by only reordering
 * previous renders.
 * @importance 18
 * @param.obj Anything which has children that can be iterated over.  Can be an array, an object, or a proxy.
 * @param.renderFn This function will get called for every child.  It will recieve both the child and the key or index of the child.
 * @eg const list = Sourcify(["a", "b", "c", "d"]);\
 * const listEl = div("List", rkmap(list, (value, index) => \
 *   div("ListValue", `${value} is ${ index%2 ? "odd" : "even" }`),\
 * ));\
 * 
 * const obj = Sourcify({ a: 1, b: 2, c: 3});\
 * const objEl = div("Obj", rmap(obj, (value, key) => \
 *   div("ObjValue", `${key} is equal to ${value}`),\
 * ));\
 */
export function renderMapKeyed(
  obj: object | Sourcified<object>, 
  renderFn: RkMapFn<any, any>
): ChildNode[] {
  return _anyRMap(true, obj, renderFn);
}

function _anyRMap(
  withKey: boolean,
  obj: object | Sourcified<object>, 
  renderFn: AnyRMapFn<any, any>
): ChildNode[] {
  if (isProxied(obj)) {
    if (withKey) {
      return (obj as any).rkmap(renderFn);
    }
    return (obj as any).rmap(renderFn as any);

  } else if (obj && typeof obj === "object") {
    const out = [];
    let renders: Innards[];
    if (Array.isArray(obj)) {
      renders = obj.map((value, index) => renderFn(value, index));
    } else {
      renders = Object.entries(obj).map(([key, value]) => renderFn(value, key));
    }
    renders.forEach((render) => {
      const nodesList = convertInnardsToElements(render);
      if (!nodesList || !nodesList.length) { return; }
      nodesList.forEach((node) => out.push(node));
    });
    return out;
  }
}

