import { SourcifiedObject } from "./SourceProxyShallowTypes";
import { SourceMap } from "../Derivations/Sources/SourceMap";
import {
  SourceProxyObject as _CoreSourceProxyObject,
} from "helium-sdx";
import { Innards } from "../El-Tool/El-Tool";


export class SourceProxyObject<TARGET extends object> 
  extends _CoreSourceProxyObject<TARGET> 
{

  constructor(target: TARGET) {
    super(target, new SourceMap() as any);
  }

  protected getSrcMap(): SourceMap<keyof TARGET, TARGET[keyof TARGET]> {
    return this.srcMap as any;
  }

  // TODO - test
  public renderMap(renderFn: (value: TARGET[keyof TARGET]) => Innards) {
    this.refreshAll();
    return this.getSrcMap().renderMap(renderFn);
  }

  // TODO - test
  public renderMapKeyed<KEY extends keyof TARGET>(renderFn: (value: TARGET[KEY], key: KEY) => Innards) {
    this.refreshAll();
    return this.getSrcMap().renderMapKeyed(renderFn as any);
  }
}


interface IAssertTestType {a: boolean, b: number[], c: { d: string }};
export const ASSERT_SHALLOW_TYPE_MATCH: keyof (SourceProxyObject<IAssertTestType> & {
  getProxyManager: any;
  getSourceMap: any;
  toObjectNoDeps: any;
  pretend: any;
  a: any;
  b: any;
  c: any;

  // TODO LIST
  toString: any;
  toLocaleString: any;
  lastIndexOf: any;
  reduceRight: any;
  copyWithin: any;
  values: any;
  flatMap: any;
  forEach: any;
  entries: any;
  clear: any;
  delete: any;
  has: any;
  isEmpty: any;
  overwrite: any;
  peekHas: any;
  upsert: any;
}) = {} as keyof SourcifiedObject<IAssertTestType>;