import test from "ava";
import { inputTextbox } from "../Factories";
import { waitNFrames } from "../../testing/wait";
import { Source } from "helium-sdx";


test("Factories - inputTextbox() should derive value from source", async (t) => {
  const source = new Source("a");
  const domNode = inputTextbox(source);
  t.is(domNode.value, "a");

  source.set("b");
  await waitNFrames(2);
  t.is(domNode.value, "b");
}) 