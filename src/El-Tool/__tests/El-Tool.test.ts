import test from "ava";
import { div } from "../Factories";
import { classSplice, addClass, haveClass, removeChildren, setStyle } from "../El-Tool";


function getClassList(domNode: HTMLElement) {
  return Array.from(domNode.classList).sort().join(" ");
}

test("El-Tool - classSplice() should remove then add classes and pass-through", async (t) => {
  const domNode = div();
  classSplice(domNode, null, "a b c");  // add items from string
  t.is(getClassList(domNode), "a b c");
  classSplice(domNode, "a c");  // remove items from string
  t.is(getClassList(domNode), "b");

  classSplice(domNode, null, ["a", "b", "c", "d"]);  // add items from array
  t.is(getClassList(domNode), "a b c d");
  classSplice(domNode, ["b", "c"]);  // remove items from array
  t.is(getClassList(domNode), "a d");

  const out = classSplice(domNode, "a d", "b c d e");  // add and remove items from string
  t.is(getClassList(domNode), "b c d e");

  t.is(out, domNode);
}) 


test("El-Tool - addClass() should add class then pass-through", async (t) => {
  const domNode = div();
  const out = addClass("a b c", domNode);
  t.is(getClassList(domNode), "a b c");
  t.is(domNode, out);
}) 


test("El-Tool - haveClass() should add/remove based off boolean and pass-through", async (t) => {
  const domNode = div();
  const out = haveClass(domNode, "a b c", true); // add classes
  t.is(getClassList(out), "a b c");
  haveClass(out, "a c", false); // remove classes
  t.is(getClassList(out), "b");
  t.is(domNode, out);
}) 


// -------


test("El-Tool - removeChildren() should make an element empty and pass-through", async (t) => {
  const domNode = div(Array(5).fill(null).map(() => div()));
  t.is(domNode.childNodes.length, 5);
  const out = removeChildren(domNode)
  t.is(out.childNodes.length, 0);
  t.is(domNode, out);
});


test("El-Tool - setStyle() should update an items style and pass-through", async (t) => {
  const domNode = setStyle(div(), "color: red");
  t.true(domNode instanceof HTMLDivElement);
  t.is(domNode.style.color, "red")
});



test("El-Tool - innard arrays should flatten", async (t) => {
  const domNode = div([
    div(),
    [
      div(),
      div(),
    ]
  ])
  t.is(domNode.childElementCount, 3);
});