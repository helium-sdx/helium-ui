import test, { skip } from "ava";
import { waitNFrames } from "../../testing/wait";
import { Source } from "helium-sdx";
import { anchor, onUrlChange, followUrl } from "../Navigation";
import { changeFreezeState } from "../../HeliumBase";


skip("Navigation - anchor() should be able to take ddx as href", async (t) => {
  const source = new Source("a");
  const domNode = anchor({
    href: () => source.get()
  });
  // anchor must be unfrozen to work
  changeFreezeState(domNode as any, false);
  t.is(domNode.href, "a");

  source.set("b");
  await waitNFrames(2);
  t.is(domNode.href, "b");
}) 




test("Navigation - onUrlChange", async (t) => {
  let href: string;
  onUrlChange((ev) => {
    href = ev.newUrl.href
  });
  // can not set window.location
  followUrl("http://test.com");
  await waitNFrames(2);
  t.is(href, "http://test.com/");
}) 