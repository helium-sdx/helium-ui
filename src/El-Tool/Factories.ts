import { IElProps, Innards, el, standardizeInput, 
	NameOrPropsOrInnards, PropsOrInnards, 
	convertInnardsToRenders, ManyStaticInnards, isHTMLElement, BaseNode } from './El-Tool';
import { derive, Source } from 'helium-sdx';

/*****************************
* REGULAR CREATION SHORTHANDS
******************************/

/** Typical first factory arg.  Either className string, or factory props */
// export type StandardArg1<PROP> = string | PROP;

/** Typical second factory arg.  Either factory props, or innards to append */
// export type Props<PROP> = PROP | Innards;

/** Typical factory arguments */
export type StandardArgs<PROP = IElProps, INNARD = Innards> = [] 
	| [NameOrPropsOrInnards<PROP>]
	| [NameOrPropsOrInnards<PROP>, PropsOrInnards<PROP>]
	| [NameOrPropsOrInnards<PROP>, PropsOrInnards<PROP>, INNARD];

/** Typical factory arguments */
export type NoClassArgs<PROP = IElProps, INNARD = Innards> = [] 
	| [PropsOrInnards<PROP>]
	| [PropsOrInnards<PROP>, INNARD];

export type CoP<PROP = IElProps> = NameOrPropsOrInnards<PROP>;
export type PoI<PROP = IElProps> = PropsOrInnards<PROP>;
export type Inn = Innards;


type ElFunction<
	OUT extends ChildNode, 
	PROPS extends IElProps<OUT> = IElProps<OUT>, 
	INN = Innards
> = {
	(): OUT;
	(className: string): OUT;
	(props: PROPS): OUT;
	(className: string, props: PROPS): OUT;

	(innards: Exclude<INN, string | BaseNode>): OUT;
	// (className: string, innards: Exclude<INN, BaseNode>): OUT;
	// (props: PROPS, innards: Exclude<INN, BaseNode>): OUT;
	(className: string, innards: INN): OUT;
	(props: PROPS, innards: INN): OUT;
	(className: string, props: PROPS, innards: INN): OUT;

	(
		nameOrPropsOrInnards?: string | PROPS | Exclude<INN, string | BaseNode>, 
		propsOrInnards?: PROPS | Exclude<INN, BaseNode>, 
		innards?: INN
	): OUT;
};

type ElFunctionTargetInnards<OUT extends ChildNode, INN = Innards, PROPS extends IElProps<OUT> = IElProps<OUT>> = 
	ElFunction<OUT, PROPS, INN>

/** Helper function for any factories which have no special props */
export function _simpleFactory<K extends keyof HTMLElementTagNameMap, PROP = IElProps>(
	tagName: K, 
	[arg1, arg2, arg3]: StandardArgs<PROP>,
): HTMLElementTagNameMap[K]  {
	const {props, innards} = standardizeInput(arg1, arg2, arg3);
	return el(tagName, props, innards);
}


/** Creates a div from the StandardArgs.
 * @importance 21 */
export const div = function(...args: StandardArgs) {
	return _simpleFactory("div", args);
} as ElFunction<HTMLDivElement>

/** Creates a <span> element 
 * @importance 14
*/
export const span = function(...args: StandardArgs) {
	return _simpleFactory("span", args);
} as ElFunction<HTMLSpanElement>


/** Creates a <pre> element 
 * @importance 13
*/
export const pre = function(...args: StandardArgs) {
	return _simpleFactory("pre", args);
} as ElFunction<HTMLPreElement>





/** Creates a <ul> element 
 * @importance 13
*/
export const ul = function(...args: StandardArgs) {
	const {props, innards} = standardizeInput<IElProps, Innards>(...args);
	const listItems = Array.isArray(innards) ? innards : [innards];
	return el("ul", props, listItems.map((it) => {
		if (isHTMLElement(it) && (it.tagName === "LI" || it.nodeName === "#comment")) { 
			return it;
		}
		return li([it]);
	}));
} as ElFunction<HTMLUListElement>


/** Creates a <ol> element 
 * @importance 13
*/
export const ol = function(...args: StandardArgs) {
	const {props, innards} = standardizeInput<IElProps, Innards>(...args);
	const listItems = Array.isArray(innards) ? innards : [innards];
	return el("ol", props, listItems.map((it) => {
		if (isHTMLElement(it) && (it.tagName === "LI" || it.nodeName === "#comment")) { 
			return it;
		}
		return li([it]);
	}));
} as ElFunction<HTMLOListElement>


/** Creates a <li> element 
 * @importance 13
*/
export const li = function(...args: StandardArgs) {
	return _simpleFactory("li", args);
} as ElFunction<HTMLLIElement>




export type TableRows = ManyStaticInnards | Array<ManyStaticInnards>;
export type TableInnards = TableRows | (() => TableRows);

export const table = function(namePropChild?: CoP | TableInnards, propChild?: IElProps | TableInnards, child?: TableInnards) {
	const {props, innards} = standardizeInput(namePropChild as any, propChild as any, child as any);
	const rows = convertInnardsToRenders(innards as any);
	return el("table", props, rows.map((row) => toTr(row)));

	function toTr(row: any) {
		if (Array.isArray(row) && row.length === 1) {	row = row.pop(); }
		if (isHTMLElement(row) && (row.tagName === "TR" || row.nodeName === "#comment")) { 
			return row;  
		}
		if (typeof row === "function") {
			return () => toTr(row())
		}
		return tr(row);
	}
} as ElFunctionTargetInnards<HTMLTableElement, TableInnards>;


export const tr = function(...args: StandardArgs) {
	const {props, innards} = standardizeInput(...args as any);
	const cols = convertInnardsToRenders(innards);
	return el("tr", props, cols.flat().map((data) => toTd(data)));
	
	function toTd(data: any) {
		// if (Array.isArray(data) && data.length === 1) {	data = data.pop(); }
		if (isHTMLElement(data) && (data.tagName === "TD" || data.nodeName === "#comment")) { 
			return data;  
		}
		// if (typeof data === "function") {
		// 	return () => toTd(data())
		// }
		return el("td", data);
	}
} as ElFunction<HTMLTableRowElement>;


/** Creates a <td> element 
 * @importance 13
*/
export const td = function(...args: StandardArgs) {
	return _simpleFactory("td", args);
} as ElFunction<HTMLTableDataCellElement>;


/** Creates a <th> element 
 * @importance 13
*/
export const th = function(...args: StandardArgs) {
	return _simpleFactory("th", args);
} as ElFunction<HTMLTableHeaderCellElement>;



export interface IImageProps extends IElProps<HTMLImageElement> { src: string; }
/** Factory function img() can take special props
 * @importance 14
 * @prop.src Is the src attribute for an <img src=""> element*/
export const img = function(...args: StandardArgs<IImageProps>) {
	const {props, innards} = standardizeInput<IImageProps>(...args);
	props.attr = props.attr || {};
	props.attr.src = props.src;
	return el("img", props, innards);
} as ElFunction<HTMLImageElement, IImageProps>


export interface IButtonProps extends IElProps<HTMLButtonElement> { onPush: () => any; }
/** Factory function button() can take special props
 * @importance 14
 * @prop.onPush Is a function which will get called onTap or onClick*/
export const button = function(...args: StandardArgs<IButtonProps>) {
	return _simpleFactory("button", args);
} as ElFunction<HTMLButtonElement, IButtonProps>





/** Factory function select() can take special props
 * @importance 13
 * @prop.onValueChange Is a function which will get called whenever the value of the input changes*/
export interface ISelectElProps extends IElProps<HTMLSelectElement> { 
	onInput?: (value: string) => any; 
}

type Printable = string | number;
export type ISelectElInnards = Array<HTMLOptionElement | Printable | (() => Printable)>; // | Source<Printable>;

export const select = function(
	classNameOrProps?: string | ISelectElProps | ISelectElInnards,
	propsOrInnards?: ISelectElProps | ISelectElInnards,
	maybeInnards?: ISelectElInnards,
): HTMLSelectElement {
	const {props, innards} = standardizeInput<ISelectElProps, ISelectElInnards>(
		classNameOrProps, propsOrInnards, maybeInnards,
		(a): a is ISelectElInnards => Array.isArray(a),
	);
	props.on = props.on || {};

	let currentVal: string;
	if (props.onInput) {
		const { onInput } = props;
		const onInputListener = props.on.input;
		props.on.input = (ev: Event & { currentTarget: HTMLSelectElement}) => {
			const newVal = out.value;
			if (newVal !== currentVal) {
				currentVal = newVal;
				onInput(newVal);
			}
			return !onInputListener || onInputListener(ev);
		}
	}

	const out = el("select", props, innards.map((innard => {
		let option: HTMLOptionElement;
		if (innard instanceof HTMLElement) {
			option = innard;
		} else if (typeof innard === "function") {
			option = el("option", { ddx: (ref: HTMLOptionElement) => {
				ref.innerText = String(innard());
				if (ref.selected) {
					// update value??
				}
			}});
		} else {
			option = el("option", String(innard));
		}
		if (option.selected) {
			currentVal = option.value;
		}
		return option;
	})));
	return out;
} as ElFunction<HTMLSelectElement, ISelectElProps, ISelectElInnards>



/** Factory function inputTextbox() can take special props
 * @importance 13
 * @prop.onValueChange Is a function which will get called whenever the value of the input changes*/
export interface IInputTextboxProps extends IElProps<HTMLInputElement> { 
	type?: "number" | "text";
	onInput?: (value: string) => any; 
	onValueEntered?: (value: string) => any; 
}

export type InputTextboxInnards = string | Source<string> | (() => string);

export const inputTextbox = function (
	classNameOrProps?: string | IInputTextboxProps | InputTextboxInnards,
	propsOrInnards?: IInputTextboxProps | InputTextboxInnards,
	maybeInnards?: InputTextboxInnards,
) {
	const {props, innards} = standardizeInput<IInputTextboxProps, InputTextboxInnards>(
		classNameOrProps, propsOrInnards, maybeInnards,
		(a): a is InputTextboxInnards => a instanceof Source
	);
	props.on = props.on || {};

	if (innards instanceof Source) {
		const onInputArg = props.onInput;
		props.onInput = (value) => {
			innards.set(value)
			onInputArg && onInputArg(value);
		}
	}

	if (props.onInput) {
		const { onInput } = props;
		const onInputListener = props.on.input;
		let oldVal = innards;
		props.on.input = (ev: Event & { currentTarget: HTMLInputElement}) => {
			const newVal = out.value;
			if (newVal !== oldVal) {
				oldVal = newVal;
				onInput(newVal);
			}
			return !onInputListener || onInputListener(ev);
		}
	}
	if (props.onValueEntered) {
		const onChange = props.on.change;
		const { onValueEntered } = props;
		props.on.change = (ev: Event & { currentTarget: HTMLInputElement}) => {
			onValueEntered((ev.target as HTMLInputElement).value);
			return !onChange || onChange(ev);
		};
	}
	const out = el("input", props);
	out.type = props.type || "text";

	if (innards) {
		if (typeof innards === "string") {
			out.value = innards;

		} else if (innards instanceof Source) {
			derive(() => out.value = innards.get());

		} else {
			derive(() => out.value = innards());
		}
	}
	return out;
} as ElFunction<HTMLInputElement, IInputTextboxProps, InputTextboxInnards>



/** Creates a <i> element 
 * @importance 14
*/
export const italic = function(...args: StandardArgs) {
	return _simpleFactory("i", args);
} as ElFunction<HTMLElement>


/** Creates a <b> element 
 * @importance 14
*/
export const bold = function(...args: StandardArgs) {
	return _simpleFactory("b", args);
} as ElFunction<HTMLElement>


/** Create an unmodified hr element 
 * @importance 13
*/
export const hr = function(...args: StandardArgs) {
	return _simpleFactory("hr", args);
} as ElFunctionTargetInnards<HTMLHRElement, never>

/** Create an unmodified br element 
 * @importance 14
*/
export function br(size?: number, props: IElProps<HTMLBRElement> = {}): HTMLBRElement { 
	if (!size) {
		return el("br", props); 
	}
	props.style = {
		display: "block", 
		height: `${size}em`
	}
	return el("em-br" as any, props);
}


/** Creates a <h1> element 
 * @importance 13
*/
export const h1 = function(...args: StandardArgs) {
	return _simpleFactory("h1", args);
} as ElFunction<HTMLHeadingElement>

/** Creates a <h2> element 
 * @importance 13
*/
export const h2 = function(...args: StandardArgs) {
	return _simpleFactory("h2", args);
} as ElFunction<HTMLHeadingElement>

/** Creates a <h3> element 
 * @importance 13
*/
export const h3 = function(...args: StandardArgs) {
	return _simpleFactory("h3", args);
} as ElFunction<HTMLHeadingElement>

/** Creates a <h4> element 
 * @importance 13
*/
export const h4 = function(...args: StandardArgs) {
	return _simpleFactory("h4", args);
} as ElFunction<HTMLHeadingElement>

/** Creates a <h5> element 
 * @importance 13
*/
export const h5 = function(...args: StandardArgs) {
	return _simpleFactory("h5", args);
} as ElFunction<HTMLHeadingElement>

/** Creates a <h6> element 
 * @importance 13
*/
export const h6 = function(...args: StandardArgs) {
	return _simpleFactory("h6", args);
} as ElFunction<HTMLHeadingElement>






