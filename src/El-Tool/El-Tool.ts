import { RerenderReplacer, anchorDeriverToDomNode } from '../Derivations';
import { isHeliumHazed } from '../HeliumBase';
import { ElementAugment } from '../Augments/Augment';

/**************************
*. BASIC HELPER FUNCTIONS
***************************/

/** Short for document.getElementById() 
 * @importance 14
 * @returnVar elementOfId */
export function byId(id: string): HTMLElement { return document.getElementById(id); }

export function on(eventName: string, ref: EventTarget, cb: (ev: Event) => any) {
	ref.addEventListener(eventName, cb);
}


/** Short for document.addEventListener("DOMContentLoaded"). 
 * It is recommended you start rendering before the dom is ready, then use this
 * event listener to add your content to the page.
 * @importance 20
 * @eg const appUi = div("App", "I'm the app");\
 * onDomReady(() => document.body.appendChild(appUi)));
*/
export function onDomReady(cb: () => void) {
	if (domIsReady) { cb(); return; }
	window.addEventListener("DOMContentLoaded", cb);
}
let domIsReady = false;
(typeof window !== "undefined") && onDomReady(() => domIsReady = true);


/** Acts similar to array.splice, but for an Element.classList. Can add/remove multiple
 * class names at once with either an array or string with spaces (eg. "big red dog")
 * @importance 10
 * @returnVar sameElement
 * @param.removeClasses classes which will be removed
 * @param.addClasses classes which element will have after function runs */
export function classSplice<ROOT extends Element = Element>(
	element: ROOT,
	removeClasses: string | string[] | null,
	addClasses?: string | string[],
): ROOT {
	if (removeClasses) {
		const remList = Array.isArray(removeClasses) ? removeClasses : removeClasses.split(/\s+/g);
		remList.filter((it) => !!it).forEach((className) => element.classList.remove(className));
	}
	if (addClasses) {
		const addList = Array.isArray(addClasses) ? addClasses : addClasses.split(/\s+/g);
		addList.filter((it) => !!it).forEach((className) => element.classList.add(className));
	}
	return element;
}


/** Short for classSplice(element, null, addClasses).  Can be used to 
 * add a class to something without affecting its render code.
 * @importance 15
 * @returnVar sameElement
 * @eg const sectionUi = renderSection();\
 * div("Eg", addClass("sectionName", sectionUi)) */
export function addClass(addClasses: string | string[], element: Element): Element {
	return classSplice(element, null, addClasses);
}


/** Boolean switcher for adding and removing classes to Element.classList. 
 * @importance 15
 * @eg haveClass(div, "selected", div === selectedItem)
 * @param.className class or classes to add/remove.  Can be array, or space separated string for groups.
 * @param.addNotRemove whether the element should or should not have these classes. 
 * @returnVar sameElement*/
export function haveClass<ROOT extends Element = Element>(
	element: ROOT, 
	className: string | string[], 
	addNotRemove: boolean
): ROOT {
  return addNotRemove ? (
    classSplice(element, null, className)
  ) : (
    classSplice(element, className, undefined)
  );
}



export function ddxClass(target: HTMLElement, ddx: () => string | string[]) {
	let lastClassList: string | string[];
	anchorDeriverToDomNode(target, () => {
		const removeUs = lastClassList;
		classSplice(target, removeUs, lastClassList = ddx());
	}, "props.ddxClass").run();
}


/** Shorthand way to remove all children from an element 
 * @importance 6
*/
export function removeChildren<ROOT extends BaseNode = BaseNode>(parent: ROOT): ROOT {
	let child = parent.firstChild;
	while(!!child) {
		child.remove();
		child = parent.firstChild;
	}
	return parent;
}



/** Shorthand way to set styles on element
 * @importance 11
 * @param.style Can be string to overwrite all inline styles (<a style="...">)
 * or an object using camel case to only overwrite specific properties.
 * @eg setStyle(body, "color: #111; background-color: orange;");\
 * setStyle(body, {\
 *   color: "#111",\
 *   backgroundColor: "orange",\
 * }); */
export function setStyle<ROOT extends HTMLElement = HTMLElement>(
	root: ROOT, 
	style: Partial<CSSStyleDeclaration> | string
): ROOT {
	if (typeof style === "string") {
		root.style.cssText = style;
	} else {
		Object.keys(style).forEach((styleName) => root.style[styleName] = style[styleName]);
	}
	return root;
}

export interface IToucherPosition {
	top: number;
	left: number;
}
export type ToucherMoveEvent = (MouseEvent | TouchEvent) & { positions: Array<IToucherPosition> };
export function onToucherMove(
	root: Element, 
	cb: (ev: ToucherMoveEvent) => EventResponse
) {
	const getPosFromClientXY = (x: number, y: number) => {
		const clientRect = root.getBoundingClientRect();
		return {
			left: x - clientRect.left,
			top: y - clientRect.top,
		};	
	}
	root.addEventListener("mousemove", (ev: MouseEvent) => {
		const toucherMoveEvent = ev as ToucherMoveEvent;
		toucherMoveEvent.positions = [
			getPosFromClientXY(ev.clientX, ev.clientY)
		];
		cb(toucherMoveEvent);
	});
	root.addEventListener("touchmove", (ev: TouchEvent) => {
		const toucherMoveEvent = ev as ToucherMoveEvent;
		const clientRect = root.getBoundingClientRect();
		toucherMoveEvent.positions = Array.from(ev.touches).map((touch) => 
			getPosFromClientXY(touch.clientX, touch.clientY)
		);
		cb(toucherMoveEvent);
		ev.preventDefault();
	}) 
}

export type ToucherEnterExitEvent = (MouseEvent | TouchEvent) & { isEnterEvent: boolean };
export function onToucherEnterExit(
	root: Element, 
	cb: (ev: ToucherEnterExitEvent) => EventResponse
) {
	const modEvent = (ev: any, isEnterEvent: boolean) => {
		ev.isEnterEvent = isEnterEvent;
		return ev as ToucherEnterExitEvent;
	}
	root.addEventListener("mouseenter", (ev) => cb(modEvent(ev, true)));
	root.addEventListener("mouseleave", (ev) => cb(modEvent(ev, false)));
	root.addEventListener("touchstart", (ev) => {
		cb(modEvent(ev, true));
		ev.preventDefault();
	});
	root.addEventListener("touchend", (ev) => {
		cb(modEvent(ev, false));
		ev.preventDefault();
	});
}


export function onTouch(
	root: Element, 
	cb: (ev: MouseEvent) => EventResponse
) {
	root.addEventListener("mousedown", (event: MouseEvent) => {
		if(event.buttons == 1 || event.buttons == 3) {
			cb(event);
		}
	});

	root.addEventListener("touchstart", (event) => cb(event as any))
}



/**************************
*. TYPES / INTERFACES
***************************/

export type RawHTML = {hackableHTML: string};
export type BaseNode = ChildNode;


export type StaticInnard = string | BaseNode | RawHTML | number | null | false | void | undefined | (Array<StaticInnard> | (() => StaticInnard));
export type SingleInnard = StaticInnard ;
type NonStringStaticInnard = Exclude<StaticInnard, string>;

export type ManyStaticInnards = SingleInnard | Array<SingleInnard>;
export type Innards = ManyStaticInnards | (() => ManyStaticInnards);
export type NonPrimativeInnards = BaseNode | Array<SingleInnard> | (() => ManyStaticInnards);

export type AugmentCb<
	CB_ARGS extends Array<any> = [], 
	CB_RETURN = void
> = (ref: Element, ...args: CB_ARGS) => CB_RETURN;

type EventResponse = boolean | void | any;

/**************************
*. TYPES CHECKS
***************************/

/** Check if thing is DOM appendable node 
 * @returnVar checkMeIsNode */
export function isNode(checkMe: any): checkMe is BaseNode {
	return checkMe instanceof Node;
}


export function isHTMLElement(checkMe: any): checkMe is HTMLElement {
	return "nodeName" in checkMe;
}

export function isTextNode(checkMe: ChildNode): checkMe is Text {
	return isNode(checkMe) && checkMe.nodeType === checkMe.TEXT_NODE;
}

/** Check if thing reprensents raw html.  WARNING: Always double check raw html for injection. 
 * @returnVar checkMeIsRawHTML */
export function isRawHTML(checkMe: any): checkMe is RawHTML {
	return (checkMe && typeof checkMe === "object") && checkMe.hasOwnProperty("hackableHTML");
}

/** Check if thing represents rendering props for el-tool 
 * @returnVar checkMeIsProps */
export function isProps(checkMe: any): checkMe is IElProps {
	return (checkMe && typeof checkMe === "object") 
		&& !Array.isArray(checkMe) 
		&& !isNode(checkMe) 
		&& !isRawHTML(checkMe);
}



/*************************
*. MAIN CREATION FN
*************************/

/** Main function at the root of el-tool (and Helium).  First standardizes output,
 * into rendering properties and innards-to-append.  Next it creates an HTMLElement
 * and begins modifying it based off the props.  Then it resolves innards to
 * an Element list and appends them as children.  And finally, it returns the HTMLElement
 * it has created, modified, and added children to.
 * @importance 16
 * @param.tagName The name of the element to create (eg. "div" -> <div>).
 * @param.propsOrInnards Either the props by which to modify the element, or innards to append.
 * @param.innards Either the innards to append, or empty.*/
export function el<K extends keyof HTMLElementTagNameMap>(
	tagName: K,
	propsOrInnards?: IElProps | Innards,
	innards?: Innards,
): HTMLElementTagNameMap[K];
export function el(
	tagName: string,
	propsOrInnards?: IElProps | Innards,
	innards?: Innards,
): HTMLElement {
	const input = standardizeInput(null, propsOrInnards, innards);
	return __createEl(tagName as any, input.props, input.innards);
}


export type NameOrPropsOrInnards<PROPTYPE extends IElProps> = string | PROPTYPE | (NonStringStaticInnard) | Array<SingleInnard>;
export type PropsOrInnards<PROPTYPE extends IElProps> = PROPTYPE | Innards;

/** Takes standard three arguments and returns a object with unambiguos props and innards.
 * Helper function for el() and factories such as div, span, etc. 
 * @importance 16
 * @returnVar {props, innards}*/
export function standardizeInput<
	PROPTYPE extends IElProps = IElProps,
	INNARDS = Innards,
>(
	nameOrPropsOrInnards?: string | PROPTYPE | INNARDS,
	propsOrInnards?:  PROPTYPE | INNARDS,
	maybeInnards?: INNARDS,
	isInnardsRazor?: (checkMe: PROPTYPE | INNARDS) => checkMe is INNARDS,
): {
	props: PROPTYPE,
	innards: INNARDS,
} {
	let innards: INNARDS = maybeInnards;
	let props: PROPTYPE;
	let name: string;

	if (nameOrPropsOrInnards) {
		if (typeof nameOrPropsOrInnards === "string") {
			name = nameOrPropsOrInnards;
		} else {
			if (maybeInnards !== undefined) {
				throw new Error("Can not define first and third argument if first is not a string"); 
			}
			maybeInnards = propsOrInnards as any;
			propsOrInnards = nameOrPropsOrInnards;
		}
	}

	if (propsOrInnards) {
		const isInnards = isInnardsRazor ? isInnardsRazor(propsOrInnards) : !isProps(propsOrInnards);
		if (isInnards) {
			if (maybeInnards !== undefined) {
				throw new Error("Can not define third argument if second is innards"); 
			}
			innards = propsOrInnards as any;
		} else {
			props = propsOrInnards as any;
			innards = maybeInnards;
		}
	}

	props = props || {} as any;
	if (name) {
		props.class = props.class || "";
		props.class += (props.class ? " " : "") + name;
	}

	return { props, innards };
}


type EventHandler<TARGET extends EventTarget, EVENT_TYPES extends Event> = 
	(event: EVENT_TYPES & { currentTarget: TARGET }) => EventResponse;


export interface IElProps<NODE extends BaseNode = HTMLElement> {
	id?: string;
	class?: string;
	title?: string;
	attr?: {[key: string]: string};
	style?: Partial<CSSStyleDeclaration> | string;
	on?: {[key: string]: EventHandler<NODE, Event> };
	onPush?: EventHandler<NODE, MouseEvent | TouchEvent>;
	onTouch?: EventHandler<NODE, MouseEvent>;
	onToucherMove?: EventHandler<NODE, ToucherMoveEvent>;
	onToucherEnterExit?: EventHandler<NODE, ToucherEnterExitEvent>;
	// onClick?: EventHandler<NODE, MouseEvent>;
	// onMouseDown?: EventHandler<NODE, MouseEvent>;
	onKeyDown?: EventHandler<NODE, KeyboardEvent>;
	onHoverChange?: (isHovered: boolean, event?: MouseEvent) => EventResponse;
	this?: {
		[key: string]: any;
	}
	innards?: Innards;
	ref?: (ref: NODE) => any;
	ddxClass?: () => string | string[];
	ddx?: (ref: NODE, keep?: { [key: string]: any}) => any;
	augments?: Array<ElementAugment> | ElementAugment;
}

export function applyProps(
	props: IElProps,
	out: HTMLElement, 
	innardsCb?: (ref: HTMLElement) => any
) {
	if (props) {
		// event stuff
		if (props.on) {
			const eventListeners = props.on || {};
			Object.keys(eventListeners).forEach((eventType) =>
				out.addEventListener(eventType, eventListeners[eventType])
			);
		}
		// if (props.onClick) { out.addEventListener("click", props.onClick); }
		if (props.onKeyDown) { out.addEventListener("keydown", props.onKeyDown); }
		// if (props.onMouseDown) { out.addEventListener("mousedown", props.onMouseDown); }
		if (props.onPush) { 
			out.classList.add("onPush");
			out.addEventListener("click", props.onPush); 
		}
		if (props.onTouch) { 
			onTouch(out, props.onTouch);
		}
		if (props.onToucherMove) { onToucherMove(out, props.onToucherMove); }
		if (props.onToucherEnterExit) { onToucherEnterExit(out, props.onToucherEnterExit); }
		if (props.onHoverChange) {
			out.addEventListener("mouseover", (ev: MouseEvent) => props.onHoverChange(true, ev));
			out.addEventListener("mouseout", (ev: MouseEvent) => props.onHoverChange(false, ev));
		}

		// element html props, and element js props
		if (props.title) { out.title = props.title; }
		if (props.attr) {
			const attrs = props.attr || {};
			Object.keys(attrs).forEach((attr) => out.setAttribute(attr, attrs[attr]));
		}
		if (props.this) {
			const thisProps = props.this || {};
			Object.keys(thisProps).forEach((prop) => out[prop] = thisProps[prop]);
		}

		// classing
		if (props.class) { 
			addClass(props.class, out);
		}
		if (props.id) { out.id = props.id; }


		// inline styles
		if (props.style) { setStyle(out, props.style); }

		// innards alternative
		if (props.innards !== undefined) { 
			append(out, props.innards); 
		}
	}

	// add all chidren, or innards
	if (innardsCb !== undefined) { innardsCb(out); }

	if (props) {
		if (props.ref) { props.ref(out); }
		if (props.ddxClass) { ddxClass(out, props.ddxClass); }
		if (props.ddx) { 
			const keep = {};
			anchorDeriverToDomNode(out, () => props.ddx(out, keep), "props.ddx").run();
		}
		if (props.augments) {
			let augs = Array.isArray(props.augments) ? props.augments : [props.augments];
			augs.forEach((aug) => aug.targetElement(out));
			out.addEventListener("helium-freeze", () => augs.forEach((aug) => aug.freeze()));
			out.addEventListener("helium-unfreeze", () => augs.forEach((aug) => aug.unfreeze()));
		}
	}
	return out;
}




function __createEl<K extends keyof HTMLElementTagNameMap>(
	tagName: K,
	props?: IElProps,
	innards?: Innards,
): HTMLElementTagNameMap[K] {
	const out = document.createElement(tagName);

	applyProps(props, out, () => {
		if (innards !== undefined) { append(out, innards); }
	});

	if (isHeliumHazed(out)) {
		out._helium.frozen = true;
	}
	
	return out;
}


let innardsWarningGiven
/** Removes all children, then appends innards
 * @related El-Tool-append 
 * @importance 15
 * @eg setInnards(document.body, [\
 *   div("Section", "This is the first section"),\
 *   div("Section", "This is the second section"),\
 * ]);
*/
export function setInnards(root: BaseNode, innards: Innards): void {
	if (root === document.body && !(window as any)._heliumNoWarnSetInnardsDocBody) {
		console.warn([
			`Suggestion: avoid using setInnards() on document body, and instead use append().`,
			`If you do not, anything added to the page outside of your root render is unsafe.  To disable this warning,`,
			`set window._heliumNoWarnSetInnardsDocBody = true`,
		].join(" "))
	}
	removeChildren(root);
	append(root, innards);
}



/** Appends innards to root element.
 * @importance 14
 * @param.before Optional element to insert before.*/
export function append<ROOT extends BaseNode>(
	root: ROOT,
	innards: Innards,
	before?: ChildNode,
): ROOT {
	const addUs = convertInnardsToElements(innards);
	if (before) {
		addUs.forEach((node) => {
			try {
				root.insertBefore(node, before);
			} catch(err) {
				console.error("Can not insert childNode into: ", root);
				console.error("Tried to insert: ", node);
				console.error(err);
			}
		});
	} else {
		addUs.forEach((node) => {
			try {
				root.appendChild(node);
			} catch(err) {
				console.error("Can not append childNode to: ", root);
				console.error("Tried to append: ", node);
				console.error(err);
			}
		});
	}
	return root;
}

/** Takes innards and converts them to DOM elements.  If the typeof innards is a function
 * that function will be managed by a RerenderReplacer 
 * @returnVar childNodes */
export function convertInnardsToElements(innards: Innards): ChildNode[] {
	const renders = convertInnardsToRenders(innards);
	const out: ChildNode[] = [];
	renders.forEach((render) => {
		if (Array.isArray(render)) {
			render.forEach((node) => out.push(node));
		} else {
			out.push(render);
		}
	})
	return out;
}



export function convertInnardsToRenders(innards: Innards): Array<ChildNode | ChildNode[]> {
	if (innards === undefined || innards === null) { return []; }
	if (typeof innards === "function") {
		const refresher = new RerenderReplacer(innards);
		return refresher.getRender(); 
	}
	let childList = Array.isArray(innards) ? innards : [innards];
	return childList.flat().map((child) => convertSingleInnardToNodes(child));
}


/** Converts a single item from an innards array into DOM elements 
 * @returnVar childNodes */
export function convertSingleInnardToNodes(child: SingleInnard) {
	if (typeof child === "function") {
		const replacer = new RerenderReplacer(child);
		return replacer.getRender();
		
	} else {
		// return _convertSingleStaticInnardToNodes(child);
		let out: BaseNode[] = [];
		if (child === undefined || child === null) { return []; }
		if (typeof child === "number") { child = child + ""; }
		if (isRawHTML(child)) {
			const template = document.createElement('div');
			template.innerHTML = child.hackableHTML;
			template.getRootNode();
			Array.from(template.childNodes).forEach((child) => out.push(child));
		} else if (typeof child === "string") {
			out.push(new Text(child))
		} else if (child) {
			if (Array.isArray(child)) {
				out = out.concat(convertSingleInnardToNodes(child));
			} else {
				out.push(child); 
			}
		}
		return out;
	}
}

