import { IElProps, standardizeInput, el, setStyle, on, onDomReady, byId } from "./El-Tool";
import { StandardArgs } from "./Factories";
import { anchorDeriverToDomNode } from "../Derivations";
import { noDeps, derive } from "helium-sdx";
import { Sourcify } from "../Sourcify";



//-------- DOM NODES ------------

/** Factory function a() can take special props
 * @importance 14
 * @prop.href Is the href attribute for an <a href=""> element*/
export interface IAnchorProps extends IElProps { 
  href?: string | (() => string);
  target?: "_blank";
  disableHref?: boolean;
	allowTextSelect?: boolean;
}
export function anchor(...[classOrProp, propOrChild, child]: StandardArgs<IAnchorProps>) {
	const {props, innards} = standardizeInput<IAnchorProps>(classOrProp, propOrChild, child);
  props.attr = props.attr || {};
  const { href } = props;
  if (!href) {
    props.attr.href = "#";
  } else if (typeof href === "string") {
    props.attr.href = href;
  } 

	if (props.target) { props.attr.target = props.target; }
  const ref = el("a", props, innards);
  if (typeof href === "function") {
    anchorDeriverToDomNode(ref, () => {
      ref.href = href();
    })
  }

	if (props.allowTextSelect) {
		setStyle(ref, {"-webkit-user-drag": "none"} as any);
		let selectedText = "";
		on("mousedown", ref, (ev) => selectedText = window.getSelection().toString());
		on("click", ref, (ev) => {
			if (selectedText !== window.getSelection().toString()) {
				ev.preventDefault();
			}
		});
  }
  on("click", ref, (ev: MouseEvent) => {
    if (ref.href) {
      if (props.disableHref) {
        return ev.preventDefault();
      }

      if (ev.metaKey || ref.target === "_blank") {
        window.open(ref.href, '_blank')
        return ev.preventDefault();
        
      } else if (UrlState.update({ urlPart: ref.href, mode: "push", deferLocationChange: true}) !== "DEFERRED") {
        return ev.preventDefault();
      }
    }
  })
	return ref;
}


export function a(...args: StandardArgs<IAnchorProps>) {
  console.warn(`helium function "a" has been deprecated.  Please use "anchor" instead.`)
  return anchor(...args);
}


export function externalLink(...[classOrProp, propOrChild, child]: StandardArgs<IAnchorProps>) {
	const {props, innards} = standardizeInput<IAnchorProps>(classOrProp, propOrChild, child);
	props.target = "_blank";
	return anchor(props, innards);
}





//-------- NAVIGATION EVENT ------------

/** locationchange is triggered for every location change and 
 * followurl is triggered when a url change is attempted (can be cancelled)
 */
export type NavigationEventName = "locationchange" | "followurl" ;

export class NavigationEvent extends Event {
  static createFullUrl(urlPart: string) {
    try {
      const out = new URL(urlPart);
      if (!out.host) { throw ""; }
      return out;
    } catch(err) {
      return new URL(urlPart, location.origin);
    }
  }

  public newUrl: URL;
  public pageRefreshCancelled: boolean = false;
  public hashFocusCancelled: boolean = false;

  constructor(name: NavigationEventName, props: EventInit, newUrl?: string) {
    super(name, props);
    this.newUrl = NavigationEvent.createFullUrl(newUrl);
  }

  // Safari doesn't like class extension
  public preventPageRefresh = () => {
    this.pageRefreshCancelled = true;
  }
  public preventHashFocus = () => {
    this.hashFocusCancelled = true;
  }
}




//-------- URL STATE ------------

export interface IUrlUpdateArgs {
  urlPart: string;
  preventHashFocus?: boolean;
  mode?: "push" | "replace";
  deferLocationChange?: boolean,
}


export class _UrlState {
  protected state = Sourcify({
    url: new URL(location.href),
    urlParams: {} as Record<string, string>,
    hash: "",
  })

  constructor() {
    this.initListeners();
  }

  public getUrl() { return this.state.url; }
  public get href() { return this.getUrl().href; }
  public get host() { return this.getUrl().host; }
  public get hostname() { return this.getUrl().hostname; }
  public get pathname() { return this.getUrl().pathname; }
  public get hash() { return this.state.hash; }
  public get urlParams() { return this.state.urlParams; }

  // Called by clicking link, first gets the event returned by dispatching a url update
  // If the event is captured and handled (pageRefreshCancelled) the url is pushed to history
  public followUrl(newUrl: string): "CAPTURED" | "BLOCKED" | "DEFERRED" {
    return this.update({ urlPart: newUrl, mode: "push" });
  }

  public update(argsOrUrl: string | IUrlUpdateArgs) {
    const args: IUrlUpdateArgs = typeof argsOrUrl === "object" ? argsOrUrl : {
      urlPart: argsOrUrl
    };
    args.mode = args.mode || "push";

    const followResponse = this.newNavEvent("followurl", args.urlPart, true);
    if (followResponse && args.preventHashFocus) { followResponse.preventHashFocus(); }

    let evResponse: NavigationEvent;
    if (followResponse) {
      const changeEv = this.newNavEvent("locationchange", args.urlPart);
      if (followResponse.hashFocusCancelled) { changeEv.preventHashFocus(); }
      evResponse = window.dispatchEvent(changeEv) && changeEv;
    }

    if (!evResponse) { return "BLOCKED"; }
    if (evResponse.pageRefreshCancelled) {  
      if (evResponse.newUrl.href !== location.href) { 
        // can not cancel refresh with host change
        if (location.host !== evResponse.newUrl.host) {
          window.location.href = evResponse.newUrl.href as any;
          return;

        } else {
          if (args.mode === "push") {
            history.pushState(null, "", args.urlPart);
          } else { //if (args.mode === "replace") {
            history.replaceState(null, "", args.urlPart);
          }
        }
      }
      return "CAPTURED";
    }
    if (args.deferLocationChange) {
      return "DEFERRED";
  
    } else {
      window.location.href = NavigationEvent.createFullUrl(args.urlPart).href as any;
    }
  }

  public replaceUrl(newUrl: string) {
    return this.update({ urlPart: newUrl, mode: "replace" });
  }

  public onUrlChange(cb: (ev: NavigationEvent) => any) {
    window.addEventListener("locationchange", cb);
  }
  
  public observeUrl(cb: (ev: NavigationEvent) => any) {
    this.onUrlChange(cb);
    const ev = this.newNavEvent(name, location.href);
    cb(ev);
  }

  // public _updateUrl(args: {
  //   urlPart: string, 
  //   deferLocationChange?: boolean,
  //   mode: "push" | "replace"
  // }): "CAPTURED" | "BLOCKED" | "DEFERRED" {
  //   const evResponse = dispatchUrlUpdate("followurl", args.urlPart);
  //   if (!evResponse) { return "BLOCKED"; }
  //   if (evResponse.pageRefreshCancelled) {  
  //     if (evResponse.newUrl.href !== location.href) { 
  //       // can not cancel refresh with host change
  //       if (location.host !== evResponse.newUrl.host) {
  //         window.location.href = evResponse.newUrl.href as any;
  //         return;

  //       } else {
  //         if (args.mode === "push") {
  //           history.pushState(null, "", args.urlPart);
  //         } else if (args.mode === "replace") {
  //           history.replaceState(null, "", args.urlPart);
  //         }
  //         this.state.set("url", new URL(args.urlPart));
  //       }
  //     }
  //     return "CAPTURED";
  //   }
  //   if (args.deferLocationChange) {
  //     return "DEFERRED";
  
  //   } else {
  //     window.location.href = NavigationEvent.createFullUrl(args.urlPart).href as any;
  //   }
  // }

  protected newNavEvent(name: NavigationEventName, newUrl: string, dispatch: boolean = false) {
    const ev = new NavigationEvent(name, {cancelable: true}, newUrl);
    if (dispatch) {
      window.dispatchEvent(ev)
    }
    return ev;
  }

  protected initListeners() {
    window.addEventListener('popstate', () => {
      this.newNavEvent("locationchange", location.href, true);
    });

    // Some strange circular reference exists somewhere
    setTimeout(() => {
      onDomReady(() => {
        setTimeout(() => {
          const hash = location.hash;
          const target = byId(hash.slice(1));
          if (!!target) { target.scrollIntoView({ behavior: "auto" }); }
        }, 200);
      })
    }, 0);

    let lastHash: string;
    this.onUrlChange((ev) => {
      if (ev.hashFocusCancelled !== true
        && ev.newUrl.hash !== lastHash
      ) {
        lastHash = ev.newUrl.hash;
        requestAnimationFrame(() => requestAnimationFrame(() => {
          const target = byId(lastHash.slice(1));
          if (!!target) { target.scrollIntoView({ behavior: "auto" }); }
        }));
      }
    });

    this.observeUrl((ev) => {
      this.state.set("url", ev.newUrl);
    })

    derive({
      anchor: "NO_ANCHOR",
      fn: () => {
        const url = this.getUrl();
        const urlParams = new URLSearchParams(url.search);
        urlParams.forEach((value, key) => {
          this.state.urlParams[key] = value;
        })

        this.state.hash = url.hash;
      }
    })
  }
}

export const UrlState = new _UrlState();

export const onUrlChange = UrlState.onUrlChange.bind(UrlState) as typeof UrlState.onUrlChange;
export const observeUrl = UrlState.observeUrl.bind(UrlState) as typeof UrlState.observeUrl;
export const followUrl = UrlState.followUrl.bind(UrlState) as typeof UrlState.followUrl;
export const replaceUrl = UrlState.replaceUrl.bind(UrlState) as typeof UrlState.replaceUrl;







