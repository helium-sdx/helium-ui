
import { RkMapFn, AnyRMapFn } from './SourceBase';
import { RerenderReplacer } from '../Derivers/RerenderReplacer';

import { 
  SourceMap as _CoreSourceMap
} from "helium-sdx";



/**
 * @importance 7
 */
export class SourceMap<KEY, VALUE> extends _CoreSourceMap<KEY, VALUE> {
  
  public renderMap(renderFn: RkMapFn<VALUE, KEY>): ChildNode[] {
    return this._mapReplacers(false, renderFn);
  }

  public renderMapKeyed(renderFn: RkMapFn<VALUE, KEY>): ChildNode[] {
    return this._mapReplacers(true, renderFn);
  }

  private _mapReplacers(withKey: boolean, renderFn: AnyRMapFn<VALUE, KEY>): ChildNode[] {
    const entryReplacers = new Map<KEY, RerenderReplacer>();
    const orderer = new RerenderReplacer(() => {
      const out: ChildNode[] = [];
      const keys = this.keys();
      for (const key of keys) {
        if (entryReplacers.has(key) === false) {
          const source = this.getSource(key);
          entryReplacers.set(key, new RerenderReplacer(() => 
            renderFn(source.get(), withKey ? key : undefined)
          ));
        }
        const nodeList = entryReplacers.get(key).getRender();
        for (const node of nodeList) {
          out.push(node);
        }
      }
      return out;
    });
    return orderer.getRender();
  }
}

