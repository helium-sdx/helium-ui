import { Innards } from '../../El-Tool/El-Tool';
import { RerenderReplacer } from '../Derivers/RerenderReplacer';

import {
  Source,
  SourceArray as _CoreSourceArray
} from "helium-sdx"



/**
 * @importance 7
 */
export class SourceArray<TYPE> extends _CoreSourceArray<TYPE> {

  public renderMap(renderFn: (value: TYPE) => Innards) {
    return this._mapReplacers(false, renderFn);
  }

  public renderMapKeyed(renderFn: (value: TYPE, index: number) => Innards) {
    return this._mapReplacers(true, renderFn);
  }



  private _mapReplacers(
    withIndex: boolean, 
    renderFn: (value: TYPE, index?: number) => Innards
  ) {
    const obsToRerenderMap = new Map<Source<TYPE | Source<TYPE>>, RerenderReplacer>();
    const listReplacer = new RerenderReplacer(() => {
      this.orderMatters()
      const out: ChildNode[] = [];
      const length = this.length;
      for (let i = 0; i < length; i++) {
        const observable = withIndex ? this._getIndexObserver(i) : this.getSource(i);
        if (obsToRerenderMap.has(observable) === false) {
          obsToRerenderMap.set(observable, new RerenderReplacer(() => {
            const value = observable.get();
            if (withIndex) {
              return renderFn(this.noDeletedConstant((value as Source<TYPE>).get()), i);
            } else {
              return renderFn(this.noDeletedConstant(value as TYPE));
            }
          }));
        }
        const nodes = obsToRerenderMap.get(observable).getRender();
        nodes.forEach((node) => out.push(node));
      }
      return out;
    });
    return listReplacer.getRender();
  }
}