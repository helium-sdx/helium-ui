import { Sourcified } from "../../Sourcify/SourceProxyShallowTypes";
import { derive } from "helium-sdx";



export type Storable<TYPE extends object = {}> = Sourcified<TYPE>;// | Source<any> | SourceArray<any> | SourceMap<string | number, any>;

export function syncWithLocalStorage<TYPE extends object>(id: string, storeMe: Storable<TYPE>) {
  const init = localStorage.getItem(id);
  if (init) {
    try {
      const startingData = JSON.parse(init);
      storeMe.upsert(startingData);
    } catch(err) {
      console.warn("Could not parse locally stored data", err);
    }
  }
  derive({
    anchor: "NO_ANCHOR",
    fn: () => {
      const toStore = storeMe.toObject();
      try {
        localStorage.setItem(id, JSON.stringify(toStore));
      } catch(err) {
        console.warn("Could not store", err);
        console.log(toStore);
      }
    }
  });
}