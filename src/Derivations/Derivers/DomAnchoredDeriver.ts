import { changeFreezeState, heliumHaze, isHeliumHazed } from "../../HeliumBase";
import { DeriverScope, DeriveFn, aLog, AnchorNode, AnchorStatus } from "helium-sdx";




export interface IDomAnchorReport {
  scope?: DeriverScope,
} 

export type ReportRequest<OUTLINE> = {[key in keyof OUTLINE]?: true};

export type Reporter<OUTLINE> = 
  (request: ReportRequest<OUTLINE>) => Required<Pick<OUTLINE, keyof typeof request>>;


export function anchorDeriverToDomNode(
  node: ChildNode, 
  ddx: DeriveFn, 
  type?: string
): {
  run: () => void;
  report: Reporter<IDomAnchorReport>
} {
  let scope: DeriverScope;
  setupNodeAnchorMutationObserver();

  return {
    run: () => {
      const helium = heliumHaze(node)._helium;

      const anchor = new AnchorNode();
      anchor.details = [`(Right Click > Reveal in Elements Panel).  Issue occurred in render preceding: `, node];
      const deriveFn = () => {
        const onPage = node.getRootNode() === document;
        if (!onPage) {
          changeFreezeState(node as any, true);
        }
        ddx();
      };

      scope = new DeriverScope({
        fn: deriveFn,
        batching: "singleFrame",
        anchor,
      });

      (helium.unfreeze = helium.unfreeze || []).push(() => {
        anchor.setStatus(AnchorStatus.NORMAL);
      });

      (helium.freeze = helium.freeze || []).push(() => {
        anchor.setStatus(AnchorStatus.FROZEN);
      });
      scope.runDdx();
    },


    report: (req) => ({
      ...(req.scope) && {scope},
    })
  }
}




let mutationsBuffer: MutationRecord[] = [];
let bufferTimeout: any;
let mutationObserver: MutationObserver;
function setupNodeAnchorMutationObserver() {
  if (typeof MutationObserver === "undefined") { return; }
  if (mutationObserver) { return; }
  mutationObserver = new MutationObserver((mutationsList) => {
    mutationsList.forEach((mutation) => {
      if (mutation.type !== 'childList') { return; }
      mutationsBuffer.push(mutation);
    })
    if (bufferTimeout) { clearTimeout(bufferTimeout); }
    bufferTimeout = setTimeout(() => {
      bufferTimeout = undefined;
      const oldBuffer = mutationsBuffer;
      mutationsBuffer = [];
      const haveNode = new Map<Node, boolean>();
      
      for (const mutation of oldBuffer) {
        mutation.removedNodes.forEach((node) => haveNode.set(node, false));
        mutation.addedNodes.forEach((node) => haveNode.set(node, true));
      }
      const addedNodes: Node[] = [];
      const removedNodes: Node[] = [];
      Array.from(haveNode.entries()).forEach(([node, shouldHave]) => {
        if (isHeliumHazed(node) && node._helium.moveMutation) {
          return node._helium.moveMutation = false;
        }
        shouldHave ? addedNodes.push(node) : removedNodes.push(node);
      });
      aLog("treeMutations", "Adding:", addedNodes);
      aLog("treeMutations", "Removing:", removedNodes);

      addedNodes.forEach((node: Node) => {
        heliumHaze(node);
        permeate(node, (child) => {
          if (isHeliumHazed(child)) {
            changeFreezeState(child, false);
          } 
        })
      });

      removedNodes.forEach((node) => {
        heliumHaze(node);
        permeate(node, (child) => {
          if (isHeliumHazed(child)) {
            changeFreezeState(child, true);
          }
        })
      });
    }, 10);
  });

  var config = { childList: true, subtree: true };
  if (typeof window !== "undefined") {
    const tryAddObserver = () => mutationObserver.observe(document.body, config);
    if (document && document.body) { 
      tryAddObserver(); 
    } else {
      document.addEventListener("DOMContentLoaded", () => tryAddObserver());
    }
  }
  
  function permeate(root: Node, cb: (node: Node) => any) {
    cb(root);
    root.childNodes.forEach((child) => permeate(child, cb));
  }
}

