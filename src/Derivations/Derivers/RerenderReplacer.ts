import { Innards, convertInnardsToElements, el, BaseNode } from '../../El-Tool/El-Tool';

import { heliumHaze, isHeliumHazed } from '../../HeliumBase';
import { anchorDeriverToDomNode, IDomAnchorReport, Reporter } from './DomAnchoredDeriver';
import { DerivationManager, SourceBase } from 'helium-sdx';


const REMOVE_NODE = false;
const ADD_NODE = true;

/** A form of deriver management.  Beyond hosting the deriver function, this class 
 * also handles freezing and unfreezing derivation when its placeholder is added
 * or removed from the page.  Further, it makes sure to only add, remove, or swap
 * the elements in each rerender if they require it. 
 * @param.rerenderFn - A function which returns whatever the most up to date elements
 * for it should be */
export class RerenderReplacer {
  private static nextId = 0;

  private currentRender: ChildNode[];
  private id = "ddx_" + RerenderReplacer.nextId++;
  private placeHolder: Comment;
  protected getAnchorReport: Reporter<IDomAnchorReport>

  constructor(protected rerenderFn: () => Innards) {
    this.preConstructHook();
    if (DerivationManager.disabled()) {
      this.currentRender = convertInnardsToElements(this.rerenderFn());
      
    } else {
      this.placeHolder = document.createComment(this.id);
      const helium = heliumHaze(this.placeHolder)._helium;
      helium.rerenderReplacer = this;
      helium.isReplacer = true;
      const domAnchorKit = anchorDeriverToDomNode(
        this.placeHolder, 
        this.derive.bind(this), 
        "RerenderReplacer"
      );
      this.getAnchorReport = domAnchorKit.report;
      domAnchorKit.run();
    }
  }

  protected preConstructHook() {}

  /** Returns the current render. This render is live and will update automatically. */
  public getRender(): BaseNode[] { return this.currentRender; }

  /** Typically called by DerivationManager, if not frozen, this function 
   * will update its current render both on the page and locally, then return it. */
  public derive(): BaseNode[] {

    const placehold = this.placeHolder;
    
    const newRender = convertInnardsToElements(this.rerenderFn());
    newRender.push(this.placeHolder);

    const oldRender = this.currentRender;
    this.currentRender = newRender;

    const parent = placehold.parentElement;
    if (!parent || !oldRender) {
      return;
    }

    // console.log("replacing");

    const addRemoveMap = new Map<Node, boolean>();
    oldRender.forEach((node) => addRemoveMap.set(node, REMOVE_NODE));
    newRender.forEach((node) => {
      if (addRemoveMap.has(node)) {
        addRemoveMap.delete(node); // neither added nor removed
      } else {
        addRemoveMap.set(node, ADD_NODE);
      }
    });
    Array.from(addRemoveMap.entries()).forEach(([node, addRemove]) => {
      if (addRemove === REMOVE_NODE && node.parentElement) {
        node.parentElement.removeChild(node);
        if (isHeliumHazed(node) && node._helium.isReplacer) {
          (node._helium.rerenderReplacer as RerenderReplacer).removeAll();
        }
      }
    });

    const currentNodes = parent.childNodes;
    let parentIndex = Array.from(currentNodes).indexOf(placehold);
    // console.log("parent index", parentIndex, parent);
    let lastNode: ChildNode;
    const length = newRender.length;
    for (let i = 0; i < length; i++) {
      const addMe = newRender[length - 1 - i];
      const current = currentNodes[parentIndex - i];
      if (addMe === current) {
        // console.log("Same items", addMe, current);
        // do nothing
      } else {
        parent.insertBefore(addMe, lastNode);
        if (addRemoveMap.get(addMe) !== ADD_NODE) {
          // console.log("Non-new item", addMe, current);
          heliumHaze(addMe)._helium.moveMutation = true;
        } else {
          // console.log("New item", addMe, current);
          parentIndex++;
        }
      }
      lastNode = addMe;
    }
  }

  /** Removes all elements managed by this replacer from the page */
  public removeAll() {
    this.currentRender.forEach((node) => {
      if (node.parentElement) {
        node.parentElement.removeChild(node);
      }
    });
  }
}













// MOVED FROM RECYCLER FOR CIRCULAR REFERENCE REASONS




export interface ICachedRender {
  sourceVals: Map<SourceBase, any>,
  render: Innards
};


export class RenderRecycler extends RerenderReplacer {
  protected preConstructHook() {
    const renders: Array<ICachedRender> = []
    const rootRenderFn = this.rerenderFn;
    this.rerenderFn = () => {
      let cachedRender = renders.find((prevRender) => {
        const sourceVals = Array.from(prevRender.sourceVals.entries());
        for (const [source, val] of sourceVals) {
          if (val !== source.peek()) { return false; }
        }
        // make sure dependencies are updated
        for (const [source, val] of sourceVals) { source.get(); }
        return true;
      });

      // TODO: Make sure that the render is anchored to the cache, not
      // the recycler
      if (!cachedRender) {  
        renders.push(cachedRender = {
          sourceVals: new Map(), 
          render: rootRenderFn(),
        });
        const { scope } = this.getAnchorReport({scope: true});
        const sources = scope.getSourceDependencies();
        sources.forEach((source) => cachedRender.sourceVals.set(source, source.peek()));
      } else {
        console.log("Reusing cached render!", cachedRender);
      }

      return cachedRender.render;
    }
  }
}


/** Shorthand for creating a new RenderRecycler class.  Use this when you want to 
 * cache previous renders.
 * @importance 17
 * @warning This functions stops garbage collection of old views by caching them.  Only use this
 * function for sections of your UI which have very few permutations each with large amounts of content.
 * @eg const view = source("bert");\
 * const viewEl = div("View", recycle(\
 *   // each of these will only be rendered once\
 *   switch (view.get()) {\
 *     case "bert": return renderLargeMarkdown("berts-autobiography.md");\
 *     case "ernie": return renderLargeMarkdown("ernies-autobiography.md");\
 *   }\
 * ));
 */
export function recycle(renderFn: () => Innards) {
  const replacer = new RenderRecycler(renderFn);
  return replacer.getRender();
}